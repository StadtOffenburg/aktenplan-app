FROM maven:3-openjdk-17-slim as build
RUN useradd -m appuser
WORKDIR /usr/src/app/
RUN chown appuser:appuser /usr/src/app/
USER appuser
COPY --chown=appuser ./ ./
RUN mvn dependency:go-offline -Pproduction
RUN mvn clean package -DskipTests -Pproduction
RUN printf '${project.artifactId}-${project.version}.${project.packaging}'  | mvn help:evaluate -q -DforceStdout > /tmp/_build_file_name_

FROM openjdk:17-jdk-slim
COPY --from=build /tmp/_build_file_name_ /tmp/_build_file_name_
COPY --from=build /usr/src/app/target/*.jar /usr/app/
COPY ./environment-setup/docker/certificates/aktenplan_keycloak_server.der /usr/app/aktenplan_keycloak_server.der
RUN keytool -import -trustcacerts -cacerts -storepass changeit -noprompt -alias aktenplan_keycloak_ca.pem -file /usr/app/aktenplan_keycloak_server.der
RUN useradd -m appuser
USER appuser
ENV ENCRYPTOR_PASSWORD=""
EXPOSE 8080
ENTRYPOINT JAR_NAME=$(cat /tmp/_build_file_name_) \
    && java -jar -Djasypt.encryptor.password=${ENCRYPTOR_PASSWORD} /usr/app/${JAR_NAME}
