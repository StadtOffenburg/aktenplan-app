# Aktenplan-Anwendung

## Management Summary

Die Aktenplan Anwendung ermöglicht den Verwaltungsmitarbeitern der Stadt Offenburg einen Überblick über 
existierenden Aktenzeichen und deren hierarchiche Struktur. Der Nutzer wird bei der Recherche nach rechtsgültigen 
Aktenzeichen durch eine umfangreiche Suche- und Filterfunktion die mit verschiedenen Parametern und einer 
Volltextsuche umgesetzt wird, unterstützt Aktenzeicheneinträge werden mit zusätzlichen Informationen und Verweise zu 
rechtlichen Grundlage ergänzt und der Anwender kann individuelle Kommentare erstellen sowie zusätzliche 
Informationen von Wissensträgern erfragen. Ein Nutzer mit Administrationsrechten kann eine bestehende 
Aktenzeichen-Strukturen als XML Datei importieren und die Benutzer- und Rechteverwaltung durchführen.

The Aktenplan application provides an overview of existing file numbers and their hierarchical structure to the 
administrative staff of the city of Offenburg. The users are supported searching for legally valid file numbers by 
an extensive search and filter function which is implemented with various parameters and a full text search. File 
number entries are supplemented with additional information and references to legal basis and the user can create 
individual comments and request additional information from knowledge carriers. A user with administrative rights 
can import an existing file number structure as an XML file and perform user and rights management.

## Features
- Login as Admin user.
    - upload aktenplan/schema data using XML import
    - uploading a new XML will overwrite the previous aktenplan data, but will retain user created data items like unit tagging, guidances, notes, etc
    - if the new XML does not have an aktenplan item previously present in the system, then it will be marked as deprecated
    - create Administrative Units
    - add guidance to aktenplans
    - create/Edit users
- Login as user (non-admin)
    - use the various search criteria's to filter the aktenplans
    - include or exclue deprecated aktenplans from the view and search results
    - create notes against aktenplans
    - request guidance for aktenplans, this will create a new ticket in the osticket environment

## Running the application in development mode
- setup docker in your environment
- environment-setup/docker/docker-compose.yml details the configuration of the dependencies for this application
- `cd environment-setup/docker` directory and run `docker compose up -d`
- verify all docker containers are running in healthy state, `docker ps`
- start the application with `java -Djasypt.encryptor.password=<your-secret-goes-here> -jar aktenplan-<version_number>.jar`
- application runs on port 8080 at /
- default user created in keycloak with username `admin@offenburg.de` and password `12345`

## Production Deployment steps for Linux
- reconfigure apache to run on 443
- installation of apache module - mod_proxy (if needed)
- configure apache as reverse proxy to forward requests on 443 to 8080
- configure firewall to accept requests on 443
- configure firewall to block 8080 to be accesses from outside
- install https certificate on apache
- create a service to start the spring boot jar (the application runs on port 8080 and at / )
- setup backups of the system/database as necessary
- setup keycloak, osticket, elasticsearch and logstash
- deploy the application with the supplied jar file or build the application from source using `mvn -Pproduction clean install`
- if building the application from source, check/create application-prod.properties file in `src\main\resources\application-prod.properties`, update with appropriate values