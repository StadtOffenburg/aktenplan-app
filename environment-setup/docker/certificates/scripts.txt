#bash in wsl2
##create certificates
openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes -keyout aktenplan_keycloak_server.key -out aktenplan_keycloak_server.crt -subj "/C=FI/ST=Turku/L=Turku/O=Vaadin/OU=Services/CN=localhost" -addext "subjectAltName = DNS:localhost,DNS:keycloak"
openssl x509 -outform der -in aktenplan_keycloak_server.crt -out aktenplan_keycloak_server.der

#cmd in admin mode
##delete prior certificate (if any)
keytool -delete -alias aktenplan_keycloak_ca.pem  -cacerts -storepass changeit

##install certificate
keytool -import -trustcacerts -cacerts -storepass changeit -noprompt -alias aktenplan_keycloak_ca.pem -file aktenplan_keycloak_server.der
