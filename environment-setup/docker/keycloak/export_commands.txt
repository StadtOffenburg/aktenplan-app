docker exec -it aktenplan-keycloak /opt/keycloak/bin/kc.sh export --file /tmp/offenburg.json --realm stadt-offenburg
docker cp aktenplan-keycloak:/tmp/offenburg.json .
docker exec -it aktenplan-keycloak rm /tmp/offenburg.json
