#!/bin/sh

LANG1_FILE_PATH="../src/main/resources/translation_de.properties"
LANG2_FILE_PATH="../src/main/resources/translation_en.properties"

LANG1_KEYS=$(mktemp)
LANG2_KEYS=$(mktemp)

cut -d'=' -f1 $LANG1_FILE_PATH | sed '/^$/d' | sort -o $LANG1_KEYS
cut -d'=' -f1 $LANG2_FILE_PATH | sed '/^$/d' | sort -o $LANG2_KEYS

echo "comparing $LANG1_FILE_PATH with $LANG2_FILE_PATH"
comm -23 $LANG1_KEYS $LANG2_KEYS

echo "comparing $LANG2_FILE_PATH with $LANG1_FILE_PATH"
comm -23 $LANG2_KEYS $LANG1_KEYS


rm $LANG1_KEYS $LANG2_KEYS
