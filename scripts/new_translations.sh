#!/bin/sh

GIT_MAIN_BRANCH="main"

FILE_NAME=$(mktemp)
TRACK_CHANGES_IN_FILE_PATH="../src/main/resources/translation_de.properties"
COMPARE_CHANGES_WITH_FILE_PATH="../src/main/resources/translation_en.properties"

if [ -f $FILE_NAME ]
then
    rm $FILE_NAME
fi
git diff $GIT_MAIN_BRANCH HEAD --unified=0  -- $TRACK_CHANGES_IN_FILE_PATH | grep '^[+][^+]' | sed 's/^+/#de /' > $FILE_NAME
cut -d'=' -f1 $FILE_NAME | cut -d' ' -f2 | grep -f - $COMPARE_CHANGES_WITH_FILE_PATH | sed 's/^/#en /' >> $FILE_NAME
sed 's/^\(.*\) \(.*\)=\(.*\)$/\1 \2 \2=\3/' $FILE_NAME | sort -k2,2 -k1,1r -o $FILE_NAME
awk -i inplace '{$2=""}1' $FILE_NAME
sed -i 's/#en/\n#en/' $FILE_NAME
sed -i '/./,$!d' $FILE_NAME
cat $FILE_NAME
rm $FILE_NAME

