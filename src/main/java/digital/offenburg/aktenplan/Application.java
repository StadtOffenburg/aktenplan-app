package digital.offenburg.aktenplan;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.keycloak.admin.client.Keycloak;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import com.vaadin.flow.component.dependency.NpmPackage;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.server.AppShellSettings;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.theme.Theme;

import digital.offenburg.aktenplan.config.ApplicationProperties;
import digital.offenburg.aktenplan.config.session.SessionRepository;
import digital.offenburg.aktenplan.config.session.SessionRepositoryListener;
import digital.offenburg.aktenplan.data.dto.UserDto;
import digital.offenburg.aktenplan.data.entity.User;
import digital.offenburg.aktenplan.data.mapper.MapStructMapper;
import digital.offenburg.aktenplan.data.service.user.KeycloakUserService;
import digital.offenburg.aktenplan.data.service.user.UserService;

/**
 * The entry point of the Spring Boot application.
 * Use the @PWA annotation make the application installable on phones, tablets
 * and some desktop browsers.
 */
@SpringBootApplication(exclude = { UserDetailsServiceAutoConfiguration.class })
@EnableTransactionManagement
@Theme(value = "aktenplan")
@PWA(name = "AktenPlan", shortName = "AktenPlan", offlineResources = {})
@NpmPackage(value = "line-awesome", version = "1.3.0")
@EnableJpaAuditing
@EnableJpaRepositories
@EnableEncryptableProperties
public class Application extends SpringBootServletInitializer implements AppShellConfigurator {

    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void configurePage(final AppShellSettings settings) {
        settings.addFavIcon("icon", "icons/icon.png", "32x32");
    }

    @Bean
    public MessageSource messageSource() {
        final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:translation");
        messageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());
        return messageSource;
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        final LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(this.messageSource());
        return bean;
    }

    @Bean
    public AuditorAware<User> auditorProvider(final MapStructMapper mapper) {
        return () -> Optional.ofNullable(SecurityContextHolder.getContext())
                             .map(SecurityContext::getAuthentication)
                             .filter(Authentication::isAuthenticated)
                             .map(Authentication::getPrincipal)
                             .map(UserDto.class::cast)
                             .map(mapper::toEntity);
    }

    @Bean
    public UserService userService(final Keycloak keycloak, final ApplicationProperties applicationProperties) {
        return new KeycloakUserService(keycloak, applicationProperties.getRealm());
    }

    @Bean
    public SessionRepository sessionRepository() {
        return new SessionRepository();
    }

    @Bean
    public ServletListenerRegistrationBean<SessionRepositoryListener> sessionRepositoryListener() {
        final ServletListenerRegistrationBean<SessionRepositoryListener> bean = new ServletListenerRegistrationBean<>();
        bean.setListener(new SessionRepositoryListener(this.sessionRepository()));
        return bean;
    }

}
