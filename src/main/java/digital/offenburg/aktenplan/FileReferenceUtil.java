package digital.offenburg.aktenplan;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;

public final class FileReferenceUtil {

    private FileReferenceUtil() {
    }

    public static Set<FileReferenceDto> merge(final Set<FileReferenceDto> newReferences, final Set<FileReferenceDto> oldReferences) {
        final Map<String, FileReferenceDto> newMap = newReferences.stream()
                                                                  .collect(Collectors.toMap(FileReferenceDto::getId, Function.identity()));

        final Map<String, FileReferenceDto> oldMap = oldReferences.stream()
                                                                  .collect(Collectors.toMap(FileReferenceDto::getId, Function.identity()));

        oldMap.entrySet()
              .stream()
              .filter(entry -> !newMap.containsKey(entry.getKey()))
              .forEach(entry -> newMap.put(entry.getKey(), entry.getValue()));

        newMap.entrySet().forEach(entry -> entry.getValue().merge(oldMap.get(entry.getKey())));
        return newMap.values().stream().collect(Collectors.toSet());
    }

    public static Set<FileReferenceDto> prepareHierarchy(final Stream<FileReferenceDto> fileReferenceDtoStream, final Map<String, String> userFavourites) {
        final Map<String, FileReferenceDto> fileRefMap = fileReferenceDtoStream.collect(Collectors.toMap(FileReferenceDto::getId, Function.identity()));
        for (final Entry<String, FileReferenceDto> entry : fileRefMap.entrySet()) {
            final FileReferenceDto item = entry.getValue();
            item.setUserFavourite(userFavourites.containsKey(item.getId()));
            if (item.getParentId() != null) {
                fileRefMap.get(item.getParentId()).getSubReference().add(item);
            }
        }
        return fileRefMap.values().stream().filter(item -> item.getParentId() == null).collect(Collectors.toSet());
    }
}
