package digital.offenburg.aktenplan;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

public final class GenericUtil {
    private final static Map<Character, String> REPLACEMENT_MAP;

    static {
        REPLACEMENT_MAP = new HashMap<>();
        REPLACEMENT_MAP.put('ß', "s");
        REPLACEMENT_MAP.put('Æ', "AE");
        REPLACEMENT_MAP.put('æ', "ae");
        REPLACEMENT_MAP.put('Ð', "D");
        REPLACEMENT_MAP.put('đ', "d");
        REPLACEMENT_MAP.put('ð', "d");
        REPLACEMENT_MAP.put('Ø', "O");
        REPLACEMENT_MAP.put('ø', "o");
        REPLACEMENT_MAP.put('Œ', "OE");
        REPLACEMENT_MAP.put('œ', "oe");
        REPLACEMENT_MAP.put('Ŧ', "T");
        REPLACEMENT_MAP.put('ŧ', "t");
        REPLACEMENT_MAP.put('Ł', "L");
        REPLACEMENT_MAP.put('ł', "l");
    }

    private GenericUtil() {
    }

    public static String stripAccents(final String input) {
        String strippedText = StringUtils.stripAccents(input);
        if (strippedText != null) {
            strippedText = strippedText.chars()
                                       .mapToObj(ch -> (char) ch)
                                       .map(ch -> REPLACEMENT_MAP.getOrDefault(ch, String.valueOf(ch)))
                                       .collect(Collectors.joining());
        }
        return strippedText;
    }

    public static Pattern normalizedPattern(final String input) {
        final String normalizedText = GenericUtil.stripAccents(input);
        return Pattern.compile(Pattern.quote(normalizedText), Pattern.CASE_INSENSITIVE);
    }

    public static void findAndHighlight(final Pattern pattern, final String matchText, final Function<String, String> searchHighlighter, final Consumer<String> consumeSearchHighlight) {
        findAndHighlight(pattern, matchText, searchHighlighter, consumeSearchHighlight, null);
    }

    public static void findAndHighlight(final Pattern pattern, final String matchText, final Function<String, String> searchHighlighter, final Consumer<String> consumeSearchHighlight,
            final Runnable emptyAction) {
        final String normalizedText = GenericUtil.stripAccents(matchText);
        final Matcher matcher = pattern.matcher(normalizedText);
        if (matcher.find()) {
            final String matchedString = matchText.substring(matcher.start(), matcher.end());
            final String result = matchText.replace(matchedString, searchHighlighter.apply(matchedString));
            consumeSearchHighlight.accept(result);
        } else if (emptyAction != null) {
            emptyAction.run();
        }
    }

}
