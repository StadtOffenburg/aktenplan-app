package digital.offenburg.aktenplan;

import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.i18n.LocaleContextHolder;

import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;

@SpringComponent
public class TranslationProvider implements I18NProvider {
    private static final Logger LOGGER = LogManager.getLogger(TranslationProvider.class);

    public static final String BUNDLE_PREFIX = "translation";

    @Override
    public List<Locale> getProvidedLocales() {
        return List.of(Locale.GERMAN, Locale.ENGLISH);
    }

    @Override
    public String getTranslation(final String key, final Locale locale, final Object... params) {
        String value = "";
        if (key == null) {
            LOGGER.warn("Got lang request for key with null value!");
        } else {
            final ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_PREFIX, locale);
            try {
                value = bundle.getString(key);
                if (params.length > 0) {
                    value = String.format(LocaleContextHolder.getLocale(), value, params);
                }
            } catch (final MissingResourceException e) {
                LOGGER.error("Missing resource", e);
                value = "!" + locale.getLanguage() + ": " + key;
            }
        }
        return value;
    }

}
