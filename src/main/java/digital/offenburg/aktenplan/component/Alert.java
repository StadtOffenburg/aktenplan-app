package digital.offenburg.aktenplan.component;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public final class Alert {

    private static final int DEFAULT_DURATION_IN_SECONDS = 10;

    private Alert() {
    }

    public static void error(final String message) {
        new Alert().create(message, NotificationVariant.LUMO_ERROR)
                   .open();
    }

    public static void primary(final String message) {
        new Alert().create(message, NotificationVariant.LUMO_PRIMARY)
                   .open();
    }

    public static void success(final String message) {
        new Alert().create(message, NotificationVariant.LUMO_SUCCESS)
                   .open();
    }

    public Notification create(final String message, final NotificationVariant variant) {
        final var notification = this.notification();
        notification.addThemeVariants(variant);

        final var layout = new HorizontalLayout();
        layout.setWidthFull();
        layout.addAndExpand(new Span(message));

        final var closeBtn = new Button(new Icon("lumo", "cross"));
        closeBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        closeBtn.getElement().setAttribute("aria-label", "Close");
        closeBtn.addClickListener(event -> {
            notification.close();
        });

        layout.add(closeBtn);

        notification.add(layout);
        return notification;
    }

    private Notification notification() {
        final var notification = new Notification();
        notification.setPosition(Position.TOP_END);
        notification.setDuration(DEFAULT_DURATION_IN_SECONDS * 1000);
        return notification;
    }

}
