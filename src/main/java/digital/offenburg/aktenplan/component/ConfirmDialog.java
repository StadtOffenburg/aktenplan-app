package digital.offenburg.aktenplan.component;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.shared.Registration;

import digital.offenburg.aktenplan.event.ConfirmationEvent;

public class ConfirmDialog extends Dialog {

    public ConfirmDialog(final ConfirmDialogI18n confirmDialogI18n) {
        final Button cancelBtn = new Button(confirmDialogI18n.getCancelButtonLabel(), event -> this.close());
        cancelBtn.addClassName("ui-secondary-button");

        final Button confirmBtn = new Button(confirmDialogI18n.getConfirmButtonLabel(), event -> {
            this.fireEvent(new ConfirmationEvent(this));
        });
        confirmBtn.addClassName("ui-danger-button");

        final Paragraph body = new Paragraph(confirmDialogI18n.getMessage());

        this.setHeaderTitle(confirmDialogI18n.getTitle());
        this.getFooter().add(cancelBtn, confirmBtn);
        this.add(body);
        this.addClassNames("ui-dialog", "ui-confirm-dialog");
    }

    public Registration addConfirmListener(final ComponentEventListener<ConfirmationEvent> listener) {
        return super.addListener(ConfirmationEvent.class, listener);
    }

    public static final class ConfirmDialogI18n {
        private String title;
        private String message;
        private String cancelButtonLabel;
        private String confirmButtonLabel;

        public String getTitle() {
            return this.title;
        }

        public String getMessage() {
            return this.message;
        }

        public void setTitle(final String title) {
            this.title = title;
        }

        public void setMessage(final String message) {
            this.message = message;
        }

        public String getCancelButtonLabel() {
            return this.cancelButtonLabel;
        }

        public void setCancelButtonLabel(final String cancelButtonLabel) {
            this.cancelButtonLabel = cancelButtonLabel;
        }

        public String getConfirmButtonLabel() {
            return this.confirmButtonLabel;
        }

        public void setConfirmButtonLabel(final String confirmButtonLabel) {
            this.confirmButtonLabel = confirmButtonLabel;
        }

    }
}
