package digital.offenburg.aktenplan.component;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.shared.Registration;

import digital.offenburg.aktenplan.event.SaveFormDialogEvent;

public class FormDialog<T extends Serializable> extends Dialog {
    private static final Logger LOGGER = LogManager.getLogger(FormDialog.class);

    private final Binder<T> binder;
    private final T item;

    public FormDialog(final Class<T> beanType, final T item) {
        this.item = item;
        this.binder = new BeanValidationBinder<>(beanType);
    }

    public void prepare(final FormDialogI18n formDialogI18n, final Component formComponent) {
        this.binder.readBean(this.item);

        final Button cancelBtn = new Button(formDialogI18n.getCancelButtonLabel(), event -> this.close());
        cancelBtn.addClassName("ui-secondary-button");

        final Button saveBtn = new Button(formDialogI18n.getSaveButtonLabel(), event -> {
            this.save();
        });
        saveBtn.addClickShortcut(Key.ENTER);
        saveBtn.addClassName("ui-primary-button");

        this.setHeaderTitle(formDialogI18n.getHeaderTitle());
        this.getFooter().add(cancelBtn, saveBtn);
        this.add(formComponent);
        this.addClassNames("ui-dialog");
    }

    private void save() {
        if (this.binder.validate().isOk()) {
            try {
                this.binder.writeBean(this.item);
                this.fireEvent(new SaveFormDialogEvent<>(this, this.item));
            } catch (final ValidationException ex) {
                LOGGER.fatal(ex.getMessage(), ex);
                Alert.error(this.getTranslation("error.message.fatal"));
            }
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Registration addSaveItemEventListener(final ComponentEventListener<SaveFormDialogEvent<T>> listener) {
        final Class<? extends SaveFormDialogEvent> clazz = SaveFormDialogEvent.class;
        return super.addListener((Class<SaveFormDialogEvent<T>>) clazz, listener);
    }

    public Binder<T> getBinder() {
        return this.binder;
    }

    public static final class FormDialogI18n {
        private String headerTitle;
        private String saveButtonLabel;
        private String cancelButtonLabel;

        public String getHeaderTitle() {
            return this.headerTitle;
        }

        public void setHeaderTitle(final String headerTitle) {
            this.headerTitle = headerTitle;
        }

        public String getSaveButtonLabel() {
            return this.saveButtonLabel;
        }

        public void setSaveButtonLabel(final String saveButtonLabel) {
            this.saveButtonLabel = saveButtonLabel;
        }

        public String getCancelButtonLabel() {
            return this.cancelButtonLabel;
        }

        public void setCancelButtonLabel(final String cancelButtonLabel) {
            this.cancelButtonLabel = cancelButtonLabel;
        }
    }
}
