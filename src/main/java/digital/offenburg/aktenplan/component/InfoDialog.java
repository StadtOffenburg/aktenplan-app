package digital.offenburg.aktenplan.component;

import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Span;

public class InfoDialog extends Dialog {
    private final String header;
    private final String message;

    public InfoDialog(final String header, final String message) {
        this.header = header;
        this.message = message;
        this.prepare();
    }

    private void prepare() {
        this.setHeaderTitle(this.header);
        this.add(new Span(this.message));
    }

}
