package digital.offenburg.aktenplan.component;

import org.apache.commons.lang3.StringUtils;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.theme.lumo.LumoUtility;

public class LargeTextDisplay extends Div {

    private final String fullText;
    private final int minLength;
    private final String moreLabel;
    private final String lessLabel;
    private final boolean trimRequired;

    private boolean showingMore;

    public LargeTextDisplay(final String fullText, final int minLength, final String moreLabel, final String lessLabel) {
        this.fullText = fullText;
        this.minLength = minLength;
        this.moreLabel = moreLabel;
        this.lessLabel = lessLabel;
        this.showingMore = false;
        this.trimRequired = this.fullText.length() > this.minLength;
        this.setup();
    }

    private void setup() {
        final String minContent = StringUtils.left(this.fullText, this.minLength) + (this.trimRequired ? " ..." : "");
        final Span span = new Span(minContent);

        this.add(span);
        this.setWidthFull();

        if (this.trimRequired) {
            final Button moreLink = new LinkButton(this.moreLabel);
            moreLink.addClassNames(LumoUtility.Padding.Left.XSMALL, LumoUtility.Display.INLINE);
            moreLink.addClickListener(event -> {
                if (this.showingMore) {
                    span.setText(minContent);
                } else {
                    span.setText(this.fullText);
                }
                this.showingMore = !this.showingMore;
                moreLink.setText(this.showingMore ? this.lessLabel : this.moreLabel);
            });
            this.add(moreLink);
        }
    }

}
