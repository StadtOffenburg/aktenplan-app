package digital.offenburg.aktenplan.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;

public class LinkButton extends Button {

    public LinkButton(final Component component) {
        super(component);
        this.init();
    }

    public LinkButton(final String text) {
        super(text);
        this.init();
    }

    public LinkButton(final String text, final Component icon) {
        super(text, icon);
        this.init();
    }

    private void init() {
        this.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        this.addClassNames("ui-link-button");
    }

}
