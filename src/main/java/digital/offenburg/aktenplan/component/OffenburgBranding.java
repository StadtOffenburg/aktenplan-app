package digital.offenburg.aktenplan.component;

import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.theme.lumo.LumoUtility;

public class OffenburgBranding extends FlexLayout {

    public OffenburgBranding() {
        this.addClassNames("ui-offenburg-branding");
        this.getStyle().set("flex", "1 1 0");

        final FlexLayout brandName = new FlexLayout();
        brandName.setFlexDirection(FlexDirection.COLUMN);
        brandName.setJustifyContentMode(JustifyContentMode.CENTER);

        brandName.addClassNames(LumoUtility.Padding.MEDIUM, LumoUtility.Padding.Horizontal.XLARGE, LumoUtility.BoxSizing.BORDER);

        final Span line1 = new Span("OFFENBURG");
        line1.addClassNames(LumoUtility.FontWeight.MEDIUM, LumoUtility.LineHeight.NONE);
        final Span line2 = new Span("UNSERE STADT");
        line2.addClassNames(LumoUtility.TextColor.ERROR, LumoUtility.FontWeight.BOLD, LumoUtility.LineHeight.NONE);
        brandName.add(line1, line2);

        this.add(brandName);
    }

}
