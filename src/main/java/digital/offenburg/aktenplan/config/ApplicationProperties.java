package digital.offenburg.aktenplan.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationProperties {
    @Value("${osticket.api.url}")
    private String osTicketUrl;
    
    @Value("${osticket.api.key}")
    private String osTicketAPI;
    
    @Value("${spring.security.oauth2.client.registration.aktenplan.provider}")
    private String keycloakRegistrationId;
    
    @Value("${keycloak.server.url}")
    private String serverUrl;
    
    @Value("${keycloak.realm}")
    private String realm;
    
    @Value("${keycloak.client.id}")
    private String clientId;
    
    @Value("${keycloak.client.secret}")
    private String clientSecret;
    
    public String getOsTicketUrl() {
        return this.osTicketUrl;
    }
    
    public String getOsTicketAPI() {
        return this.osTicketAPI;
    }
    
    public String getKeycloakRegistrationId() {
        return this.keycloakRegistrationId;
    }
    
    public String getServerUrl() {
        return this.serverUrl;
    }
    
    public String getRealm() {
        return this.realm;
    }
    
    public String getClientId() {
        return this.clientId;
    }
    
    public String getClientSecret() {
        return this.clientSecret;
    }
    
}
