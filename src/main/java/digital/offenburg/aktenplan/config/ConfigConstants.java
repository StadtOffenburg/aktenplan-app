package digital.offenburg.aktenplan.config;

public final class ConfigConstants {
    public static final String APP_BASE_URL = "{baseUrl}";
    public static final String LOGOUT_URL = "/logout";
    public static final String LOGIN_URL = "/oauth2/authorization/aktenplan";
}
