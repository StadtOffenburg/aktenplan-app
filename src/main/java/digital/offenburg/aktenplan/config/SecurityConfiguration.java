package digital.offenburg.aktenplan.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.FormLoginConfigurer;
import org.springframework.security.config.annotation.web.configurers.HttpBasicConfigurer;
import org.springframework.security.config.annotation.web.configurers.RememberMeConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.oidc.web.logout.OidcClientInitiatedLogoutSuccessHandler;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.vaadin.flow.spring.security.VaadinSavedRequestAwareAuthenticationSuccessHandler;
import com.vaadin.flow.spring.security.VaadinWebSecurity;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends VaadinWebSecurity {
    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;
    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Override
    public void configure(final WebSecurity web) throws Exception {
        super.configure(web);
        web.ignoring()
           .antMatchers("/images/*.png")
           .antMatchers("/images/*.jpg")
           .antMatchers("/icons/*.svg")
           .antMatchers("/back-channel-logout");
    }
    
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        super.configure(http);
        
        // disable spring security features which are not required
        http.httpBasic(HttpBasicConfigurer::disable)
            .formLogin(FormLoginConfigurer::disable)
            .rememberMe(RememberMeConfigurer::disable);
        
        http.oauth2Login(oauth -> oauth.successHandler(new VaadinSavedRequestAwareAuthenticationSuccessHandler()))
            .exceptionHandling(exp -> exp.authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint(ConfigConstants.LOGIN_URL)))
            .logout(logout -> logout.logoutSuccessHandler(this.logoutSuccessHandler())
                                    .logoutRequestMatcher(new AntPathRequestMatcher(ConfigConstants.LOGOUT_URL, "GET"))
                                    .invalidateHttpSession(true)
                                    .deleteCookies("JSESSIONID"));
    }
    
    private OidcClientInitiatedLogoutSuccessHandler logoutSuccessHandler() {
        final var logoutSuccessHandler = new OidcClientInitiatedLogoutSuccessHandler(this.clientRegistrationRepository);
        logoutSuccessHandler.setPostLogoutRedirectUri(ConfigConstants.APP_BASE_URL);
        return logoutSuccessHandler;
    }
    
}
