package digital.offenburg.aktenplan.config.hibernate;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitForeignKeyNameSource;
import org.hibernate.boot.model.naming.ImplicitUniqueKeyNameSource;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;

public class CustomImplicitNamingStrategy extends SpringImplicitNamingStrategy {
    @Override
    public Identifier determineForeignKeyName(final ImplicitForeignKeyNameSource source) {
        final String columnNames = source.getColumnNames()
                                         .stream()
                                         .map(Identifier::getCanonicalName)
                                         .collect(Collectors.joining("_"));

        final String tableNamePart = Arrays.stream(source.getTableName().getCanonicalName().split("_"))
                                           .skip(1)
                                           .collect(Collectors.joining());

        final StringBuilder sb = new StringBuilder()
                                                    .append("fk_")
                                                    .append(tableNamePart)
                                                    .append("_")
                                                    .append(columnNames);

        return this.toIdentifier(sb.toString(), source.getBuildingContext());
    }

    @Override
    public Identifier determineUniqueKeyName(final ImplicitUniqueKeyNameSource source) {
        final String columnNames = source.getColumnNames()
                                         .stream()
                                         .map(Identifier::getCanonicalName)
                                         .collect(Collectors.joining("_"));

        final String tableNamePart = Arrays.stream(source.getTableName().getCanonicalName().split("_"))
                                           .skip(1)
                                           .collect(Collectors.joining());

        final StringBuilder sb = new StringBuilder()
                                                    .append("fk_")
                                                    .append(tableNamePart)
                                                    .append("_")
                                                    .append(columnNames);

        return this.toIdentifier(sb.toString(), source.getBuildingContext());
    }
}