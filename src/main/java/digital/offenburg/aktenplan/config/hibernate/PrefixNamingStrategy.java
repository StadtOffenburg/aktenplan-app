package digital.offenburg.aktenplan.config.hibernate;

import org.hibernate.boot.model.naming.CamelCaseToUnderscoresNamingStrategy;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

public class PrefixNamingStrategy extends CamelCaseToUnderscoresNamingStrategy {
    private static final String TABLE_PREFIX = "T_";

    @Override
    public Identifier toPhysicalTableName(final Identifier name, final JdbcEnvironment jdbcEnvironment) {
        final Identifier newIdentifier = super.toPhysicalTableName(name, jdbcEnvironment);
        return this.getIdentifier(TABLE_PREFIX + newIdentifier.getText(), newIdentifier.isQuoted(), jdbcEnvironment);
    }

}
