package digital.offenburg.aktenplan.config.keycloak;

import java.util.Optional;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import digital.offenburg.aktenplan.config.ApplicationProperties;
import digital.offenburg.aktenplan.config.session.SessionRepository;
import digital.offenburg.aktenplan.data.dto.UserDto;

@RestController
public class KeycloakBackChannelLogoutController {
    private final JwtDecoder jwtDecoder;
    private final SessionRepository sessionRepository;
    
    public KeycloakBackChannelLogoutController(
            final ApplicationProperties applicationProperties,
            final ClientRegistrationRepository clientRegistrationRepository,
            final SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
        final String jwkSetUri = clientRegistrationRepository.findByRegistrationId(applicationProperties.getKeycloakRegistrationId())
                                                             .getProviderDetails()
                                                             .getJwkSetUri();
        this.jwtDecoder = NimbusJwtDecoder.withJwkSetUri(jwkSetUri).build();
    }
    
    @PostMapping("/back-channel-logout")
    public void logout(@RequestParam("logout_token") final String rawLogoutToken) {
        final var logoutToken = this.jwtDecoder.decode(rawLogoutToken);
        final var sessionId = logoutToken.getClaimAsString("sid");
        this.invalidateSession(sessionId);
    }
    
    private void invalidateSession(final String keycloakSessionId) {
        this.sessionRepository.invalidate(session -> Optional.ofNullable(session.getAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY))
                                                             .map(SecurityContext.class::cast)
                                                             .map(context -> context.getAuthentication().getPrincipal())
                                                             .filter(principal -> {
                                                                 if (principal instanceof final UserDto userDto) {
                                                                     return keycloakSessionId.equals(userDto.getSessionId());
                                                                 }
                                                                 return false;
                                                             })
                                                             .isPresent());
    }
    
}
