package digital.offenburg.aktenplan.config.keycloak;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import digital.offenburg.aktenplan.config.ApplicationProperties;

@Configuration
public class KeycloakConfiguration {
    private static final String GRANT_TYPE = "client_credentials";
    
    @Bean
    public Keycloak keycloakClient(final ApplicationProperties applicationProperties) {
        return KeycloakBuilder.builder()
                              .serverUrl(applicationProperties.getServerUrl())
                              .realm(applicationProperties.getRealm())
                              .clientId(applicationProperties.getClientId())
                              .clientSecret(applicationProperties.getClientSecret())
                              .grantType(GRANT_TYPE)
                              .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
                              .build();
    }
}
