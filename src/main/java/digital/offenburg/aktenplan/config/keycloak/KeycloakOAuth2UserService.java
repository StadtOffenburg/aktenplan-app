package digital.offenburg.aktenplan.config.keycloak;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority;
import org.springframework.stereotype.Service;

import digital.offenburg.aktenplan.data.constant.Role;
import digital.offenburg.aktenplan.data.dto.UserDto;
import digital.offenburg.aktenplan.data.service.user.LocalUserService;

@Service
public class KeycloakOAuth2UserService extends OidcUserService {
    
    @Autowired
    private LocalUserService localUserService;
    
    @Override
    public OidcUser loadUser(final OidcUserRequest userRequest) throws OAuth2AuthenticationException {
        final OidcUser oidcUser = super.loadUser(userRequest);
        
        final List<String> roles = this.findRoles(oidcUser.getAuthorities());
        final Collection<? extends GrantedAuthority> mappedAuthorities = roles.stream()
                                                                              .map(role -> "ROLE_" + role)
                                                                              .map(SimpleGrantedAuthority::new)
                                                                              .toList();
        
        final UserDto userDto = new UserDto();
        userDto.setId(oidcUser.getUserInfo().getSubject());
        userDto.setEmail(oidcUser.getEmail());
        userDto.setFirstName(oidcUser.getGivenName());
        userDto.setLastName(oidcUser.getFamilyName());
        userDto.setAuthorities(mappedAuthorities);
        
        final Role role = roles.stream().map(Role::valueOf).findFirst().orElse(Role.USER);
        userDto.setRole(role);
        
        userDto.setIdToken(oidcUser.getIdToken());
        userDto.setSessionId(oidcUser.getAttribute("sid"));
        localUserService.saveUser(userDto);
        return userDto;
    }
    
    private List<String> findRoles(final Collection<? extends GrantedAuthority> authorities) {
        return authorities.stream()
                          .filter(OidcUserAuthority.class::isInstance)
                          .map(OidcUserAuthority.class::cast)
                          .findFirst()
                          .map(this::extractClientRoles)
                          .orElseGet(Collections::emptyList);
    }
    
    @SuppressWarnings("unchecked")
    private List<String> extractClientRoles(final OidcUserAuthority oauthAuthority) {
        return Optional.ofNullable(oauthAuthority.getUserInfo().getClaimAsMap("realm_access"))
                       .map(realm -> (Collection<String>) realm.get("roles"))
                       .stream()
                       .flatMap(Collection::stream)
                       .toList();
    }
    
}
