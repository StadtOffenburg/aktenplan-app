package digital.offenburg.aktenplan.config.session;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

import javax.servlet.http.HttpSession;

public class SessionRepository {
    private final Set<HttpSession> sessions = new HashSet<>();
    
    public synchronized void add(final HttpSession session) {
        this.sessions.add(session);
    }
    
    public synchronized void remove(final HttpSession session) {
        this.sessions.remove(session);
    }
    
    public synchronized void invalidate(final Predicate<HttpSession> predicate) {
        Set.copyOf(this.sessions).stream().filter(predicate).forEach(HttpSession::invalidate);
    }
}
