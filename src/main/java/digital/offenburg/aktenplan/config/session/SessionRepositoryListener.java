package digital.offenburg.aktenplan.config.session;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionRepositoryListener implements HttpSessionListener {

    private final SessionRepository sessionRepository;

    public SessionRepositoryListener(final SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    @Override
    public void sessionCreated(final HttpSessionEvent se) {
        this.sessionRepository.add(se.getSession());
    }

    @Override
    public void sessionDestroyed(final HttpSessionEvent se) {
        this.sessionRepository.remove(se.getSession());
    }
}