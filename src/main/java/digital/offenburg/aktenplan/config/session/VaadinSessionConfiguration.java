package digital.offenburg.aktenplan.config.session;

import org.springframework.stereotype.Component;

import com.vaadin.flow.server.CustomizedSystemMessages;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.SessionDestroyEvent;
import com.vaadin.flow.server.SessionDestroyListener;
import com.vaadin.flow.server.SystemMessages;
import com.vaadin.flow.server.SystemMessagesInfo;
import com.vaadin.flow.server.SystemMessagesProvider;
import com.vaadin.flow.server.VaadinServiceInitListener;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.WrappedSession;

import digital.offenburg.aktenplan.config.ConfigConstants;

@Component
public class VaadinSessionConfiguration implements VaadinServiceInitListener, SystemMessagesProvider, SessionDestroyListener {

    @Override
    public void sessionDestroy(final SessionDestroyEvent event) {
        final VaadinSession session = event.getSession();

        if (session != null) {
            final WrappedSession wrappedSession = session.getSession();
            if (wrappedSession != null) {
                wrappedSession.invalidate();
            }
            session.close();
        }
    }

    @Override
    public SystemMessages getSystemMessages(final SystemMessagesInfo systemMessagesInfo) {
        final CustomizedSystemMessages messages = new CustomizedSystemMessages();
        messages.setSessionExpiredURL(ConfigConstants.LOGOUT_URL);
        return messages;
    }

    @Override
    public void serviceInit(final ServiceInitEvent event) {
        event.getSource().setSystemMessagesProvider(this);
        event.getSource().addSessionDestroyListener(this);
    }

}
