package digital.offenburg.aktenplan.data.constant;

import org.apache.commons.lang3.StringUtils;
import org.keycloak.representations.idm.RoleRepresentation;
import org.springframework.context.i18n.LocaleContextHolder;

public enum Role {
    USER(Named.USER),
    ADMIN(Named.ADMIN);

    public final String description;

    public static class Named {
        public static final String USER = "USER";
        public static final String ADMIN = "ADMIN";
    }

    Role(final String description) {
        if (!this.name().equals(description)) {
            throw new IllegalArgumentException("wrong description");
        }
        this.description = description;
    }

    public String getDescription() {
        return StringUtils.capitalize(this.description.toLowerCase(LocaleContextHolder.getLocale()));
    }

    public RoleRepresentation keycloakRole() {
        return new RoleRepresentation(this.name(), null, false);
    }

}
