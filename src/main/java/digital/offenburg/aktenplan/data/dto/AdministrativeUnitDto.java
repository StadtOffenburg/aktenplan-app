package digital.offenburg.aktenplan.data.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.validation.constraints.NotEmpty;

public class AdministrativeUnitDto implements Serializable {
    private Long id;
    @NotEmpty
    private String name;
    private LocalDateTime timestamp;

    public AdministrativeUnitDto() {
        this.id = null;
        this.name = null;
        this.timestamp = null;
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public LocalDateTime getTimestamp() {
        return this.timestamp;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setTimestamp(final LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final AdministrativeUnitDto other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }

}
