package digital.offenburg.aktenplan.data.dto;

import java.io.Serializable;
import java.util.Objects;

import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;

public class FileReferenceUnitDto implements Serializable {
    private FileReferenceUnitIdDto id;
    private AdministrativeUnitDto unit;
    private FileReferenceDto fileReference;

    public FileReferenceUnitDto() {
    }

    public FileReferenceUnitDto(final AdministrativeUnitDto unit, final FileReferenceDto fileReference) {
        this.id = new FileReferenceUnitIdDto(unit.getId(), fileReference.getId());
        this.unit = unit;
        this.fileReference = fileReference;
    }

    public FileReferenceUnitIdDto getId() {
        return this.id;
    }

    public void setId(final FileReferenceUnitIdDto id) {
        this.id = id;
    }

    public AdministrativeUnitDto getUnit() {
        return this.unit;
    }

    public void setUnit(final AdministrativeUnitDto unit) {
        this.unit = unit;
    }

    public FileReferenceDto getFileReference() {
        return this.fileReference;
    }

    public void setFileReference(final FileReferenceDto fileReference) {
        this.fileReference = fileReference;
    }

    public static class FileReferenceUnitIdDto implements Serializable {
        private Long unitId;
        private String fileReferenceId;

        public FileReferenceUnitIdDto() {
        }

        public FileReferenceUnitIdDto(final Long unitId, final String fileReferenceId) {
            this.unitId = unitId;
            this.fileReferenceId = fileReferenceId;
        }

        public Long getUnitId() {
            return this.unitId;
        }

        public void setUnitId(final Long unitId) {
            this.unitId = unitId;
        }

        public String getFileReferenceId() {
            return this.fileReferenceId;
        }

        public void setFileReferenceId(final String fileReferenceId) {
            this.fileReferenceId = fileReferenceId;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.fileReferenceId, this.unitId);
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof final FileReferenceUnitIdDto other)) {
                return false;
            }
            return this.fileReferenceId != null && Objects.equals(this.fileReferenceId, other.fileReferenceId) && Objects.equals(this.unitId, other.unitId);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final FileReferenceUnitDto other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }

}
