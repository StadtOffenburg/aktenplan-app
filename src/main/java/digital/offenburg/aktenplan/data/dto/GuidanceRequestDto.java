package digital.offenburg.aktenplan.data.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;

public class GuidanceRequestDto implements Serializable {
    private Long id;
    private String title;

    @NotEmpty
    @Size(min = 1, max = 4000)
    private String text;
    private String planVersion;
    private FileReferenceDto fileReference;

    public GuidanceRequestDto() {
        this.id = null;
        this.title = null;
        this.text = null;
        this.planVersion = null;
        this.fileReference = null;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getText() {
        return this.text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getPlanVersion() {
        return this.planVersion;
    }

    public void setPlanVersion(final String planVersion) {
        this.planVersion = planVersion;
    }

    public FileReferenceDto getFileReference() {
        return this.fileReference;
    }

    public void setFileReference(final FileReferenceDto fileReference) {
        this.fileReference = fileReference;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final GuidanceRequestDto other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }

}
