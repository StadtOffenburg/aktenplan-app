package digital.offenburg.aktenplan.data.dto;

public class OsTicket {
    private final boolean autorespond;
    private final String source;
    private String name;
    private String email;
    private String subject;
    private String message;

    public OsTicket() {
        this.autorespond = false;
        this.source = "API";
    }

    public boolean isAutorespond() {
        return this.autorespond;
    }

    public String getSource() {
        return this.source;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(final String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

}
