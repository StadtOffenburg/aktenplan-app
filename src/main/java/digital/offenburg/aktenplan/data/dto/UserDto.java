package digital.offenburg.aktenplan.data.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.ObjectUtils;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

import digital.offenburg.aktenplan.data.constant.Role;

public class UserDto implements UserDetails, OidcUser, Serializable {
    private String id;
    @Email
    @NotEmpty
    private String email;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    @NotNull
    private Role role;
    
    @NotEmpty
    @Size(min = 5, max = 100)
    private String inputPassword;
    @NotEmpty
    @Size(min = 5, max = 100)
    private String confirmPassword;
    
    private Collection<? extends GrantedAuthority> authorities;
    private OidcIdToken idToken;
    private String sessionId;
    
    public static UserDto from(final UserRepresentation userRepresentation) {
        final UserDto userDto = new UserDto();
        userDto.setId(userRepresentation.getId());
        userDto.setEmail(userRepresentation.getEmail());
        userDto.setFirstName(userRepresentation.getFirstName());
        userDto.setLastName(userRepresentation.getLastName());
        
        final Role role = Optional.ofNullable(userRepresentation.getRealmRoles())
                                  .orElseGet(Collections::emptyList)
                                  .stream()
                                  .map(Role::valueOf)
                                  .filter(Objects::nonNull)
                                  .findFirst()
                                  .orElse(null);
        userDto.setRole(role);
        
        return userDto;
    }
    
    public UserRepresentation toUserRepresentation() {
        final UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setId(this.id);
        userRepresentation.setEmail(this.email);
        userRepresentation.setFirstName(this.firstName);
        userRepresentation.setLastName(this.lastName);
        userRepresentation.setRealmRoles(Collections.singletonList(this.role.getDescription()));
        
        userRepresentation.setUsername(this.email);
        userRepresentation.setEnabled(Boolean.TRUE);
        userRepresentation.setEmailVerified(Boolean.TRUE);
        return userRepresentation;
    }
    
    public void updateCredentials(final UserRepresentation userRepresentation) {
        final CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(this.inputPassword);
        userRepresentation.setCredentials(Collections.singletonList(credentialRepresentation));
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    @Override
    public String getEmail() {
        return this.email;
    }
    
    public String getFirstName() {
        return this.firstName;
    }
    
    public String getLastName() {
        return this.lastName;
    }
    
    public String getInputPassword() {
        return this.inputPassword;
    }
    
    public String getConfirmPassword() {
        return this.confirmPassword;
    }
    
    public void setEmail(final String email) {
        this.email = email;
    }
    
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }
    
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }
    
    public void setInputPassword(final String inputPassword) {
        this.inputPassword = inputPassword;
    }
    
    public void setConfirmPassword(final String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
    
    public Role getRole() {
        return this.role;
    }
    
    public void setRole(final Role role) {
        this.role = role;
    }
    
    @Override
    public String getUsername() {
        return this.email;
    }
    
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (this.authorities != null) {
            return this.authorities;
        }
        return Stream.of(this.role)
                     .map(role -> new SimpleGrantedAuthority("ROLE_" + role))
                     .toList();
    }
    
    public void setAuthorities(final Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }
    
    @Override
    public String getPassword() {
        return null;
    }
    
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    
    @Override
    public boolean isEnabled() {
        return true;
    }
    
    public String name() {
        return Stream.of(this.firstName, this.lastName).filter(ObjectUtils::isNotEmpty).collect(Collectors.joining(" "));
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final UserDto other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }
    
    @Override
    public Map<String, Object> getAttributes() {
        return Collections.emptyMap();
    }
    
    @Override
    public String getName() {
        return null;
    }
    
    @Override
    public Map<String, Object> getClaims() {
        return Collections.emptyMap();
    }
    
    @Override
    public OidcUserInfo getUserInfo() {
        return null;
    }
    
    @Override
    public OidcIdToken getIdToken() {
        return this.idToken;
    }
    
    public void setIdToken(final OidcIdToken idToken) {
        this.idToken = idToken;
    }
    
    public String getSessionId() {
        return this.sessionId;
    }
    
    public void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
    }
    
}
