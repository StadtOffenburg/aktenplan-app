package digital.offenburg.aktenplan.data.dto;

import java.io.Serializable;
import java.util.Objects;

import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;

public class UserFavouriteDto implements Serializable {
    private UserFavouriteIdDto id;
    private UserDto user;
    private FileReferenceDto fileReference;

    public UserFavouriteIdDto getId() {
        return this.id;
    }

    public UserDto getUser() {
        return this.user;
    }

    public FileReferenceDto getFileReference() {
        return this.fileReference;
    }

    public void setFileReference(final FileReferenceDto fileReference) {
        this.fileReference = fileReference;
    }

    public void setId(final UserFavouriteIdDto id) {
        this.id = id;
    }

    public void setUser(final UserDto user) {
        this.user = user;
    }

    public static class UserFavouriteIdDto implements Serializable {
        private String userId;
        private String fileReferenceId;

        public String getUserId() {
            return this.userId;
        }

        public void setUserId(final String userId) {
            this.userId = userId;
        }

        public String getFileReferenceId() {
            return this.fileReferenceId;
        }

        public void setFileReferenceId(final String fileReferenceId) {
            this.fileReferenceId = fileReferenceId;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.fileReferenceId, this.userId);
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof final UserFavouriteIdDto other)) {
                return false;
            }
            return this.fileReferenceId != null && Objects.equals(this.fileReferenceId, other.fileReferenceId) && Objects.equals(this.userId, other.userId);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final UserFavouriteDto other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }

}
