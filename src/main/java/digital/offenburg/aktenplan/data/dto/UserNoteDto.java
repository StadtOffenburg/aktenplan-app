package digital.offenburg.aktenplan.data.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.validation.constraints.NotEmpty;

import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;

public class UserNoteDto implements Serializable {
    private Long id;
    private UserDto user;
    @NotEmpty
    private String title;
    @NotEmpty
    private String text;
    private LocalDateTime timestamp;
    private String planVersion;
    private FileReferenceDto fileReference;

    public UserNoteDto() {
        this.id = null;
        this.user = null;
        this.title = null;
        this.text = null;
        this.timestamp = null;
        this.planVersion = null;
        this.fileReference = null;
    }

    public Long getId() {
        return this.id;
    }

    public UserDto getUser() {
        return this.user;
    }

    public String getTitle() {
        return this.title;
    }

    public String getText() {
        return this.text;
    }

    public LocalDateTime getTimestamp() {
        return this.timestamp;
    }

    public String getPlanVersion() {
        return this.planVersion;
    }

    public FileReferenceDto getFileReference() {
        return this.fileReference;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setUser(final UserDto user) {
        this.user = user;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public void setTimestamp(final LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public void setPlanVersion(final String planVersion) {
        this.planVersion = planVersion;
    }

    public void setFileReference(final FileReferenceDto fileReference) {
        this.fileReference = fileReference;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final UserNoteDto other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }

}
