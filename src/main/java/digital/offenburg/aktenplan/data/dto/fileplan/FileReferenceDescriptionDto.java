package digital.offenburg.aktenplan.data.dto.fileplan;

import java.io.Serializable;
import java.util.Objects;

public class FileReferenceDescriptionDto implements Serializable {
    private Long id;
    private String description;

    public FileReferenceDescriptionDto() {
        this.id = null;
        this.description = null;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final FileReferenceDescriptionDto other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }

}
