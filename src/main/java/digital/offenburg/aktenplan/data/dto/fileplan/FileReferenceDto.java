package digital.offenburg.aktenplan.data.dto.fileplan;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import digital.offenburg.aktenplan.FileReferenceUtil;
import digital.offenburg.aktenplan.data.entity.Guidance;

public class FileReferenceDto implements Serializable {
    private String id;
    private String refId;
    private String labelRef;
    private String title;
    private String schemaRefId;
    private String parentId;
    private String version;
    private String mainGroupType;
    private Set<FileReferenceDto> subReference;
    private Set<FileReferenceDescriptionDto> descriptions;
    private Set<ReferenceLinksDto> links;
    private Set<MoreInformationDto> informations;
    private Set<Guidance> guidances;
    private boolean selected;
    private boolean userFavourite;
    private boolean active;
    private boolean searchMatchedInDetails;

    public FileReferenceDto() {
        this.id = null;
        this.refId = null;
        this.labelRef = null;
        this.title = null;
        this.schemaRefId = null;
        this.parentId = null;
        this.version = null;
        this.descriptions = new HashSet<>();
        this.links = new HashSet<>();
        this.subReference = new HashSet<>();
        this.informations = new HashSet<>();
        this.guidances = new HashSet<>();
        this.selected = false;
        this.userFavourite = false;
        this.active = false;
        this.searchMatchedInDetails = false;
    }

    public void merge(final FileReferenceDto fileReference) {
        if (fileReference != null) {
            final Set<FileReferenceDto> mergedSubReferences = FileReferenceUtil.merge(this.getSubReference(), fileReference.getSubReference());
            this.subReference.clear();
            this.subReference.addAll(mergedSubReferences);
        }
    }

    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getRefId() {
        return this.refId;
    }

    public void setRefId(final String refId) {
        this.refId = refId;
    }

    public String getLabelRef() {
        return this.labelRef;
    }

    public void setLabelRef(final String labelRef) {
        this.labelRef = labelRef;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public Set<FileReferenceDescriptionDto> getDescriptions() {
        return this.descriptions;
    }

    public void setDescriptions(final Set<FileReferenceDescriptionDto> descriptions) {
        this.descriptions = descriptions;
    }

    public Set<ReferenceLinksDto> getLinks() {
        return this.links;
    }

    public void setLinks(final Set<ReferenceLinksDto> links) {
        this.links = links;
    }

    public Set<FileReferenceDto> getSubReference() {
        return this.subReference;
    }

    public void setSubReference(final Set<FileReferenceDto> subReference) {
        this.subReference = subReference;
    }

    public Set<MoreInformationDto> getInformations() {
        return this.informations;
    }

    public void setInformations(final Set<MoreInformationDto> informations) {
        this.informations = informations;
    }

    public String getSchemaRefId() {
        return this.schemaRefId;
    }

    public void setSchemaRefId(final String schemaRefId) {
        this.schemaRefId = schemaRefId;
    }

    public String getParentId() {
        return this.parentId;
    }

    public void setParentId(final String parentId) {
        this.parentId = parentId;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public String getMainGroupType() {
        return this.mainGroupType;
    }

    public void setMainGroupType(final String mainGroupType) {
        this.mainGroupType = mainGroupType;
    }

    public Set<Guidance> getGuidances() {
        return this.guidances;
    }

    public void setGuidances(final Set<Guidance> guidances) {
        this.guidances = guidances;
    }

    public boolean isSelected() {
        return this.selected;
    }

    public FileReferenceDto withSelectedFor(final String id) {
        this.selected = id != null && id.equals(this.id);
        return this;
    }

    public boolean isUserFavourite() {
        return this.userFavourite;
    }

    public void setUserFavourite(final boolean userFavourite) {
        this.userFavourite = userFavourite;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(final boolean active) {
        this.active = active;
    }

    public boolean isSearchMatchedInDetails() {
        return this.searchMatchedInDetails;
    }

    public void setSearchMatchedInDetails(final boolean searchMatchedInDetails) {
        this.searchMatchedInDetails = searchMatchedInDetails;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final FileReferenceDto other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }

}
