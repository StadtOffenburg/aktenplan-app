package digital.offenburg.aktenplan.data.dto.fileplan;

import java.io.Serializable;
import java.util.Objects;

public class MoreInformationDto implements Serializable {
    private Long id;
    private String text;

    public MoreInformationDto() {
        this.id = null;
        this.text = null;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getText() {
        return this.text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final MoreInformationDto other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }

}
