package digital.offenburg.aktenplan.data.dto.fileplan;

import java.io.Serializable;
import java.util.Objects;

public class ReferenceLinksDto implements Serializable {
    private Long id;
    private String type;
    private String target;
    private String label;

    public ReferenceLinksDto() {
        this.id = null;
        this.type = null;
        this.target = null;
        this.label = null;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getType() {
        return this.type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getTarget() {
        return this.target;
    }

    public void setTarget(final String target) {
        this.target = target;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final ReferenceLinksDto other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }

}
