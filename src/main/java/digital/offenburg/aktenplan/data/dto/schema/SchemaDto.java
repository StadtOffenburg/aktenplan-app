package digital.offenburg.aktenplan.data.dto.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SchemaDto implements Serializable {
    private String id;
    private boolean active;
    private String name;
    private List<SchemaEntryDto> entries;
    private String hint;
    private String version;

    public SchemaDto() {
        this.id = null;
        this.active = false;
        this.name = null;
        this.entries = new ArrayList<>();
        this.hint = null;
        this.version = null;
    }

    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(final boolean active) {
        this.active = active;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public List<SchemaEntryDto> getEntries() {
        return this.entries;
    }

    public void setEntries(final List<SchemaEntryDto> entries) {
        this.entries = entries;
    }

    public String getHint() {
        return this.hint;
    }

    public void setHint(final String hint) {
        this.hint = hint;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final SchemaDto other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }

}
