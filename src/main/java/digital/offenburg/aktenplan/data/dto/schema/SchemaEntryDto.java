package digital.offenburg.aktenplan.data.dto.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import digital.offenburg.aktenplan.data.entity.schema.Schema;

public class SchemaEntryDto implements Serializable {
    private SchemaEntryIdDto id;
    private Schema schema;
    private String text;
    private List<SchemaEntryDto> subEntry;
    private String parentEntryNumber;

    public SchemaEntryDto() {
        this.id = null;
        this.schema = null;
        this.text = null;
        this.subEntry = new ArrayList<>();
        this.parentEntryNumber = null;
    }

    public Optional<SchemaEntryIdDto> parentId() {
        return Optional.ofNullable(this.parentEntryNumber)
                       .map(pId -> new SchemaEntryIdDto(pId, this.schema.getId()));
    }

    public SchemaEntryIdDto getId() {
        return this.id;
    }

    public void setId(final SchemaEntryIdDto id) {
        this.id = id;
    }

    public Schema getSchema() {
        return this.schema;
    }

    public void setSchema(final Schema schema) {
        this.schema = schema;
    }

    public String getText() {
        return this.text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public List<SchemaEntryDto> getSubEntry() {
        return this.subEntry;
    }

    public void setSubEntry(final List<SchemaEntryDto> subEntry) {
        this.subEntry = subEntry;
    }

    public String getParentEntryNumber() {
        return this.parentEntryNumber;
    }

    public void setParentEntryNumber(final String parentEntryNumber) {
        this.parentEntryNumber = parentEntryNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final SchemaEntryDto other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }

}
