package digital.offenburg.aktenplan.data.dto.schema;

import java.io.Serializable;
import java.util.Objects;

public class SchemaEntryIdDto implements Serializable {
    private String entryNumber;
    private String schemaId;

    public SchemaEntryIdDto() {
    }

    public SchemaEntryIdDto(final String entryNumber, final String schemaId) {
        this.entryNumber = entryNumber;
        this.schemaId = schemaId;
    }

    public String getEntryNumber() {
        return this.entryNumber;
    }

    public void setEntryNumber(final String entryNumber) {
        this.entryNumber = entryNumber;
    }

    public String getSchemaId() {
        return this.schemaId;
    }

    public void setSchemaId(final String schemaId) {
        this.schemaId = schemaId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.entryNumber, this.schemaId);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final SchemaEntryIdDto other)) {
            return false;
        }
        return this.entryNumber != null && Objects.equals(this.entryNumber, other.entryNumber) && Objects.equals(this.schemaId, other.schemaId);
    }

}
