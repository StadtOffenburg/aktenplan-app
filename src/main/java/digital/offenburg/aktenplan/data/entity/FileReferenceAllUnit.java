package digital.offenburg.aktenplan.data.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class FileReferenceAllUnit implements Serializable {

    @EmbeddedId
    private FileReferenceAllUnitKey id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("fileReferenceId")
    @JoinColumn(insertable = false, updatable = false, nullable = false)
    private FileReference fileReference;

    @NotNull
    @LastModifiedDate
    private LocalDateTime timestamp;

    @NotNull
    @CreatedBy
    @LastModifiedBy
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public FileReferenceAllUnitKey getId() {
        return this.id;
    }

    public void setId(final FileReferenceAllUnitKey id) {
        this.id = id;
    }

    public LocalDateTime getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(final LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

    public FileReference getFileReference() {
        return this.fileReference;
    }

    public void setFileReference(final FileReference fileReference) {
        this.fileReference = fileReference;
    }

    @Embeddable
    public static class FileReferenceAllUnitKey implements Serializable {

        private String fileReferenceId;

        public FileReferenceAllUnitKey() {
        }

        public FileReferenceAllUnitKey(final String fileReferenceId) {
            this.fileReferenceId = fileReferenceId;
        }

        public String getFileReferenceId() {
            return this.fileReferenceId;
        }

        public void setFileReferenceId(final String fileReferenceId) {
            this.fileReferenceId = fileReferenceId;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.fileReferenceId);
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof final FileReferenceAllUnitKey other)) {
                return false;
            }
            return this.fileReferenceId != null && Objects.equals(this.fileReferenceId, other.fileReferenceId);
        }

    }

}
