package digital.offenburg.aktenplan.data.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class FileReferenceUnit {
    @EmbeddedId
    private FileReferenceUnitId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("unitId")
    @JoinColumn(insertable = false, updatable = false, nullable = false)
    private AdministrativeUnit unit;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("fileReferenceId")
    @JoinColumn(insertable = false, updatable = false, nullable = false)
    private FileReference fileReference;

    @NotNull
    @LastModifiedDate
    private LocalDateTime timestamp;

    @NotNull
    @CreatedBy
    @LastModifiedBy
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public FileReferenceUnit() {
    }

    public FileReferenceUnit(final AdministrativeUnit unit, final FileReference fileReference) {
        this.id = new FileReferenceUnitId(unit.getId(), fileReference.getId());
        this.unit = unit;
        this.fileReference = fileReference;
    }

    public FileReferenceUnitId getId() {
        return this.id;
    }

    public void setId(final FileReferenceUnitId id) {
        this.id = id;
    }

    public AdministrativeUnit getUnit() {
        return this.unit;
    }

    public void setUnit(final AdministrativeUnit unit) {
        this.unit = unit;
    }

    public FileReference getFileReference() {
        return this.fileReference;
    }

    public void setFileReference(final FileReference fileReference) {
        this.fileReference = fileReference;
    }

    @Embeddable
    public static class FileReferenceUnitId implements Serializable {
        private Long unitId;
        private String fileReferenceId;

        public FileReferenceUnitId() {
        }

        public FileReferenceUnitId(final Long unitId, final String fileReferenceId) {
            this.unitId = unitId;
            this.fileReferenceId = fileReferenceId;
        }

        public Long getUnitId() {
            return this.unitId;
        }

        public void setUnitId(final Long unitId) {
            this.unitId = unitId;
        }

        public String getFileReferenceId() {
            return this.fileReferenceId;
        }

        public void setFileReferenceId(final String fileReferenceId) {
            this.fileReferenceId = fileReferenceId;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.fileReferenceId, this.unitId);
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof final FileReferenceUnitId other)) {
                return false;
            }
            return this.fileReferenceId != null && Objects.equals(this.fileReferenceId, other.fileReferenceId) && Objects.equals(this.unitId, other.unitId);
        }

    }
}
