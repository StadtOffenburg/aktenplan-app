package digital.offenburg.aktenplan.data.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class GuidanceRequest implements Serializable {
    @Id
    @Column(updatable = false)
    private Long id;

    @NotNull
    @Column(length = 1000)
    private String title;

    @NotNull
    @Column(length = 4000)
    private String text;

    @NotNull
    private String planVersion;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "id", nullable = false)
    private FileReference fileReference;

    @NotNull
    @LastModifiedDate
    private LocalDateTime timestamp;

    @NotNull
    @CreatedBy
    @LastModifiedBy
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public GuidanceRequest() {
        this.id = null;
        this.title = null;
        this.text = null;
        this.planVersion = null;
        this.fileReference = null;
        this.timestamp = null;
        this.user = null;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getText() {
        return this.text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getPlanVersion() {
        return this.planVersion;
    }

    public void setPlanVersion(final String planVersion) {
        this.planVersion = planVersion;
    }

    public FileReference getFileReference() {
        return this.fileReference;
    }

    public void setFileReference(final FileReference fileReference) {
        this.fileReference = fileReference;
    }

    public LocalDateTime getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(final LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final GuidanceRequest other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }

}
