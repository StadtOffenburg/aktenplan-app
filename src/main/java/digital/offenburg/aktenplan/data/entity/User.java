package digital.offenburg.aktenplan.data.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import digital.offenburg.aktenplan.data.constant.Role;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class User implements Serializable {
    @Id
    @Column(updatable = false, nullable = false)
    private String id;
    @NotNull
    @Column(unique = true)
    private String email;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    
    @Enumerated(EnumType.STRING)
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<Role> roles;
    
    @NotNull
    @LastModifiedDate
    private LocalDateTime timestamp;
    
    public User() {
        this.id = null;
        this.email = null;
        this.firstName = null;
        this.lastName = null;
        this.roles = new HashSet<>();
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public String getEmail() {
        return this.email;
    }
    
    public String getFirstName() {
        return this.firstName;
    }
    
    public String getLastName() {
        return this.lastName;
    }
    
    public void setEmail(final String email) {
        this.email = email;
    }
    
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }
    
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }
    
    public Set<Role> getRoles() {
        return this.roles;
    }
    
    public void setRoles(final Set<Role> roles) {
        this.roles = roles;
    }
    
    public LocalDateTime getTimestamp() {
        return this.timestamp;
    }
    
    public void setTimestamp(final LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final User other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }
    
}
