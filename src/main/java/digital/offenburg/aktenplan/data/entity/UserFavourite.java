package digital.offenburg.aktenplan.data.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;

@Entity
public class UserFavourite {
    @EmbeddedId
    private UserFavouriteId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    @JoinColumn(insertable = false, updatable = false, nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("fileReferenceId")
    @JoinColumn(insertable = false, updatable = false, nullable = false)
    private FileReference fileReference;

    public UserFavourite() {
    }

    public UserFavourite(final User user, final FileReference fileReference) {
        this.id = new UserFavouriteId(user.getId(), fileReference.getId());
        this.user = user;
        this.fileReference = fileReference;
    }

    public UserFavouriteId getId() {
        return this.id;
    }

    public void setId(final UserFavouriteId id) {
        this.id = id;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

    public FileReference getFileReference() {
        return this.fileReference;
    }

    public void setFileReference(final FileReference fileReference) {
        this.fileReference = fileReference;
    }

    @Embeddable
    public static class UserFavouriteId implements Serializable {
        private String userId;
        private String fileReferenceId;

        public UserFavouriteId() {
        }

        public UserFavouriteId(final String userId, final String fileReferenceId) {
            this.userId = userId;
            this.fileReferenceId = fileReferenceId;
        }

        public String getUserId() {
            return this.userId;
        }

        public void setUserId(final String userId) {
            this.userId = userId;
        }

        public String getFileReferenceId() {
            return this.fileReferenceId;
        }

        public void setFileReferenceId(final String fileReferenceId) {
            this.fileReferenceId = fileReferenceId;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.fileReferenceId, this.userId);
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof final UserFavouriteId other)) {
                return false;
            }
            return this.fileReferenceId != null && Objects.equals(this.fileReferenceId, other.fileReferenceId) && Objects.equals(this.userId, other.userId);
        }

    }
}
