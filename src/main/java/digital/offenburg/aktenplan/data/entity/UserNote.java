package digital.offenburg.aktenplan.data.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class UserNote extends AbstractId {
    @NotNull
    @Column(length = 1000)
    private String title;
    @NotNull
    @Column(length = 4000)
    private String text;
    @NotNull
    @LastModifiedDate
    private LocalDateTime timestamp;
    @NotNull
    private String planVersion;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "id", nullable = false)
    private FileReference fileReference;

    @NotNull
    @CreatedBy
    @LastModifiedBy
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public UserNote() {
        this.title = null;
        this.text = null;
        this.timestamp = null;
        this.planVersion = null;
        this.fileReference = null;
        this.user = null;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getText() {
        return this.text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public LocalDateTime getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(final LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getPlanVersion() {
        return this.planVersion;
    }

    public void setPlanVersion(final String planVersion) {
        this.planVersion = planVersion;
    }

    public FileReference getFileReference() {
        return this.fileReference;
    }

    public void setFileReference(final FileReference fileReference) {
        this.fileReference = fileReference;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

}
