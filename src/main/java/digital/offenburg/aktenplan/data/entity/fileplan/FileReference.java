package digital.offenburg.aktenplan.data.entity.fileplan;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class FileReference implements Serializable {
    @Id
    @Column(updatable = false, nullable = false)
    private String id;

    @NotNull
    private String refId;

    @NotNull
    private String labelRef;

    @NotNull
    private String title;
    private String schemaRefId;

    @NotNull
    private String version;

    @NotNull
    private String mainGroupType;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private Set<FileReference> subReference;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "file_reference_id", nullable = false)
    private Set<FileReferenceDescription> descriptions;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "file_reference_id", nullable = false)
    private Set<ReferenceLinks> links;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "file_reference_id", nullable = false)
    private Set<MoreInformation> informations;

    @Column(name = "parent_id", updatable = false, insertable = false)
    private String parentId;

    @NotNull
    private boolean active;

    public FileReference() {
        this.refId = null;
        this.labelRef = null;
        this.id = null;
        this.title = null;
        this.schemaRefId = null;
        this.parentId = null;
        this.version = null;
        this.mainGroupType = null;
        this.descriptions = new HashSet<>();
        this.links = new HashSet<>();
        this.subReference = new HashSet<>();
        this.informations = new HashSet<>();
        this.active = true;
    }

    public String getRefId() {
        return this.refId;
    }

    public void setRefId(final String refId) {
        this.refId = refId;
    }

    public String getLabelRef() {
        return this.labelRef;
    }

    public void setLabelRef(final String labelRef) {
        this.labelRef = labelRef;
    }

    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public Set<FileReferenceDescription> getDescriptions() {
        return this.descriptions;
    }

    public void setDescriptions(final Set<FileReferenceDescription> descriptions) {
        this.descriptions = descriptions;
    }

    public Set<ReferenceLinks> getLinks() {
        return this.links;
    }

    public void setLinks(final Set<ReferenceLinks> links) {
        this.links = links;
    }

    public Set<FileReference> getSubReference() {
        return this.subReference;
    }

    public void setSubReference(final Set<FileReference> subReference) {
        this.subReference = subReference;
    }

    public Set<MoreInformation> getInformations() {
        return this.informations;
    }

    public void setInformations(final Set<MoreInformation> informations) {
        this.informations = informations;
    }

    public String getSchemaRefId() {
        return this.schemaRefId;
    }

    public void setSchemaRefId(final String schemaRefId) {
        this.schemaRefId = schemaRefId;
    }

    public String getParentId() {
        return this.parentId;
    }

    public void setParentId(final String parentId) {
        this.parentId = parentId;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(final boolean active) {
        this.active = active;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public String getMainGroupType() {
        return this.mainGroupType;
    }

    public void setMainGroupType(final String mainGroupType) {
        this.mainGroupType = mainGroupType;
    }

}
