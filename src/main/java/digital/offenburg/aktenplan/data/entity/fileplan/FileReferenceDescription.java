package digital.offenburg.aktenplan.data.entity.fileplan;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import digital.offenburg.aktenplan.data.entity.AbstractId;

@Entity
public class FileReferenceDescription extends AbstractId {
    @NotNull
    @Column(length = 4000)
    private String description;

    public FileReferenceDescription() {
        this.description = null;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

}
