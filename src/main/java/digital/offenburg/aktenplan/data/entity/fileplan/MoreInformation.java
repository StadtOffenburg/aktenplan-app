package digital.offenburg.aktenplan.data.entity.fileplan;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import digital.offenburg.aktenplan.data.entity.AbstractId;

@Entity
public class MoreInformation extends AbstractId {
    @NotNull
    @Column(length = 4000)
    private String text;

    public MoreInformation() {
        this.text = null;
    }

    public String getText() {
        return this.text;
    }

    public void setText(final String text) {
        this.text = text;
    }

}
