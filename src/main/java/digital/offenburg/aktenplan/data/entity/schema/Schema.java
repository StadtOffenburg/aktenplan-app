package digital.offenburg.aktenplan.data.entity.schema;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Schema implements Serializable {
    @Id
    @Column(updatable = false, nullable = false)
    private String id;
    @NotNull
    private boolean active;
    @NotNull
    private String name;
    @Column(length = 4000)
    private String hint;
    @NotNull
    private String version;

    public Schema() {
        this.id = null;
        this.active = false;
        this.name = null;
        this.hint = null;
        this.version = null;
    }

    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(final boolean active) {
        this.active = active;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getHint() {
        return this.hint;
    }

    public void setHint(final String hint) {
        this.hint = hint;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final Schema other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }

}
