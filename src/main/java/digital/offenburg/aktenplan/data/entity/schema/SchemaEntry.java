package digital.offenburg.aktenplan.data.entity.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class SchemaEntry implements Serializable {

    @EmbeddedId
    private SchemaEntryId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("schemaId")
    @JoinColumn(insertable = false, updatable = false, nullable = false)
    private Schema schema;

    @NotNull
    @Column(length = 4000)
    private String text;

    @OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "parent_entry_number")
    @JoinColumn(name = "schema_id")
    private List<SchemaEntry> subEntry;

    @Column(name = "parent_entry_number", updatable = false, insertable = false)
    private String parentEntryNumber;

    public SchemaEntry() {
        this.schema = null;
        this.text = null;
        this.subEntry = new ArrayList<>();
        this.parentEntryNumber = null;
    }

    public SchemaEntryId getId() {
        return this.id;
    }

    public void setId(final SchemaEntryId id) {
        this.id = id;
    }

    public Schema getSchema() {
        return this.schema;
    }

    public void setSchema(final Schema schema) {
        this.schema = schema;
    }

    public String getText() {
        return this.text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public List<SchemaEntry> getSubEntry() {
        return this.subEntry;
    }

    public void setSubEntry(final List<SchemaEntry> subEntry) {
        this.subEntry = subEntry;
    }

    public String getParentEntryNumber() {
        return this.parentEntryNumber;
    }

    public void setParentEntryNumber(final String parentEntryNumber) {
        this.parentEntryNumber = parentEntryNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final SchemaEntry other)) {
            return false;
        }
        return this.id != null && Objects.equals(this.id, other.id);
    }

    @Embeddable
    public static class SchemaEntryId implements Serializable {
        private String entryNumber;
        private String schemaId;

        public SchemaEntryId() {
        }

        public SchemaEntryId(final String entryNumber, final String schemaId) {
            this.entryNumber = entryNumber;
            this.schemaId = schemaId;
        }

        public String getEntryNumber() {
            return this.entryNumber;
        }

        public void setEntryNumber(final String entryNumber) {
            this.entryNumber = entryNumber;
        }

        public String getSchemaId() {
            return this.schemaId;
        }

        public void setSchemaId(final String schemaId) {
            this.schemaId = schemaId;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.entryNumber, this.schemaId);
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof final SchemaEntryId other)) {
                return false;
            }
            return this.entryNumber != null && Objects.equals(this.entryNumber, other.entryNumber) && Objects.equals(this.schemaId, other.schemaId);
        }

    }

}
