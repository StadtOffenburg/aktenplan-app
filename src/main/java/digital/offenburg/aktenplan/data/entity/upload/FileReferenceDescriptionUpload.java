package digital.offenburg.aktenplan.data.entity.upload;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import digital.offenburg.aktenplan.data.entity.AbstractId;

@Entity
public class FileReferenceDescriptionUpload extends AbstractId {
    @NotNull
    @Column(length = 4000)
    private String description;

    public FileReferenceDescriptionUpload() {
        this.description = null;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

}
