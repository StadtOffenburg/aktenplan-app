package digital.offenburg.aktenplan.data.entity.upload;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import digital.offenburg.aktenplan.data.entity.AbstractId;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "file_upload_id", "labelId" }) })
public class FileReferenceUpload extends AbstractId {
    @NotNull
    private String refId;

    @NotNull
    private String labelRef;

    @NotNull
    private String labelId;

    @NotNull
    private String title;

    private String schemaRefId;

    @NotNull
    private String version;

    @NotNull
    private String mainGroupType;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private Set<FileReferenceUpload> subReference;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "file_reference_id", nullable = false)
    private Set<FileReferenceDescriptionUpload> descriptions;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "file_reference_id", nullable = false)
    private Set<ReferenceLinksUpload> links;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "file_reference_id", nullable = false)
    private Set<MoreInformationUpload> informations;

    @Column(name = "parent_id", updatable = false, insertable = false)
    private Long parentId;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private FileUpload fileUpload;

    public FileReferenceUpload() {
        this.refId = null;
        this.labelRef = null;
        this.labelId = null;
        this.title = null;
        this.schemaRefId = null;
        this.parentId = null;
        this.version = null;
        this.mainGroupType = null;
        this.descriptions = new HashSet<>();
        this.links = new HashSet<>();
        this.subReference = new HashSet<>();
        this.informations = new HashSet<>();
        this.fileUpload = null;
    }

    public String getRefId() {
        return this.refId;
    }

    public void setRefId(final String refId) {
        this.refId = refId;
    }

    public String getLabelRef() {
        return this.labelRef;
    }

    public void setLabelRef(final String labelRef) {
        this.labelRef = labelRef;
    }

    public String getLabelId() {
        return this.labelId;
    }

    public void setLabelId(final String labelId) {
        this.labelId = labelId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public Set<FileReferenceDescriptionUpload> getDescriptions() {
        return this.descriptions;
    }

    public void setDescriptions(final Set<FileReferenceDescriptionUpload> descriptions) {
        this.descriptions = descriptions;
    }

    public Set<ReferenceLinksUpload> getLinks() {
        return this.links;
    }

    public void setLinks(final Set<ReferenceLinksUpload> links) {
        this.links = links;
    }

    public Set<FileReferenceUpload> getSubReference() {
        return this.subReference;
    }

    public void setSubReference(final Set<FileReferenceUpload> subReference) {
        this.subReference = subReference;
    }

    public Set<MoreInformationUpload> getInformations() {
        return this.informations;
    }

    public void setInformations(final Set<MoreInformationUpload> informations) {
        this.informations = informations;
    }

    public String getSchemaRefId() {
        return this.schemaRefId;
    }

    public void setSchemaRefId(final String schemaRefId) {
        this.schemaRefId = schemaRefId;
    }

    public Long getParentId() {
        return this.parentId;
    }

    public void setParentId(final Long parentId) {
        this.parentId = parentId;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public String getMainGroupType() {
        return this.mainGroupType;
    }

    public void setMainGroupType(final String mainGroupType) {
        this.mainGroupType = mainGroupType;
    }

    public FileUpload getFileUpload() {
        return this.fileUpload;
    }

    public void setFileUpload(final FileUpload fileUpload) {
        this.fileUpload = fileUpload;
    }

}
