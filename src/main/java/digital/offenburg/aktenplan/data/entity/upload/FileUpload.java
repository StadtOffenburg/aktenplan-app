package digital.offenburg.aktenplan.data.entity.upload;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import digital.offenburg.aktenplan.data.entity.AbstractId;
import digital.offenburg.aktenplan.data.entity.User;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class FileUpload extends AbstractId {
    @NotNull
    @Column(length = 400)
    private String fileName;

    @NotNull
    private String hash;

    @NotNull
    @CreatedBy
    @ManyToOne(fetch = FetchType.LAZY)
    private User uploadedBy;

    @NotNull
    @CreatedDate
    private LocalDateTime uploadedOn;

    public FileUpload() {
        this.fileName = null;
        this.hash = null;
        this.uploadedBy = null;
        this.uploadedOn = null;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    public String getHash() {
        return this.hash;
    }

    public void setHash(final String hash) {
        this.hash = hash;
    }

    public User getUploadedBy() {
        return this.uploadedBy;
    }

    public void setUploadedBy(final User uploadedBy) {
        this.uploadedBy = uploadedBy;
    }

    public LocalDateTime getUploadedOn() {
        return this.uploadedOn;
    }

    public void setUploadedOn(final LocalDateTime uploadedOn) {
        this.uploadedOn = uploadedOn;
    }

}
