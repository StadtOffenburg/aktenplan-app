package digital.offenburg.aktenplan.data.entity.upload;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import digital.offenburg.aktenplan.data.entity.AbstractId;

@Entity
public class ReferenceLinksUpload extends AbstractId {
    @NotNull
    private String type;
    @NotNull
    private String target;
    @NotNull
    @Column(length = 4000)
    private String label;
    
    public ReferenceLinksUpload() {
        this.type = null;
        this.target = null;
        this.label = null;
    }
    
    public String getType() {
        return this.type;
    }
    
    public void setType(final String type) {
        this.type = type;
    }
    
    public String getTarget() {
        return this.target;
    }
    
    public void setTarget(final String target) {
        this.target = target;
    }
    
    public String getLabel() {
        return this.label;
    }
    
    public void setLabel(final String label) {
        this.label = label;
    }
    
}
