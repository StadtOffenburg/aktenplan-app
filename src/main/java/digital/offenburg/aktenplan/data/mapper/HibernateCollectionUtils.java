package digital.offenburg.aktenplan.data.mapper;

import org.hibernate.Hibernate;
import org.hibernate.collection.spi.PersistentCollection;
import org.mapstruct.Condition;

public final class HibernateCollectionUtils {
    
    private HibernateCollectionUtils() {
    }
    
    @Condition
    public static boolean isObjectAvailable(final Object object) {
        if (object == null) {
            return false;
        }
        
        if (!(object instanceof final PersistentCollection pc)) {
            return Hibernate.isInitialized(object);
        }
        
        return pc.wasInitialized();
    }
    
}
