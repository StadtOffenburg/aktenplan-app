package digital.offenburg.aktenplan.data.mapper;

import java.util.Optional;
import java.util.Set;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.ReportingPolicy;

import digital.offenburg.aktenplan.data.dto.AdministrativeUnitDto;
import digital.offenburg.aktenplan.data.dto.FileReferenceUnitDto;
import digital.offenburg.aktenplan.data.dto.GuidanceDto;
import digital.offenburg.aktenplan.data.dto.GuidanceRequestDto;
import digital.offenburg.aktenplan.data.dto.UserDto;
import digital.offenburg.aktenplan.data.dto.UserFavouriteDto;
import digital.offenburg.aktenplan.data.dto.UserNoteDto;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDescriptionDto;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.dto.fileplan.MoreInformationDto;
import digital.offenburg.aktenplan.data.dto.fileplan.ReferenceLinksDto;
import digital.offenburg.aktenplan.data.dto.schema.SchemaDto;
import digital.offenburg.aktenplan.data.dto.schema.SchemaEntryDto;
import digital.offenburg.aktenplan.data.entity.AdministrativeUnit;
import digital.offenburg.aktenplan.data.entity.FileReferenceUnit;
import digital.offenburg.aktenplan.data.entity.Guidance;
import digital.offenburg.aktenplan.data.entity.GuidanceRequest;
import digital.offenburg.aktenplan.data.entity.User;
import digital.offenburg.aktenplan.data.entity.UserFavourite;
import digital.offenburg.aktenplan.data.entity.UserNote;
import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;
import digital.offenburg.aktenplan.data.entity.fileplan.FileReferenceDescription;
import digital.offenburg.aktenplan.data.entity.fileplan.MoreInformation;
import digital.offenburg.aktenplan.data.entity.fileplan.ReferenceLinks;
import digital.offenburg.aktenplan.data.entity.schema.Schema;
import digital.offenburg.aktenplan.data.entity.schema.SchemaEntry;
import digital.offenburg.aktenplan.data.entity.upload.FileReferenceUpload;
import digital.offenburg.aktenplan.data.entity.upload.FileUpload;
import digital.offenburg.aktenplan.data.xml.fileplan.FileReferenceXml;
import digital.offenburg.aktenplan.data.xml.fileplan.FileUploadXml;
import digital.offenburg.aktenplan.data.xml.schema.SchemaEntryXml;
import digital.offenburg.aktenplan.data.xml.schema.SchemaXml;

@Mapper(componentModel = "spring",
        uses = HibernateCollectionUtils.class,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValueIterableMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface MapStructMapper {

    FileReferenceDescriptionDto toDto(FileReferenceDescription fileReferenceDescription);

    FileReferenceDto toDto(FileReference fileReference);

    MoreInformationDto toDto(MoreInformation moreInformation);

    ReferenceLinksDto toDto(ReferenceLinks referenceLinks);

    GuidanceDto toDto(Guidance guidance);

    Guidance toEntity(GuidanceDto guidanceDto);

    Schema toEntity(SchemaXml schemaXml);

    SchemaDto toDto(Schema schema);

    SchemaEntry toEntity(SchemaEntryXml schemaEntryXml);

    SchemaEntryDto toDto(SchemaEntry schemaEntry);

    FileReference toEntity(FileReferenceDto fileReferenceDto);

    FileReference toEntity(FileReferenceXml fileReferenceXml);

    AdministrativeUnitDto toDto(AdministrativeUnit administrativeUnit);

    AdministrativeUnit toEntity(AdministrativeUnitDto administrativeUnit);

    UserDto toDto(User user);

    User toEntity(UserDto userDto);

    @AfterMapping
    default void additional(final User user, @MappingTarget final UserDto userDto) {
        user.getRoles().stream().findFirst().ifPresent(role -> {
            userDto.setRole(role);
        });
    }

    @AfterMapping
    default void additional(final UserDto userDto, @MappingTarget final User user) {
        Optional.ofNullable(userDto.getRole()).ifPresent(role -> {
            user.setRoles(Set.of(role));
        });
    }

    UserFavouriteDto toDto(UserFavourite userFavourite);

    UserFavourite toEntity(UserFavouriteDto userFavouriteDto);

    UserNoteDto toDto(UserNote userNote);

    UserNote toEntity(UserNoteDto userNoteDto);

    FileReferenceUnitDto toDto(FileReferenceUnit fileReferenceUnit);

    FileReferenceUnit toEntity(FileReferenceUnitDto fileReferenceUnitDto);

    FileUpload toUploadEntity(FileUploadXml fileUploadXml);

    FileReferenceUpload toUploadEntity(FileReferenceXml fileReferenceXml);

    FileReferenceDto toDto(FileReferenceUpload fileReferenceUpload);

    @AfterMapping
    default void additional(final FileReferenceUpload fileReferenceUpload, @MappingTarget final FileReferenceDto fileReferenceDto) {
        fileReferenceDto.setId(fileReferenceUpload.getLabelId());
        fileReferenceDto.setActive(true);
    }

    GuidanceRequest toEntity(GuidanceRequestDto guidanceRequestDto);

}