package digital.offenburg.aktenplan.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import digital.offenburg.aktenplan.data.entity.AdministrativeUnit;

@Repository
public interface AdministrativeUnitRepository extends JpaRepository<AdministrativeUnit, Long> {
}
