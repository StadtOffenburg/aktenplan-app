package digital.offenburg.aktenplan.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import digital.offenburg.aktenplan.data.entity.FileReferenceAllUnit;
import digital.offenburg.aktenplan.data.entity.FileReferenceAllUnit.FileReferenceAllUnitKey;

@Repository
public interface FileReferenceAllUnitRepository extends JpaRepository<FileReferenceAllUnit, FileReferenceAllUnitKey> {
    @Override
    @Modifying
    @Query("DELETE FROM FileReferenceAllUnit f WHERE f.id = :id")
    void deleteById(FileReferenceAllUnitKey id);
}
