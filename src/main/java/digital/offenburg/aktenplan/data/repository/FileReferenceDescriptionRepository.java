package digital.offenburg.aktenplan.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import digital.offenburg.aktenplan.data.entity.fileplan.FileReferenceDescription;

@Repository
public interface FileReferenceDescriptionRepository extends JpaRepository<FileReferenceDescription, Long> {

}
