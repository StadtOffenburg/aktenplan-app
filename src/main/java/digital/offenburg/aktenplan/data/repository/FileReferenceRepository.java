package digital.offenburg.aktenplan.data.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;
import digital.offenburg.aktenplan.data.entity.upload.FileUpload;

@Repository
public interface FileReferenceRepository extends JpaRepository<FileReference, String>, JpaSpecificationExecutor<FileReference> {

    @Query("SELECT f FROM FileReference f LEFT JOIN FETCH f.subReference LEFT JOIN FETCH f.descriptions LEFT JOIN FETCH f.links LEFT JOIN FETCH f.informations")
    Set<FileReference> fetchAllWithDetails();

    @Query("SELECT f FROM FileReference f LEFT JOIN FETCH f.descriptions LEFT JOIN FETCH f.links LEFT JOIN FETCH f.informations WHERE f.id = :id")
    FileReference fetchDetails(String id);

    @Query(value = """
                   WITH RECURSIVE cte_connect_by AS (
                               SELECT
                                       s.*
                               FROM
                                       t_file_reference s
                               where
                                       s.id = :id
                               UNION ALL
                               SELECT
                                       s.*
                               FROM
                                       cte_connect_by r
                               JOIN t_file_reference s ON
                                       r.id = s.parent_id
                               )
                               SELECT
                                       *
                               FROM
                                       cte_connect_by
                       """, nativeQuery = true)
    Set<FileReference> fetchAllChildren(String id);

    @Query(value = """
                   WITH RECURSIVE cte_connect_by AS (
                               SELECT
                                       s.*
                               FROM
                                       t_file_reference s
                               where
                                       s.id = :id
                               UNION ALL
                               SELECT
                                       s.*
                               FROM
                                       cte_connect_by r
                               JOIN t_file_reference s ON
                                       r.parent_id = s.id
                               )
                               SELECT
                                       *
                               FROM
                                       cte_connect_by
                       """, nativeQuery = true)
    Set<FileReference> fetchAllParents(String id);

    @Modifying
    @Query("update FileReference r set r.active=false where not exists (select f from FileReferenceUpload f where f.fileUpload=:fileUpload and f.labelId=r.id)")
    void markMissingReferencesInactive(FileUpload fileUpload);
}
