package digital.offenburg.aktenplan.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import digital.offenburg.aktenplan.data.entity.FileReferenceUnit;
import digital.offenburg.aktenplan.data.entity.FileReferenceUnit.FileReferenceUnitId;

@Repository
public interface FileReferenceUnitRepository extends JpaRepository<FileReferenceUnit, FileReferenceUnitId> {
    @Modifying
    @Query("delete from FileReferenceUnit f where f.id.fileReferenceId = :fileReferenceId")
    void deleteAllByFileReferenceId(String fileReferenceId);

    @Query("SELECT f FROM FileReferenceUnit f JOIN FETCH f.unit WHERE f.id.fileReferenceId = :fileReferenceId")
    List<FileReferenceUnit> findAllByFileReferenceId(String fileReferenceId);

    @Modifying
    @Query("delete from FileReferenceUnit f where f.id.fileReferenceId = :fileReferenceId and f.id.unitId not in (:unitIds)")
    void deleteDeTaggedUnitsForFileReferenceId(String fileReferenceId, List<Long> unitIds);
}
