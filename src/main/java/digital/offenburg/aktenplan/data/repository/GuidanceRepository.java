package digital.offenburg.aktenplan.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import digital.offenburg.aktenplan.data.entity.Guidance;
import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;

@Repository
public interface GuidanceRepository extends JpaRepository<Guidance, Long> {
    List<Guidance> findAllByFileReferenceOrderByTimestampDesc(FileReference fileReference);

    @Query("select g from Guidance g where g.fileReference = :fileReference and (g.title like %:searchText% or g.text like %:searchText%)")
    List<Guidance> findAllByFileReferenceAndText(FileReference fileReference, String searchText);

}
