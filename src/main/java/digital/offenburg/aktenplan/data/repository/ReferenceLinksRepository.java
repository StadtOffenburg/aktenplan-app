package digital.offenburg.aktenplan.data.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import digital.offenburg.aktenplan.data.entity.fileplan.ReferenceLinks;

@Repository
public interface ReferenceLinksRepository extends CrudRepository<ReferenceLinks, Long> {

    @Modifying
    @Query("update ReferenceLinks r SET r.label = (select f.title from FileReference f WHERE f.id = r.target ) WHERE r.type='unknown' ")
    void updateLinksOfTypeUnknown();
}
