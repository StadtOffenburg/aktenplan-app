package digital.offenburg.aktenplan.data.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import digital.offenburg.aktenplan.data.entity.schema.Schema;
import digital.offenburg.aktenplan.data.entity.schema.SchemaEntry;

@Repository
public interface SchemaEntryRepository extends CrudRepository<SchemaEntry, String> {
    List<SchemaEntry> findAllBySchema(Schema schema);
}
