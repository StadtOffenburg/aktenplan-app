package digital.offenburg.aktenplan.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import digital.offenburg.aktenplan.data.entity.schema.Schema;

@Repository
public interface SchemaRepository extends JpaRepository<Schema, String> {
}
