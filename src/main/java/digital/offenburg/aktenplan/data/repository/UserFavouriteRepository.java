package digital.offenburg.aktenplan.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import digital.offenburg.aktenplan.data.entity.UserFavourite;
import digital.offenburg.aktenplan.data.entity.UserFavourite.UserFavouriteId;

@Repository
public interface UserFavouriteRepository extends JpaRepository<UserFavourite, UserFavouriteId> {

    @Query("SELECT f FROM UserFavourite f JOIN FETCH f.fileReference WHERE f.id.userId = :userId and lower(f.fileReference.title) like concat('%',lower(:searchText),'%')")
    List<UserFavourite> findAllByUserId(String userId, String searchText);

    @Query("SELECT f FROM UserFavourite f WHERE f.id.userId = :userId")
    List<UserFavourite> findByUserIdWithNoDetails(String userId);
}
