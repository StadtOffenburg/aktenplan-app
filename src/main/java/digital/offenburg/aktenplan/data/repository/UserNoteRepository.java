package digital.offenburg.aktenplan.data.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import digital.offenburg.aktenplan.data.entity.User;
import digital.offenburg.aktenplan.data.entity.UserNote;
import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;

@Repository
public interface UserNoteRepository extends JpaRepository<UserNote, Long> {
    List<UserNote> findAllByFileReferenceAndUserOrderByTimestampDesc(FileReference fileReference, User user);

    @Query("select n from UserNote n where n.user = :user and n.fileReference = :fileReference and (n.title like %:searchText% or n.text like %:searchText%)")
    List<UserNote> findAllByUserAndFileReferenceAndText(User user, FileReference fileReference, String searchText);
}
