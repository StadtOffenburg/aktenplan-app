package digital.offenburg.aktenplan.data.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import digital.offenburg.aktenplan.data.entity.User;

public interface UserRepository extends JpaRepository<User, String> {

    Optional<User> findByEmail(String email);

    @Query("select u from User u where lower(concat(u.firstName,u.lastName,u.email)) like %:searchText%")
    List<User> searchUser(String searchText);
}