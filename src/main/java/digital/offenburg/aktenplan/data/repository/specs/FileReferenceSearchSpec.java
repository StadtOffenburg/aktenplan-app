package digital.offenburg.aktenplan.data.repository.specs;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.data.jpa.domain.Specification;

import digital.offenburg.aktenplan.data.dto.UserDto;
import digital.offenburg.aktenplan.data.entity.FileReferenceAllUnit;
import digital.offenburg.aktenplan.data.entity.FileReferenceUnit;
import digital.offenburg.aktenplan.data.entity.Guidance;
import digital.offenburg.aktenplan.data.entity.UserFavourite;
import digital.offenburg.aktenplan.data.entity.UserNote;
import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;
import digital.offenburg.aktenplan.data.view.BooleanSelection;
import digital.offenburg.aktenplan.data.view.SearchOptions;

public class FileReferenceSearchSpec implements Specification<FileReference> {

    private final UserDto userDto;
    private final SearchOptions searchOptions;

    public FileReferenceSearchSpec(final Optional<UserDto> userDto, final SearchOptions searchOptions) {
        this.userDto = userDto.orElse(null);
        this.searchOptions = searchOptions;
    }

    @Override
    public Predicate toPredicate(final Root<FileReference> root, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
        final List<Optional<Predicate>> predicates = List.of(this.textSearch(root, query, cb),
                                                             this.fileNumber(root, cb),
                                                             this.excludeInactive(root, cb),
                                                             this.guidance(root, query, cb),
                                                             this.favourites(root, query, cb),
                                                             this.administrativeUnits(root, query, cb));

        return cb.and(predicates.stream().filter(Optional::isPresent).map(Optional::get).toArray(Predicate[]::new));
    }

    private Optional<Predicate> administrativeUnits(final Root<FileReference> root, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
        Optional<Predicate> predicate = Optional.empty();
        if (this.searchOptions.getUnitSelect() != null) {
            final Subquery<FileReferenceUnit> unitSubquery = query.subquery(FileReferenceUnit.class);
            final Root<FileReferenceUnit> unitProfile = unitSubquery.from(FileReferenceUnit.class);
            unitSubquery.select(unitProfile);
            unitSubquery.where(cb.and(unitProfile.get("id").get("fileReferenceId").in(root.get("id")), unitProfile.get("id").get("unitId").in(this.searchOptions.getUnitSelect().getId())));

            final Subquery<FileReferenceAllUnit> allUnitSubquery = query.subquery(FileReferenceAllUnit.class);
            final Root<FileReferenceAllUnit> allUnitProfile = allUnitSubquery.from(FileReferenceAllUnit.class);
            allUnitSubquery.select(allUnitProfile);
            allUnitSubquery.where(allUnitProfile.get("id").get("fileReferenceId").in(root.get("id")));

            predicate = Optional.of(cb.or(cb.exists(unitSubquery), cb.exists(allUnitSubquery)));
        }
        return predicate;
    }

    private Optional<Predicate> favourites(final Root<FileReference> root, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
        Optional<Predicate> predicate = Optional.empty();
        if (this.userDto != null && this.searchOptions.getExcludeFavouritesSelect() == BooleanSelection.YES) {
            final Subquery<UserFavourite> userFavSubquery = query.subquery(UserFavourite.class);
            final Root<UserFavourite> userFavProfile = userFavSubquery.from(UserFavourite.class);
            userFavSubquery.select(userFavProfile);
            userFavSubquery.where(cb.and(userFavProfile.get("id").get("fileReferenceId").in(root.get("id")), userFavProfile.get("id").get("userId").in(this.userDto.getId())));
            predicate = Optional.of(cb.not(cb.exists(userFavSubquery)));
        }
        return predicate;
    }

    private Optional<Predicate> guidance(final Root<FileReference> root, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
        Optional<Predicate> predicate = Optional.empty();
        if (this.searchOptions.getCommentsSelect() != BooleanSelection.ALL) {
            final Subquery<Guidance> guidanceSubquery = query.subquery(Guidance.class);
            final Root<Guidance> guidanceProfile = guidanceSubquery.from(Guidance.class);
            guidanceSubquery.select(guidanceProfile);
            guidanceSubquery.where(guidanceProfile.get("fileReference").in(root.get("id")));

            if (this.searchOptions.getCommentsSelect().get()) {
                predicate = Optional.of(cb.exists(guidanceSubquery));
            } else {
                predicate = Optional.of(cb.not(cb.exists(guidanceSubquery)));
            }
        }
        return predicate;
    }

    private Optional<Predicate> excludeInactive(final Root<FileReference> root, final CriteriaBuilder cb) {
        Optional<Predicate> predicate = Optional.empty();
        if (this.searchOptions.getIncludeInactive() == BooleanSelection.NO) {
            predicate = Optional.of(cb.isTrue(root.get("active")));
        }
        return predicate;
    }

    private Optional<Predicate> fileNumber(final Root<FileReference> root, final CriteriaBuilder cb) {
        Optional<Predicate> predicate = Optional.empty();
        if (ObjectUtils.isNotEmpty(this.searchOptions.getFileNumber())) {
            predicate = Optional.of(cb.like(cb.lower(root.get("id")), cb.lower(cb.literal(this.searchOptions.getFileNumber() + "%"))));
        }
        return predicate;
    }

    private Optional<Predicate> textSearch(final Root<FileReference> root, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
        Optional<Predicate> predicate = Optional.empty();
        if (ObjectUtils.isNotEmpty(this.searchOptions.getSearchText())) {
            final Expression<String> likeSearchTextExpression = cb.lower(cb.literal("%" + this.searchOptions.getSearchText() + "%"));

            final Predicate titleLike = cb.like(cb.lower(root.get("title")), likeSearchTextExpression);

            final Subquery<Guidance> guidanceSubquery = query.subquery(Guidance.class);
            final Root<Guidance> guidanceProfile = guidanceSubquery.from(Guidance.class);
            guidanceSubquery.select(guidanceProfile);
            guidanceSubquery.where(cb.and(guidanceProfile.get("fileReference").in(root.get("id")),
                                          cb.or(
                                                cb.like(cb.lower(guidanceProfile.get("text")), likeSearchTextExpression),
                                                cb.like(cb.lower(guidanceProfile.get("title")), likeSearchTextExpression))));

            final List<Predicate> textPredicates = new ArrayList<>(List.of(titleLike, cb.exists(guidanceSubquery)));

            if (this.userDto != null) {
                final Subquery<UserNote> noteSubquery = query.subquery(UserNote.class);
                final Root<UserNote> noteProfile = noteSubquery.from(UserNote.class);
                noteSubquery.select(noteProfile);
                noteSubquery.where(cb.and(noteProfile.get("fileReference").in(root.get("id")),
                                          noteProfile.get("user").get("id").in(this.userDto.getId()),
                                          cb.or(
                                                cb.like(cb.lower(noteProfile.get("text")), likeSearchTextExpression),
                                                cb.like(cb.lower(noteProfile.get("title")), likeSearchTextExpression))));
                textPredicates.add(cb.exists(noteSubquery));
            }
            predicate = Optional.of(cb.or(textPredicates.toArray(Predicate[]::new)));
        }
        return predicate;
    }

}
