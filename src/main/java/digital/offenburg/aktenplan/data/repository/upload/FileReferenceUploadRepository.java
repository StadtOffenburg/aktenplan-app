package digital.offenburg.aktenplan.data.repository.upload;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import digital.offenburg.aktenplan.data.entity.upload.FileReferenceUpload;

@Repository
public interface FileReferenceUploadRepository extends CrudRepository<FileReferenceUpload, Long> {

}
