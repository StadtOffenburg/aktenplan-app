package digital.offenburg.aktenplan.data.repository.upload;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import digital.offenburg.aktenplan.data.entity.upload.FileUpload;

@Repository
public interface FileUploadRepository extends CrudRepository<FileUpload, Long> {
}
