package digital.offenburg.aktenplan.data.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import digital.offenburg.aktenplan.data.constant.Role;
import digital.offenburg.aktenplan.data.dto.UserDto;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;

@Transactional
public interface AccessControlService {

    default Optional<UserDto> currentUser() {
        final SecurityContext context = SecurityContextHolder.getContext();
        return Optional.ofNullable(context.getAuthentication())
                       .filter(authentication -> !(authentication instanceof AnonymousAuthenticationToken))
                       .map(authentication -> {
                           if (authentication.getPrincipal() instanceof final UserDto user) {
                               return user;
                           }
                           return null;
                       });
    }

    default boolean isAdmin() {
        return this.currentUser()
                   .map(UserDto::getRole)
                   .filter(role -> role == Role.ADMIN)
                   .isPresent();
    }

    default boolean isUser() {
        return this.currentUser()
                   .map(UserDto::getRole)
                   .filter(role -> role == Role.USER)
                   .isPresent();
    }

    default boolean canRequestGuidance(final FileReferenceDto fileReference) {
        return this.isUser() && fileReference.isActive();
    }

    default boolean canProvideGuidance(final FileReferenceDto fileReference) {
        return this.isAdmin() && fileReference.isActive();
    }

    default boolean canCopyGuidance(final FileReferenceDto fileReference) {
        return this.canProvideGuidance(fileReference);
    }

    default boolean canDeleteGuidance(final FileReferenceDto fileReference) {
        return this.canProvideGuidance(fileReference);
    }

    default boolean canAddUnitsToFileReference(final FileReferenceDto fileReference) {
        return this.isAdmin() && fileReference.isActive();
    }

    default boolean canAddUserNotes(final FileReferenceDto fileReference) {
        return this.isUser() && fileReference.isActive();
    }

    default boolean canCopyUserNotes(final FileReferenceDto fileReference) {
        return this.canAddUserNotes(fileReference);
    }

    default boolean canDeleteUserNotes(final FileReferenceDto fileReference) {
        return this.canAddUserNotes(fileReference);
    }

}
