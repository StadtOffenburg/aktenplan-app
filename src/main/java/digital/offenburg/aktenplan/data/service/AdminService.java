package digital.offenburg.aktenplan.data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import digital.offenburg.aktenplan.data.mapper.MapStructMapper;
import digital.offenburg.aktenplan.data.repository.AdministrativeUnitRepository;
import digital.offenburg.aktenplan.data.repository.UserRepository;

@Service
public class AdminService implements AdministrativeUnitService {
    private final PasswordEncoder passwordEncoder;
    private final MapStructMapper mapper;
    private final AdministrativeUnitRepository administrativeUnitRepository;
    private final UserRepository userRepository;

    @Autowired
    public AdminService(
            final PasswordEncoder passwordEncoder,
            final MapStructMapper mapper,
            final AdministrativeUnitRepository administrativeUnitRepository,
            final UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.mapper = mapper;
        this.administrativeUnitRepository = administrativeUnitRepository;
        this.userRepository = userRepository;
    }

    @Override
    public AdministrativeUnitRepository administrativeUnitRepository() {
        return this.administrativeUnitRepository;
    }

    @Override
    public MapStructMapper mapper() {
        return this.mapper;
    }
    
    public PasswordEncoder passwordEncoder() {
        return this.passwordEncoder;
    }
    
    public UserRepository userRepository() {
        return this.userRepository;
    }

}
