package digital.offenburg.aktenplan.data.service;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers;

import digital.offenburg.aktenplan.data.dto.AdministrativeUnitDto;
import digital.offenburg.aktenplan.data.entity.AdministrativeUnit;
import digital.offenburg.aktenplan.data.mapper.MapStructMapper;
import digital.offenburg.aktenplan.data.repository.AdministrativeUnitRepository;

@Transactional
public interface AdministrativeUnitService {
    AdministrativeUnitRepository administrativeUnitRepository();

    MapStructMapper mapper();

    default List<AdministrativeUnitDto> fetchAllUnits() {
        return this.administrativeUnitRepository().findAll().stream().map(this.mapper()::toDto).toList();
    }

    default List<AdministrativeUnitDto> fetchAllUnits(final String searchText) {
        final AdministrativeUnit unit = new AdministrativeUnit();
        unit.setName(searchText);

        final Example<AdministrativeUnit> example = Example.of(unit,
                                                               ExampleMatcher.matchingAny().withMatcher("name", GenericPropertyMatchers.contains().ignoreCase()));

        return this.administrativeUnitRepository().findAll(example).stream().map(this.mapper()::toDto).toList();
    }

    default void saveAdministrativeUnit(final AdministrativeUnitDto unitDto) {
        final AdministrativeUnit administrativeUnit = this.mapper().toEntity(unitDto);
        this.administrativeUnitRepository().save(administrativeUnit);
    }

    default void deleteAdministrativeUnits(final Set<AdministrativeUnitDto> selectedUnits) {
        final List<AdministrativeUnit> units = selectedUnits.stream().map(this.mapper()::toEntity).toList();
        this.administrativeUnitRepository().deleteAll(units);
    }
}
