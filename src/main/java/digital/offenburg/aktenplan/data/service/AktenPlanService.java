package digital.offenburg.aktenplan.data.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import digital.offenburg.aktenplan.config.ApplicationProperties;
import digital.offenburg.aktenplan.data.mapper.MapStructMapper;
import digital.offenburg.aktenplan.data.repository.AdministrativeUnitRepository;
import digital.offenburg.aktenplan.data.repository.FileReferenceAllUnitRepository;
import digital.offenburg.aktenplan.data.repository.FileReferenceRepository;
import digital.offenburg.aktenplan.data.repository.FileReferenceUnitRepository;
import digital.offenburg.aktenplan.data.repository.GuidanceRepository;
import digital.offenburg.aktenplan.data.repository.GuidanceRequestRepository;
import digital.offenburg.aktenplan.data.repository.SchemaEntryRepository;
import digital.offenburg.aktenplan.data.repository.SchemaRepository;
import digital.offenburg.aktenplan.data.repository.UserFavouriteRepository;
import digital.offenburg.aktenplan.data.repository.UserNoteRepository;

@Service
@Transactional
public class AktenPlanService implements
        AccessControlService,
        FilePlanService,
        UserFavouriteService,
        GuidanceService,
        UserNoteService,
        AdministrativeUnitService,
        FileReferenceUnitService,
        SchemaService {

    @Autowired
    private MapStructMapper mapper;
    @Autowired
    private ApplicationProperties applicationProperties;
    @Autowired
    private FileReferenceRepository fileReferenceRepository;
    @Autowired
    private SchemaRepository schemaRepository;
    @Autowired
    private SchemaEntryRepository schemaEntryRepository;
    @Autowired
    private GuidanceRepository guidanceRepository;
    @Autowired
    private GuidanceRequestRepository guidanceRequestRepository;
    @Autowired
    private UserFavouriteRepository userFavouriteRepository;
    @Autowired
    private AdministrativeUnitRepository administrativeUnitRepository;
    @Autowired
    private UserNoteRepository userNoteRepository;
    @Autowired
    private FileReferenceUnitRepository fileReferenceUnitRepository;
    @Autowired
    private FileReferenceAllUnitRepository fileReferenceAllUnitRepository;

    @Override
    public UserFavouriteRepository userFavouriteRepository() {
        return this.userFavouriteRepository;
    }

    @Override
    public MapStructMapper mapper() {
        return this.mapper;
    }

    @Override
    public GuidanceRepository guidanceRepository() {
        return this.guidanceRepository;
    }

    @Override
    public UserNoteRepository userNoteRepository() {
        return this.userNoteRepository;
    }

    @Override
    public FileReferenceRepository fileReferenceRepository() {
        return this.fileReferenceRepository;
    }

    @Override
    public SchemaRepository schemaRepository() {
        return this.schemaRepository;
    }

    @Override
    public AdministrativeUnitRepository administrativeUnitRepository() {
        return this.administrativeUnitRepository;
    }

    @Override
    public FileReferenceUnitRepository fileReferenceUnitRepository() {
        return this.fileReferenceUnitRepository;
    }

    @Override
    public SchemaEntryRepository schemaEntryRepository() {
        return this.schemaEntryRepository;
    }

    @Override
    public FileReferenceAllUnitRepository fileReferenceAllUnitRepository() {
        return this.fileReferenceAllUnitRepository;
    }

    @Override
    public GuidanceRequestRepository guidanceRequestRepository() {
        return this.guidanceRequestRepository;
    }

    @Override
    public ApplicationProperties applicationProperties() {
        return this.applicationProperties;
    }

}
