package digital.offenburg.aktenplan.data.service;

import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.apache.commons.lang3.ObjectUtils;

import digital.offenburg.aktenplan.FileReferenceUtil;
import digital.offenburg.aktenplan.GenericUtil;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;
import digital.offenburg.aktenplan.data.mapper.MapStructMapper;
import digital.offenburg.aktenplan.data.repository.FileReferenceRepository;
import digital.offenburg.aktenplan.data.repository.SchemaRepository;
import digital.offenburg.aktenplan.data.repository.specs.FileReferenceSearchSpec;
import digital.offenburg.aktenplan.data.view.SearchOptions;

@Transactional
public interface FilePlanService extends AccessControlService, UserFavouriteService {
    FileReferenceRepository fileReferenceRepository();

    SchemaRepository schemaRepository();

    @Override
    MapStructMapper mapper();

    default Set<FileReferenceDto> searchFileReferences(final SearchOptions searchOptions, final Function<String, String> searchHighlighter) {
        final var searchResults = this.fileReferenceRepository()
                                      .findAll(new FileReferenceSearchSpec(this.currentUser(), searchOptions))
                                      .stream()
                                      .map(this.mapper()::toDto)
                                      .collect(Collectors.toSet());

        if (ObjectUtils.isNotEmpty(searchOptions.getSearchText())) {
            final Pattern pattern = GenericUtil.normalizedPattern(searchOptions.getSearchText());
            searchResults.forEach(fileRef -> {
                GenericUtil.findAndHighlight(pattern, fileRef.getTitle(), searchHighlighter, fileRef::setTitle, () -> fileRef.setSearchMatchedInDetails(true));
            });
        }

        return searchResults;
    }

    default Set<FileReferenceDto> forFileNumber(final String id) {
        final Set<FileReference> parents = this.fileReferenceRepository().fetchAllParents(id);
        final Set<FileReference> children = this.fileReferenceRepository().fetchAllChildren(id);
        final Stream<FileReferenceDto> allStream = Stream.concat(parents.stream(), children.stream())
                                                         .map(item -> this.mapper().toDto(item).withSelectedFor(id))
                                                         .distinct();
        return FileReferenceUtil.prepareHierarchy(allStream, this.userFavouritesMap());
    }

    default Set<FileReferenceDto> fetchFilePlanHierarchy() {
        final Stream<FileReferenceDto> allStream = this.fileReferenceRepository().findAll().stream().map(this.mapper()::toDto);
        return FileReferenceUtil.prepareHierarchy(allStream, this.userFavouritesMap());
    }

    default FileReferenceDto fetchDetails(final FileReferenceDto fileReferenceDto) {
        final FileReference fileReferenceDetails = this.fileReferenceRepository().fetchDetails(fileReferenceDto.getId());
        final FileReferenceDto details = this.mapper().toDto(fileReferenceDetails);
        details.setUserFavourite(fileReferenceDto.isUserFavourite());
        return details;
    }

}
