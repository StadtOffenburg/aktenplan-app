package digital.offenburg.aktenplan.data.service;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.ObjectUtils;

import digital.offenburg.aktenplan.data.dto.AdministrativeUnitDto;
import digital.offenburg.aktenplan.data.dto.FileReferenceUnitDto;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.entity.FileReferenceAllUnit;
import digital.offenburg.aktenplan.data.entity.FileReferenceAllUnit.FileReferenceAllUnitKey;
import digital.offenburg.aktenplan.data.entity.FileReferenceUnit;
import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;
import digital.offenburg.aktenplan.data.mapper.MapStructMapper;
import digital.offenburg.aktenplan.data.repository.FileReferenceAllUnitRepository;
import digital.offenburg.aktenplan.data.repository.FileReferenceUnitRepository;

@Transactional
public interface FileReferenceUnitService {
    FileReferenceUnitRepository fileReferenceUnitRepository();

    FileReferenceAllUnitRepository fileReferenceAllUnitRepository();

    MapStructMapper mapper();

    default boolean isTaggedToAllUnits(final FileReferenceDto fileReferenceDto) {
        final FileReferenceAllUnitKey id = new FileReferenceAllUnitKey(fileReferenceDto.getId());
        return this.fileReferenceAllUnitRepository().existsById(id);
    }

    default void tagFileReferenceToAllUnits(final FileReferenceDto fileReferenceDto) {
        final FileReference fileReference = this.mapper().toEntity(fileReferenceDto);
        final FileReferenceAllUnit fileReferenceAllUnit = new FileReferenceAllUnit();
        fileReferenceAllUnit.setId(new FileReferenceAllUnitKey(fileReferenceDto.getId()));
        fileReferenceAllUnit.setFileReference(fileReference);

        this.fileReferenceAllUnitRepository().save(fileReferenceAllUnit);
        this.fileReferenceUnitRepository().deleteAllByFileReferenceId(fileReferenceDto.getId());
    }

    default List<FileReferenceUnitDto> fetchAllUnitsByFileReference(final FileReferenceDto fileReferenceDto) {
        return this.fileReferenceUnitRepository()
                   .findAllByFileReferenceId(fileReferenceDto.getId())
                   .stream()
                   .map(this.mapper()::toDto)
                   .toList();
    }

    default void saveFileReferenceUnits(final FileReferenceDto fileReferenceDto, final List<AdministrativeUnitDto> selectedUnits) {
        final FileReferenceAllUnitKey id = new FileReferenceAllUnitKey(fileReferenceDto.getId());
        this.fileReferenceAllUnitRepository().deleteById(id);

        if (ObjectUtils.isNotEmpty(selectedUnits)) {
            final List<Long> selectedUnitIds = selectedUnits.stream().map(AdministrativeUnitDto::getId).toList();
            this.fileReferenceUnitRepository().deleteDeTaggedUnitsForFileReferenceId(fileReferenceDto.getId(), selectedUnitIds);
            final List<FileReferenceUnit> selectedList = selectedUnits.stream()
                                                                      .map(unit -> new FileReferenceUnitDto(unit, fileReferenceDto))
                                                                      .map(this.mapper()::toEntity)
                                                                      .toList();
            this.fileReferenceUnitRepository().saveAll(selectedList);
        } else {
            this.fileReferenceUnitRepository().deleteAllByFileReferenceId(fileReferenceDto.getId());
        }
    }

}
