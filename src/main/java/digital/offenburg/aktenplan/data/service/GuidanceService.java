package digital.offenburg.aktenplan.data.service;

import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;

import javax.transaction.Transactional;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import digital.offenburg.aktenplan.GenericUtil;
import digital.offenburg.aktenplan.config.ApplicationProperties;
import digital.offenburg.aktenplan.data.dto.GuidanceDto;
import digital.offenburg.aktenplan.data.dto.GuidanceRequestDto;
import digital.offenburg.aktenplan.data.dto.OsTicket;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.entity.Guidance;
import digital.offenburg.aktenplan.data.entity.GuidanceRequest;
import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;
import digital.offenburg.aktenplan.data.mapper.MapStructMapper;
import digital.offenburg.aktenplan.data.repository.GuidanceRepository;
import digital.offenburg.aktenplan.data.repository.GuidanceRequestRepository;
import digital.offenburg.aktenplan.exceptions.OsTicketException;

@Transactional
public interface GuidanceService extends AccessControlService {
    GuidanceRepository guidanceRepository();

    GuidanceRequestRepository guidanceRequestRepository();

    MapStructMapper mapper();

    ApplicationProperties applicationProperties();

    default void saveGuidance(final FileReferenceDto fileReference, final GuidanceDto guidanceDto) {
        if (this.canProvideGuidance(fileReference)) {
            guidanceDto.setFileReference(fileReference);
            guidanceDto.setPlanVersion(fileReference.getVersion());

            final Guidance guidance = this.mapper().toEntity(guidanceDto);
            this.guidanceRepository().save(guidance);
        }
    }

    default List<GuidanceDto> fetchGuidances(final FileReferenceDto fileReferenceDto) {
        final FileReference fileReference = this.mapper().toEntity(fileReferenceDto);
        return this.guidanceRepository()
                   .findAllByFileReferenceOrderByTimestampDesc(fileReference)
                   .stream()
                   .map(this.mapper()::toDto)
                   .toList();
    }

    default void deleteGuidance(final FileReferenceDto fileReference, final GuidanceDto guidanceDto) {
        if (this.canDeleteGuidance(fileReference)) {
            this.guidanceRepository().delete(this.mapper().toEntity(guidanceDto));
        }
    }

    default void copyGuidance(final FileReferenceDto fileReference, final GuidanceDto guidanceDto) {
        if (this.canCopyGuidance(fileReference)) {
            guidanceDto.setId(null);
            guidanceDto.setFileReference(fileReference);
            guidanceDto.setPlanVersion(fileReference.getVersion());

            final Guidance guidance = this.mapper().toEntity(guidanceDto);
            this.guidanceRepository().save(guidance);
        }
    }

    default void requestGuidance(final FileReferenceDto fileReference, final GuidanceRequestDto guidanceRequestDto, final String requestTitle) throws OsTicketException {
        guidanceRequestDto.setPlanVersion(fileReference.getVersion());
        guidanceRequestDto.setFileReference(fileReference);
        guidanceRequestDto.setTitle(requestTitle);

        final HttpHeaders headers = new HttpHeaders();
        headers.set("X-API-Key", this.applicationProperties().getOsTicketAPI());
        headers.setContentType(MediaType.APPLICATION_JSON);

        final OsTicket osTicket = this.toTicketObject(guidanceRequestDto);
        final HttpEntity<OsTicket> entity = new HttpEntity<>(osTicket, headers);

        final RestTemplate rs = new RestTemplate();
        final ResponseEntity<String> response = rs.exchange(this.applicationProperties().getOsTicketUrl(), HttpMethod.POST, entity, String.class);

        if (response.getStatusCode().is2xxSuccessful()) {
            final Long ticketId = Long.valueOf(response.getBody());
            final GuidanceRequest guidanceRequest = this.mapper().toEntity(guidanceRequestDto);
            guidanceRequest.setId(ticketId);
            this.guidanceRequestRepository().save(guidanceRequest);
        } else {
            final String message = response.getStatusCodeValue() + " - " + response.getStatusCode().getReasonPhrase() + " -- " + response.getBody();
            throw new OsTicketException(message);
        }
    }

    private OsTicket toTicketObject(final GuidanceRequestDto guidanceRequestDto) {
        final OsTicket ticket = new OsTicket();
        this.currentUser().ifPresent(user -> {
            ticket.setName(user.name());
            ticket.setEmail(user.getEmail());
            ticket.setMessage(guidanceRequestDto.getText());
            ticket.setSubject(guidanceRequestDto.getTitle());
        });
        return ticket;
    }

    default List<GuidanceDto> findGuidances(final FileReferenceDto fileReferenceDto, final String searchText, final Function<String, String> searchHighlighter) {
        final FileReference fileReference = this.mapper().toEntity(fileReferenceDto);

        final List<GuidanceDto> guidances = this.guidanceRepository()
                                                .findAllByFileReferenceAndText(fileReference, searchText)
                                                .stream()
                                                .map(this.mapper()::toDto)
                                                .toList();

        if (ObjectUtils.isNotEmpty(guidances)) {
            final Pattern pattern = GenericUtil.normalizedPattern(searchText);
            guidances.forEach(guidance -> {
                GenericUtil.findAndHighlight(pattern, guidance.getTitle(), searchHighlighter, guidance::setTitle);
                GenericUtil.findAndHighlight(pattern, guidance.getText(), searchHighlighter, guidance::setText);
            });
        }
        return guidances;
    }

}
