package digital.offenburg.aktenplan.data.service;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import digital.offenburg.aktenplan.data.dto.schema.SchemaDto;
import digital.offenburg.aktenplan.data.dto.schema.SchemaEntryDto;
import digital.offenburg.aktenplan.data.dto.schema.SchemaEntryIdDto;
import digital.offenburg.aktenplan.data.mapper.MapStructMapper;
import digital.offenburg.aktenplan.data.repository.SchemaEntryRepository;
import digital.offenburg.aktenplan.data.repository.SchemaRepository;

@Transactional
public interface SchemaService {
    SchemaEntryRepository schemaEntryRepository();

    SchemaRepository schemaRepository();

    MapStructMapper mapper();

    default Optional<SchemaDto> fetchSchemaById(final String schemaId) {
        return this.schemaRepository()
                   .findById(schemaId)
                   .map(schema -> {
                       final List<SchemaEntryDto> schemaEntries = this.schemaEntryRepository()
                                                                      .findAllBySchema(schema)
                                                                      .stream()
                                                                      .map(this.mapper()::toDto)
                                                                      .toList();
                       final List<SchemaEntryDto> schemaEntryDtoList = this.prepareHierarchy(schemaEntries);
                       final SchemaDto dto = this.mapper().toDto(schema);
                       dto.setEntries(schemaEntryDtoList);
                       return dto;
                   });
    }

    private List<SchemaEntryDto> prepareHierarchy(final List<SchemaEntryDto> schemaEntries) {
        final Map<SchemaEntryIdDto, SchemaEntryDto> schemaEntryMap = schemaEntries.stream()
                                                                                  .collect(Collectors.toMap(SchemaEntryDto::getId, Function.identity()));
        for (final Entry<SchemaEntryIdDto, SchemaEntryDto> entry : schemaEntryMap.entrySet()) {
            final SchemaEntryDto item = entry.getValue();
            item.parentId().ifPresent(pId -> {
                schemaEntryMap.get(pId).getSubEntry().add(item);
            });
        }
        return schemaEntryMap.values().stream().filter(item -> item.parentId().isEmpty()).toList();
    }
}
