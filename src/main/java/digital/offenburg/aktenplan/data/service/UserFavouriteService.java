package digital.offenburg.aktenplan.data.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import digital.offenburg.aktenplan.data.dto.UserDto;
import digital.offenburg.aktenplan.data.dto.UserFavouriteDto;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.entity.User;
import digital.offenburg.aktenplan.data.entity.UserFavourite;
import digital.offenburg.aktenplan.data.entity.UserFavourite.UserFavouriteId;
import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;
import digital.offenburg.aktenplan.data.mapper.MapStructMapper;
import digital.offenburg.aktenplan.data.repository.UserFavouriteRepository;

@Transactional
public interface UserFavouriteService extends AccessControlService {
    UserFavouriteRepository userFavouriteRepository();

    MapStructMapper mapper();

    default void addToFavourite(final FileReferenceDto fileReferenceDto) {
        final User user = this.mapper().toEntity(this.currentUser().orElseThrow());
        final FileReference fileReference = this.mapper().toEntity(fileReferenceDto);
        final UserFavourite userFavourite = new UserFavourite(user, fileReference);
        this.userFavouriteRepository().save(userFavourite);
    }

    default void deleteUserFavourites(final Set<UserFavouriteDto> selectedFavourites) {
        final List<UserFavourite> items = selectedFavourites.stream().map(this.mapper()::toEntity).toList();
        this.userFavouriteRepository().deleteAll(items);
    }

    default List<UserFavouriteDto> fetchUserFavourites(final String searchText) {
        final UserDto userDto = this.currentUser().orElseThrow();
        return this.userFavouriteRepository()
                   .findAllByUserId(userDto.getId(), searchText)
                   .stream()
                   .map(this.mapper()::toDto)
                   .toList();
    }

    default boolean toggleFavourite(final FileReferenceDto fileReference) {
        boolean added = false;
        final String userId = this.currentUser().orElseThrow().getId();
        final UserFavouriteId favId = new UserFavouriteId(userId, fileReference.getId());
        final boolean exists = this.userFavouriteRepository().existsById(favId);
        if (exists) {
            this.userFavouriteRepository().deleteById(favId);
        } else {
            this.addToFavourite(fileReference);
            added = true;
        }
        return added;
    }

    default Map<String, String> userFavouritesMap() {
        final Map<String, String> userFavsMap = new HashMap<>();
        this.currentUser().ifPresent(user -> {
            final Map<String, String> collectedMap = this.userFavouriteRepository()
                                                         .findByUserIdWithNoDetails(user.getId())
                                                         .stream()
                                                         .collect(Collectors.toMap(fav -> fav.getId().getFileReferenceId(),
                                                                                   fav -> fav.getId().getFileReferenceId()));
            userFavsMap.putAll(collectedMap);
        });
        return userFavsMap;
    }

}
