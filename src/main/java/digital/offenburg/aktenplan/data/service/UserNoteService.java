package digital.offenburg.aktenplan.data.service;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;

import javax.transaction.Transactional;

import org.apache.commons.lang3.ObjectUtils;

import digital.offenburg.aktenplan.GenericUtil;
import digital.offenburg.aktenplan.data.dto.UserNoteDto;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.entity.User;
import digital.offenburg.aktenplan.data.entity.UserNote;
import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;
import digital.offenburg.aktenplan.data.mapper.MapStructMapper;
import digital.offenburg.aktenplan.data.repository.UserNoteRepository;

@Transactional
public interface UserNoteService extends AccessControlService {
    UserNoteRepository userNoteRepository();

    MapStructMapper mapper();

    default void saveUserNote(final FileReferenceDto fileReference, final UserNoteDto userNoteDto) {
        if (this.canAddUserNotes(fileReference)) {
            this.currentUser().ifPresent(userDto -> {
                if (userNoteDto.getId() == null || userDto.equals(userNoteDto.getUser())) {
                    userNoteDto.setFileReference(fileReference);
                    userNoteDto.setPlanVersion(fileReference.getVersion());

                    final UserNote note = this.mapper().toEntity(userNoteDto);
                    this.userNoteRepository().save(note);
                }
            });
        }
    }

    default List<UserNoteDto> fetchUserNotes(final FileReferenceDto fileReferenceDto) {
        return this.currentUser().map(userDto -> {
            final FileReference fileReference = this.mapper().toEntity(fileReferenceDto);
            final User user = this.mapper().toEntity(userDto);
            return this.userNoteRepository()
                       .findAllByFileReferenceAndUserOrderByTimestampDesc(fileReference, user)
                       .stream()
                       .map(note -> {
                           final UserNoteDto userNoteDto = this.mapper().toDto(note);
                           userNoteDto.setUser(userDto);
                           return userNoteDto;
                       })
                       .toList();
        }).orElseGet(Collections::emptyList);
    }

    default void deleteUserNote(final FileReferenceDto fileReference, final UserNoteDto userNoteDto) {
        if (this.canDeleteUserNotes(fileReference)) {
            this.currentUser().ifPresent(user -> {
                if (user.equals(userNoteDto.getUser())) {
                    this.userNoteRepository().delete(this.mapper().toEntity(userNoteDto));
                }
            });
        }
    }

    default void copyUserNote(final UserNoteDto userNoteDto, final FileReferenceDto fileReferenceDto) {
        if (this.canCopyUserNotes(fileReferenceDto)) {
            this.currentUser().ifPresent(user -> {
                if (user.equals(userNoteDto.getUser())) {
                    userNoteDto.setId(null);
                    userNoteDto.setFileReference(fileReferenceDto);
                    userNoteDto.setPlanVersion(fileReferenceDto.getVersion());

                    final UserNote userNote = this.mapper().toEntity(userNoteDto);
                    this.userNoteRepository().save(userNote);
                }
            });
        }
    }

    default List<UserNoteDto> findUserNotes(final FileReferenceDto fileReferenceDto, final String searchText, final Function<String, String> searchHighlighter) {
        return this.currentUser().map(userDto -> {
            final FileReference fileReference = this.mapper().toEntity(fileReferenceDto);
            final User user = this.mapper().toEntity(userDto);
            final List<UserNoteDto> notes = this.userNoteRepository()
                                                .findAllByUserAndFileReferenceAndText(user, fileReference, searchText)
                                                .stream()
                                                .map(this.mapper()::toDto)
                                                .toList();

            if (ObjectUtils.isNotEmpty(notes)) {
                final Pattern pattern = GenericUtil.normalizedPattern(searchText);
                notes.forEach(guidance -> {
                    GenericUtil.findAndHighlight(pattern, guidance.getTitle(), searchHighlighter, guidance::setTitle);
                    GenericUtil.findAndHighlight(pattern, guidance.getText(), searchHighlighter, guidance::setText);
                });
            }
            return notes;
        }).orElseGet(Collections::emptyList);
    }
}
