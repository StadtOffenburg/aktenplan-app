package digital.offenburg.aktenplan.data.service.user;

import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.ObjectUtils;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.RoleScopeResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import digital.offenburg.aktenplan.data.constant.Role;
import digital.offenburg.aktenplan.data.dto.UserDto;

public class KeycloakUserService implements UserService {

    private static final int MAX_USERS_EXPECTED = 2000;

    private final Keycloak keycloak;
    private final String realm;

    public KeycloakUserService(final Keycloak keycloak, final String realm) {
        this.keycloak = keycloak;
        this.realm = realm;
    }

    @Override
    public List<UserDto> fetchAllUsers(final String searchText) {
        final RealmResource realmResource = this.keycloak.realm(this.realm);
        final UsersResource usersResource = realmResource.users();

        final Map<String, Role> userIdRoleMap = Arrays.stream(Role.values())
                                                      .flatMap(role -> {
                                                          final Set<UserRepresentation> roleUserMembers = realmResource.roles().get(role.name()).getRoleUserMembers();
                                                          return Optional.ofNullable(roleUserMembers)
                                                                         .orElseGet(Collections::emptySet)
                                                                         .stream()
                                                                         .map(user -> new SimpleEntry<>(user.getId(), role));
                                                      })
                                                      .collect(Collectors.toMap(Entry::getKey, Entry::getValue));

        Stream<UserDto> userStream = usersResource.list(0, MAX_USERS_EXPECTED)
                                                  .stream()
                                                  .limit(MAX_USERS_EXPECTED)
                                                  .map(user -> {
                                                      final UserDto userDto = UserDto.from(user);
                                                      userDto.setRole(userIdRoleMap.get(user.getId()));
                                                      return userDto;
                                                  });

        if (ObjectUtils.isNotEmpty(searchText)) {
            userStream = userStream.filter(user -> Stream.of(user.getFirstName(), user.getLastName(), user.getEmail())
                                                         .filter(Objects::nonNull)
                                                         .map(text -> text.toLowerCase(LocaleContextHolder.getLocale()))
                                                         .anyMatch(value -> value.contains(searchText.toLowerCase(LocaleContextHolder.getLocale()))));
        }

        return userStream.toList();
    }

    @Override
    public void saveUser(final UserDto userDto) {
        final UserRepresentation userRepresentation = userDto.toUserRepresentation();
        if (ObjectUtils.isNotEmpty(userDto.getInputPassword()) && userDto.getInputPassword().equals(userDto.getConfirmPassword())) {
            userDto.updateCredentials(userRepresentation);
        }

        final UsersResource usersResource = this.keycloak.realm(this.realm).users();
        if (ObjectUtils.isEmpty(userRepresentation.getId())) {
            try (Response response = usersResource.create(userRepresentation)) {
                final String createdUserId = CreatedResponseUtil.getCreatedId(response);
                userDto.setId(createdUserId);
            }
        } else {
            usersResource.get(userRepresentation.getId()).update(userRepresentation);
        }

        this.updateRole(userDto);
    }

    private void updateRole(final UserDto userDto) {
        final RoleScopeResource realmLevel = this.keycloak.realm(this.realm).users().get(userDto.getId()).roles().realmLevel();

        final List<RoleRepresentation> allExistingRoles = realmLevel.listAll();
        realmLevel.remove(allExistingRoles);

        final List<RoleRepresentation> roleToAdd = realmLevel.listAvailable().stream().filter(role -> role.getName().equals(userDto.getRole().name())).toList();
        realmLevel.add(roleToAdd);
    }

    @Override
    public void deleteUsers(final Set<UserDto> selectedUsers) {
        final UsersResource usersResource = this.keycloak.realm(this.realm).users();
        selectedUsers.stream().map(UserDto::getId).forEach(userId -> {
            final UserResource user = usersResource.get(userId);
            user.logout();
            user.remove();
        });
    }

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        return null;
    }

}
