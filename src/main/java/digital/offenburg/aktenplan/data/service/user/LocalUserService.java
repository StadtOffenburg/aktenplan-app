package digital.offenburg.aktenplan.data.service.user;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import digital.offenburg.aktenplan.data.dto.UserDto;
import digital.offenburg.aktenplan.data.entity.User;
import digital.offenburg.aktenplan.data.mapper.MapStructMapper;
import digital.offenburg.aktenplan.data.repository.UserRepository;

@Service
@Transactional
public class LocalUserService implements UserService {
    private final UserRepository userRepository;
    private final MapStructMapper mapper;
    
    @Autowired
    public LocalUserService(final UserRepository userRepository, final MapStructMapper mapper) {
        this.userRepository = userRepository;
        this.mapper = mapper;
    }
    
    @Override
    public List<UserDto> fetchAllUsers(final String searchText) {
        return this.userRepository.searchUser(searchText).stream().map(this.mapper::toDto).toList();
    }
    
    @Override
    public void saveUser(final UserDto userDto) {
        final User user = this.mapper.toEntity(userDto);
        this.userRepository.save(user);
    }
    
    @Override
    public void deleteUsers(final Set<UserDto> selectedUsers) {
        final List<User> units = selectedUsers.stream().map(this.mapper::toEntity).toList();
        this.userRepository.deleteAll(units);
    }
    
    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
        return this.userRepository.findByEmail(email)
                                  .map(user -> this.mapper.toDto(user))
                                  .orElseThrow(() -> new UsernameNotFoundException("No user present with email: " + email));
    }
    
}
