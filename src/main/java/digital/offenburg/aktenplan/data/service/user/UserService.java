package digital.offenburg.aktenplan.data.service.user;

import java.util.List;
import java.util.Set;

import org.springframework.security.core.userdetails.UserDetailsService;

import digital.offenburg.aktenplan.data.dto.UserDto;

public interface UserService extends UserDetailsService {
    List<UserDto> fetchAllUsers(String searchText);

    void saveUser(UserDto userDto);

    void deleteUsers(Set<UserDto> selectedUsers);
}
