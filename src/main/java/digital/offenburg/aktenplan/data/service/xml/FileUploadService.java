package digital.offenburg.aktenplan.data.service.xml;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import digital.offenburg.aktenplan.FileReferenceUtil;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.entity.fileplan.FileReference;
import digital.offenburg.aktenplan.data.entity.schema.Schema;
import digital.offenburg.aktenplan.data.entity.schema.SchemaEntry;
import digital.offenburg.aktenplan.data.entity.upload.FileReferenceUpload;
import digital.offenburg.aktenplan.data.entity.upload.FileUpload;
import digital.offenburg.aktenplan.data.mapper.MapStructMapper;
import digital.offenburg.aktenplan.data.repository.FileReferenceRepository;
import digital.offenburg.aktenplan.data.repository.ReferenceLinksRepository;
import digital.offenburg.aktenplan.data.repository.SchemaEntryRepository;
import digital.offenburg.aktenplan.data.repository.SchemaRepository;
import digital.offenburg.aktenplan.data.repository.upload.FileReferenceUploadRepository;
import digital.offenburg.aktenplan.data.repository.upload.FileUploadRepository;
import digital.offenburg.aktenplan.data.xml.fileplan.FileReferenceXml;
import digital.offenburg.aktenplan.data.xml.fileplan.FileUploadXml;
import digital.offenburg.aktenplan.data.xml.schema.SchemaEntryXml;
import digital.offenburg.aktenplan.data.xml.schema.SchemaXml;

@Service
@Transactional
public class FileUploadService {
    private final MapStructMapper mapper;
    private final FileUploadRepository fileUploadRepository;
    private final FileReferenceUploadRepository fileReferenceUploadRepository;
    private final ReferenceLinksRepository referenceLinksRepository;
    private final FileReferenceRepository fileReferenceRepository;
    private final SchemaRepository schemaRepository;
    private final SchemaEntryRepository schemaEntryRepository;

    @Autowired
    public FileUploadService(
            final MapStructMapper mapper,
            final FileUploadRepository fileUploadRepository,
            final FileReferenceUploadRepository fileReferenceUploadRepository,
            final ReferenceLinksRepository referenceLinksRepository,
            final FileReferenceRepository fileReferenceRepository,
            final SchemaRepository schemaRepository,
            final SchemaEntryRepository schemaEntryRepository) {
        this.mapper = mapper;
        this.fileUploadRepository = fileUploadRepository;
        this.fileReferenceUploadRepository = fileReferenceUploadRepository;
        this.referenceLinksRepository = referenceLinksRepository;
        this.fileReferenceRepository = fileReferenceRepository;
        this.schemaRepository = schemaRepository;
        this.schemaEntryRepository = schemaEntryRepository;
    }

    public void upload(final FileUploadXml fileUploadXml, final List<FileReferenceXml> fileReferenceXmlList, final List<SchemaXml> schemaXmlList) {
        final FileUpload fileUpload = this.mapper.toUploadEntity(fileUploadXml);
        final FileUpload savedFileUpload = this.fileUploadRepository.save(fileUpload);

        this.saveSchemaDetails(schemaXmlList);

        final List<FileReferenceUpload> newFileReferenceUploads = fileReferenceXmlList.stream().map(this.mapper::toUploadEntity).toList();
        this.updateFileUpload(newFileReferenceUploads, savedFileUpload);
        this.fileReferenceUploadRepository.saveAll(newFileReferenceUploads);

        this.mergeFileReferences(savedFileUpload, newFileReferenceUploads);
    }

    private void saveSchemaDetails(final List<SchemaXml> schemaXmlList) {
        final List<Schema> schemas = schemaXmlList.stream().map(this.mapper::toEntity).toList();
        this.schemaRepository.saveAll(schemas);

        final List<SchemaEntry> schemaEntries = schemaXmlList.stream()
                                                             .map(SchemaXml::getEntries)
                                                             .flatMap(List::stream)
                                                             .flatMap(SchemaEntryXml::flatten)
                                                             .map(this.mapper::toEntity)
                                                             .toList();
        this.schemaEntryRepository.saveAll(schemaEntries);
    }

    private void updateFileUpload(final Collection<FileReferenceUpload> fileReferenceUploads, final FileUpload fileUpload) {
        fileReferenceUploads.forEach(ref -> {
            ref.setFileUpload(fileUpload);
            if (ObjectUtils.isNotEmpty(ref.getSubReference())) {
                this.updateFileUpload(ref.getSubReference(), fileUpload);
            }
        });
    }

    private void mergeFileReferences(final FileUpload savedFileUpload, final List<FileReferenceUpload> newFileReferenceUploads) {
        final Set<FileReferenceDto> references = newFileReferenceUploads.stream().map(this.mapper::toDto).collect(Collectors.toSet());
        final Set<FileReferenceDto> oldFileReferences = this.fetchOldFileReferencesHierarchy();

        final Set<FileReference> mergedReferences = FileReferenceUtil.merge(references, oldFileReferences)
                                                                     .stream()
                                                                     .map(this.mapper::toEntity)
                                                                     .collect(Collectors.toSet());

        this.fileReferenceRepository.saveAll(mergedReferences);

        this.fileReferenceRepository.markMissingReferencesInactive(savedFileUpload);
        this.referenceLinksRepository.updateLinksOfTypeUnknown();
    }

    private Set<FileReferenceDto> fetchOldFileReferencesHierarchy() {
        final Stream<FileReferenceDto> allStream = this.fileReferenceRepository.fetchAllWithDetails().stream().map(this.mapper::toDto);
        return FileReferenceUtil.prepareHierarchy(allStream, Collections.emptyMap());
    }

}
