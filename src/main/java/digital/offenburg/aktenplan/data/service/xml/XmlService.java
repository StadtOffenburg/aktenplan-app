package digital.offenburg.aktenplan.data.service.xml;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.apache.commons.codec.digest.DigestUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.vaadin.flow.component.upload.receivers.FileData;

import digital.offenburg.aktenplan.data.xml.fileplan.FilePlanXml;
import digital.offenburg.aktenplan.data.xml.fileplan.FileReferenceXml;
import digital.offenburg.aktenplan.data.xml.fileplan.FileUploadXml;
import digital.offenburg.aktenplan.data.xml.schema.SchemaXml;
import digital.offenburg.aktenplan.data.xml.schema.SchemataXml;

@Service
@Transactional
public class XmlService {
    private final FileUploadService fileUploadService;

    @Autowired
    public XmlService(final FileUploadService fileUploadService) {
        this.fileUploadService = fileUploadService;
    }

    private Element findRootElement(final File xmlFile) throws DocumentException, SAXException {
        final SAXReader reader = new SAXReader();
        reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        final Document document = reader.read(xmlFile);
        document.normalize();
        return document.getRootElement();
    }

    private List<SchemaXml> extractSchemas(final Element rootElement) {
        final List<Element> schemataEls = rootElement.elements("schemata");
        return schemataEls.stream()
                          .map(SchemataXml::from)
                          .map(SchemataXml::getSchemas)
                          .flatMap(List::stream)
                          .toList();
    }

    public Stream<FileReferenceXml> extractAktenPlan(final Element rootElement) {
        final List<Element> aktenPlanEls = rootElement.elements("aktenplan");
        return aktenPlanEls.stream()
                           .map(FilePlanXml::from)
                           .map(filePlanXml -> filePlanXml.getMainGroup().getFileReferences())
                           .flatMap(Set::stream);
    }

    public void upload(final FileData fileData) throws DocumentException, IOException, SAXException {
        final File uploadedFile = fileData.getFile();
        final Element rootElement = this.findRootElement(uploadedFile);
        final List<SchemaXml> schemaXmlList = this.extractSchemas(rootElement);
        final List<FileReferenceXml> fileReferenceXmlList = this.extractAktenPlan(rootElement).toList();

        final String checksum = DigestUtils.sha256Hex(Files.readAllBytes(uploadedFile.toPath()));
        final FileUploadXml fileUploadXml = new FileUploadXml();
        fileUploadXml.setFileName(fileData.getFileName());
        fileUploadXml.setHash(checksum);

        this.fileUploadService.upload(fileUploadXml, fileReferenceXmlList, schemaXmlList);
    }

}
