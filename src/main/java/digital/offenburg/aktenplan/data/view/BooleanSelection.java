package digital.offenburg.aktenplan.data.view;

public enum BooleanSelection {
    ALL("select.all"),
    YES("select.yes"),
    NO("select.no");

    private final String key;

    BooleanSelection(final String key) {
        this.key = key;
    }

    public String getKey() {
        return this.key;
    }

    public Boolean get() {
        return switch (this) {
            case ALL -> null;
            case YES -> Boolean.TRUE;
            case NO -> Boolean.FALSE;
        };
    }

}
