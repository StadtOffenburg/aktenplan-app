package digital.offenburg.aktenplan.data.view;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class Credential {
    @NotNull(message = "{login.email.requiredMessage}")
    @NotEmpty(message = "{login.email.requiredMessage}")
    @Email
    private String email;

    @NotNull(message = "{login.password.requiredMessage}")
    @NotEmpty(message = "{login.password.requiredMessage}")
    private String password;

    public String getEmail() {
        return this.email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

}
