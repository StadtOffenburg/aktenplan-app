package digital.offenburg.aktenplan.data.view;

import java.io.Serializable;

import org.apache.commons.lang3.ObjectUtils;

import digital.offenburg.aktenplan.data.dto.AdministrativeUnitDto;

public class SearchOptions implements Serializable {
    private String searchText;
    private String fileNumber;
    private AdministrativeUnitDto unitSelect;
    private BooleanSelection commentsSelect;
    private BooleanSelection excludeFavouritesSelect;
    private BooleanSelection includeInactive;

    public SearchOptions() {
        this.searchText = null;
        this.fileNumber = null;
        this.unitSelect = null;
        this.commentsSelect = BooleanSelection.ALL;
        this.excludeFavouritesSelect = BooleanSelection.NO;
        this.includeInactive = BooleanSelection.NO;
    }

    public boolean hasData() {
        return ObjectUtils.isNotEmpty(this.searchText)
                || ObjectUtils.isNotEmpty(this.fileNumber)
                || ObjectUtils.isNotEmpty(this.unitSelect)
                || this.commentsSelect != BooleanSelection.ALL;
    }

    public String getSearchText() {
        return this.searchText;
    }

    public void setSearchText(final String searchText) {
        this.searchText = searchText;
    }

    public String getFileNumber() {
        return this.fileNumber;
    }

    public void setFileNumber(final String fileNumber) {
        this.fileNumber = fileNumber;
    }

    public AdministrativeUnitDto getUnitSelect() {
        return this.unitSelect;
    }

    public void setUnitSelect(final AdministrativeUnitDto unitSelect) {
        this.unitSelect = unitSelect;
    }

    public BooleanSelection getCommentsSelect() {
        return this.commentsSelect;
    }

    public void setCommentsSelect(final BooleanSelection commentsSelect) {
        this.commentsSelect = commentsSelect;
    }

    public BooleanSelection getExcludeFavouritesSelect() {
        return this.excludeFavouritesSelect;
    }

    public void setExcludeFavouritesSelect(final BooleanSelection excludeFavouritesSelect) {
        this.excludeFavouritesSelect = excludeFavouritesSelect;
    }

    public BooleanSelection getIncludeInactive() {
        return this.includeInactive;
    }

    public void setIncludeInactive(final BooleanSelection includeInactive) {
        this.includeInactive = includeInactive;
    }

}
