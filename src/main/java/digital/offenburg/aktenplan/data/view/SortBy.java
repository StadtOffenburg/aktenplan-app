package digital.offenburg.aktenplan.data.view;

public enum SortBy {
    ASCENDING("select.asc"),
    DESCENDING("select.desc");

    private final String key;

    SortBy(final String key) {
        this.key = key;
    }

    public String getKey() {
        return this.key;
    }

}
