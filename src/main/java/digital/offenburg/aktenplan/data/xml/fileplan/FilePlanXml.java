package digital.offenburg.aktenplan.data.xml.fileplan;

import org.dom4j.Element;

public class FilePlanXml {
    private String version;
    private MainGroupXml mainGroup;

    public static FilePlanXml from(final Element aktenPlanEl) {
        final FilePlanXml filePlan = new FilePlanXml();
        filePlan.setVersion(aktenPlanEl.attributeValue("version"));

        final Element mainGroupEl = aktenPlanEl.element("apl-hauptgruppe");
        filePlan.setMainGroup(MainGroupXml.from(mainGroupEl, filePlan));
        return filePlan;
    }

    public FilePlanXml() {
        this.version = null;
        this.mainGroup = null;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public MainGroupXml getMainGroup() {
        return this.mainGroup;
    }

    public void setMainGroup(final MainGroupXml mainGroup) {
        this.mainGroup = mainGroup;
    }

}
