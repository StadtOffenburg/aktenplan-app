package digital.offenburg.aktenplan.data.xml.fileplan;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.dom4j.Element;
import org.dom4j.Node;

public class FileReferenceDescriptionXml {
    private String description;

    public static Set<FileReferenceDescriptionXml> from(final Element fileRefEl) {
        final Set<FileReferenceDescriptionXml> descriptions = new HashSet<>();
        final Iterator<Element> descriptionItr = fileRefEl.elementIterator("hinweis");
        while (descriptionItr.hasNext()) {
            final Element descriptionEl = descriptionItr.next();
            final FileReferenceDescriptionXml fileReferenceDescription = new FileReferenceDescriptionXml();

            final Iterator<Node> nodeIterator = descriptionEl.element("p").nodeIterator();
            final String description = Stream.generate(() -> null)
                                             .takeWhile(x -> nodeIterator.hasNext())
                                             .map(n -> nodeIterator.next().getText().strip())
                                             .collect(Collectors.joining(" "));

            fileReferenceDescription.setDescription(description);
            descriptions.add(fileReferenceDescription);
        }

        return descriptions;
    }

    public FileReferenceDescriptionXml() {
        this.description = null;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

}
