package digital.offenburg.aktenplan.data.xml.fileplan;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.ObjectUtils;
import org.dom4j.Element;
import org.dom4j.Node;

public class FileReferenceXml {
    private String refId;
    private String labelRef;
    private String labelId;
    private String title;
    private String schemaRefId;
    private String version;
    private String mainGroupType;
    private Set<FileReferenceXml> subReference;
    private Set<FileReferenceDescriptionXml> descriptions;
    private Set<ReferenceLinksXml> links;
    private Set<MoreInformationXml> informations;

    public static FileReferenceXml from(final Element fileRefEl, final FilePlanXml filePlan, final MainGroupXml mainGroup) {
        final FileReferenceXml fileReference = new FileReferenceXml();
        if (fileRefEl != null) {
            fileReference.setVersion(filePlan.getVersion());
            fileReference.setMainGroupType(mainGroup.getType());
            fileReference.setRefId(fileRefEl.attributeValue("id"));

            final Element labelElement = fileRefEl.element("bezeichnung");
            final String labelRef = labelElement.attributeValue("nummer");
            fileReference.setLabelRef(labelRef);
            if (labelRef != null) {
                fileReference.setLabelId(labelRef.replace("bez-nr_", ""));
            }
            fileReference.setTitle(labelElement.getTextTrim());

            fileReference.setDescriptions(FileReferenceDescriptionXml.from(fileRefEl));
            fileReference.setLinks(ReferenceLinksXml.from(fileRefEl));

            final Element schemaRefEl = fileRefEl.element("schemaverweis");
            if (schemaRefEl != null) {
                fileReference.setSchemaRefId(schemaRefEl.attributeValue("schema_id"));

                final Element moreInfoEl = schemaRefEl.element("schemaerlaeuterung_aktenzeichen");
                fileReference.setInformations(MoreInformationXml.from(moreInfoEl));
            }

            fileReference.setSubReference(FileReferenceXml.processSubReferences(fileRefEl, filePlan, mainGroup));
        }
        return fileReference;
    }

    private static Set<FileReferenceXml> processSubReferences(final Element fileRefEl, final FilePlanXml filePlan, final MainGroupXml mainGroup) {
        final Set<FileReferenceXml> subReference = new HashSet<>();

        final List<Node> siblings = fileRefEl
                                             .selectNodes(
                                                          "./following-sibling::*[name()='apl-gruppe' or name()='apl-untergruppe' or name()='apl-sachgruppe' or name()='apl-aktenstelle' or name()='apl-unteraktenstelle']");

        if (ObjectUtils.isNotEmpty(siblings)) {
            for (final Node node : siblings) {
                final Element subGroupEl = (Element) node;
                final Iterator<Element> subRefItr = subGroupEl.elementIterator("aktenzeichen");
                while (subRefItr.hasNext()) {
                    final Element subRefEl = subRefItr.next();
                    subReference.add(FileReferenceXml.from(subRefEl, filePlan, mainGroup));
                }
            }
        }
        return subReference;
    }

    public FileReferenceXml() {
        this.refId = null;
        this.labelRef = null;
        this.labelId = null;
        this.title = null;
        this.schemaRefId = null;
        this.version = null;
        this.descriptions = new HashSet<>();
        this.links = new HashSet<>();
        this.subReference = new HashSet<>();
        this.informations = new HashSet<>();
    }

    public String getRefId() {
        return this.refId;
    }

    public void setRefId(final String refId) {
        this.refId = refId;
    }

    public String getLabelRef() {
        return this.labelRef;
    }

    public void setLabelRef(final String labelRef) {
        this.labelRef = labelRef;
    }

    public String getLabelId() {
        return this.labelId;
    }

    public void setLabelId(final String labelId) {
        this.labelId = labelId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public Set<FileReferenceDescriptionXml> getDescriptions() {
        return this.descriptions;
    }

    public void setDescriptions(final Set<FileReferenceDescriptionXml> descriptions) {
        this.descriptions = descriptions;
    }

    public Set<ReferenceLinksXml> getLinks() {
        return this.links;
    }

    public void setLinks(final Set<ReferenceLinksXml> links) {
        this.links = links;
    }

    public Set<FileReferenceXml> getSubReference() {
        return this.subReference;
    }

    public void setSubReference(final Set<FileReferenceXml> subReference) {
        this.subReference = subReference;
    }

    public Set<MoreInformationXml> getInformations() {
        return this.informations;
    }

    public void setInformations(final Set<MoreInformationXml> informations) {
        this.informations = informations;
    }

    public String getSchemaRefId() {
        return this.schemaRefId;
    }

    public void setSchemaRefId(final String schemaRefId) {
        this.schemaRefId = schemaRefId;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public String getMainGroupType() {
        return this.mainGroupType;
    }

    public void setMainGroupType(final String mainGroupType) {
        this.mainGroupType = mainGroupType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.labelId);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof final FileReferenceXml other)) {
            return false;
        }
        return this.labelId != null && Objects.equals(this.labelId, other.labelId);
    }

}
