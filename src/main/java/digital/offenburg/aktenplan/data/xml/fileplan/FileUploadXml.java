package digital.offenburg.aktenplan.data.xml.fileplan;

import java.util.List;

public class FileUploadXml {
    private String fileName;
    private String hash;

    private List<FileReferenceXml> fileReferences;

    public FileUploadXml() {
        this.fileName = null;
        this.hash = null;
        this.fileReferences = null;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    public String getHash() {
        return this.hash;
    }

    public void setHash(final String hash) {
        this.hash = hash;
    }

    public List<FileReferenceXml> getFileReferences() {
        return this.fileReferences;
    }

    public void setFileReferences(final List<FileReferenceXml> fileReferences) {
        this.fileReferences = fileReferences;
    }

}
