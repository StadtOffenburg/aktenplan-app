package digital.offenburg.aktenplan.data.xml.fileplan;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.dom4j.Element;

public class MainGroupXml {
    private String type;
    private Set<FileReferenceXml> fileReferences;

    public static MainGroupXml from(final Element mainGroupEl, final FilePlanXml filePlan) {
        final MainGroupXml mainGroup = new MainGroupXml();
        if (mainGroupEl != null) {
            mainGroup.setType(mainGroupEl.attributeValue("typ"));
            final Iterator<Element> fileRefItr = mainGroupEl.elementIterator("aktenzeichen");
            while (fileRefItr.hasNext()) {
                final Element fileRefEl = fileRefItr.next();
                mainGroup.getFileReferences().add(FileReferenceXml.from(fileRefEl, filePlan, mainGroup));
            }
        }
        return mainGroup;
    }

    public MainGroupXml() {
        this.type = null;
        this.fileReferences = new HashSet<>();
    }

    public String getType() {
        return this.type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public Set<FileReferenceXml> getFileReferences() {
        return this.fileReferences;
    }

    public void setFileReferences(final Set<FileReferenceXml> fileReferences) {
        this.fileReferences = fileReferences;
    }

}
