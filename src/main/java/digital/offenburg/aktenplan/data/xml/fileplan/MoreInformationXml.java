package digital.offenburg.aktenplan.data.xml.fileplan;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.lang3.ObjectUtils;
import org.dom4j.Element;

public class MoreInformationXml {
    private String text;

    public static Set<MoreInformationXml> from(final Element moreInfoEl) {
        final Set<MoreInformationXml> infos = new HashSet<>();
        if (moreInfoEl != null) {
            final Iterator<Element> infoItr = moreInfoEl.elementIterator("p");
            while (infoItr.hasNext()) {
                final String info = infoItr.next().getTextTrim();
                if (ObjectUtils.isNotEmpty(info) && !info.endsWith(":")) {
                    final MoreInformationXml information = new MoreInformationXml();
                    information.setText(info);
                    infos.add(information);
                }
            }
        }
        return infos;
    }

    public MoreInformationXml() {
        this.text = null;
    }

    public String getText() {
        return this.text;
    }

    public void setText(final String text) {
        this.text = text;
    }

}
