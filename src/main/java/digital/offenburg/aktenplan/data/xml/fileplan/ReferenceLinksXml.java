package digital.offenburg.aktenplan.data.xml.fileplan;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.dom4j.Element;

public class ReferenceLinksXml {
    private String type;
    private String target;
    private String label;

    public static Set<ReferenceLinksXml> from(final Element fileRefEl) {
        final var outerLinks = fileRefEl.elements("aktenplanverweis")
                                        .stream()
                                        .map(linkEl -> {
                                            final ReferenceLinksXml link = new ReferenceLinksXml();
                                            link.setType(linkEl.attributeValue("typ"));
                                            link.setLabel(linkEl.elementTextTrim("p"));
                                            link.setTarget(linkEl.attributeValue("verweisziel"));
                                            return link;
                                        });

        final var linksInDescription = fileRefEl.selectNodes("./hinweis/p/aktenzeichenverweis")
                                                .stream()
                                                .map(node -> {
                                                    final Element linkEl = (Element) node;
                                                    final ReferenceLinksXml link = new ReferenceLinksXml();
                                                    link.setType("unknown");
                                                    link.setLabel("--");
                                                    link.setTarget(linkEl.attributeValue("az").replace("bez-nr_", ""));
                                                    return link;
                                                });

        return Stream.concat(outerLinks, linksInDescription).collect(Collectors.toSet());
    }

    public ReferenceLinksXml() {
        this.type = null;
        this.target = null;
        this.label = null;
    }

    public String getType() {
        return this.type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getTarget() {
        return this.target;
    }

    public void setTarget(final String target) {
        this.target = target;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

}
