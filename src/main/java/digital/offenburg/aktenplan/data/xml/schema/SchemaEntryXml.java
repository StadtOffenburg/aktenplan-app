package digital.offenburg.aktenplan.data.xml.schema;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.dom4j.Element;
import org.dom4j.Node;

public class SchemaEntryXml {
    private SchemaEntryIdXml id;
    private SchemaXml schema;
    private String text;
    private List<SchemaEntryXml> subEntry;

    public static SchemaEntryXml from(final Element schemaTextEl, final SchemaXml schema) {
        final SchemaEntryXml schemaEntry = new SchemaEntryXml();
        schemaEntry.setSchema(schema);

        if (schemaTextEl != null) {
            final String entryNumber = schemaTextEl.elementText("untergliederungsnummer");
            final SchemaEntryIdXml id = new SchemaEntryIdXml(entryNumber, schema.getId());
            schemaEntry.setId(id);

            schemaEntry.setText(schemaTextEl.elementText("p"));

            final Node siblingNode = schemaTextEl.selectSingleNode(
                                                                   "./following-sibling::*[position()=1][name()='schemauntereintrag' or name()='schemaunteruntereintrag'] ");
            if (siblingNode != null && siblingNode.getNodeType() == Node.ELEMENT_NODE) {
                final Element subEntryEl = (Element) siblingNode;
                final Iterator<Element> schemaTextItr = subEntryEl.elementIterator("schematext");
                while (schemaTextItr.hasNext()) {
                    final SchemaEntryXml subEntry = SchemaEntryXml.from(schemaTextItr.next(), schema);
                    schemaEntry.getSubEntry().add(subEntry);
                }
            }
        }
        return schemaEntry;
    }

    public SchemaEntryXml() {
        this.id = null;
        this.schema = null;
        this.text = null;
        this.subEntry = new ArrayList<>();
    }

    public Stream<SchemaEntryXml> flatten() {
        return Stream.concat(Stream.of(this), this.subEntry.stream().flatMap(SchemaEntryXml::flatten));
    }

    public SchemaEntryIdXml getId() {
        return this.id;
    }

    public void setId(final SchemaEntryIdXml id) {
        this.id = id;
    }

    public SchemaXml getSchema() {
        return this.schema;
    }

    public void setSchema(final SchemaXml schema) {
        this.schema = schema;
    }

    public String getText() {
        return this.text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public List<SchemaEntryXml> getSubEntry() {
        return this.subEntry;
    }

    public void setSubEntry(final List<SchemaEntryXml> subEntry) {
        this.subEntry = subEntry;
    }

}
