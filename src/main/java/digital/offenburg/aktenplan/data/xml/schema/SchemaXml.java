package digital.offenburg.aktenplan.data.xml.schema;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.dom4j.Element;

public class SchemaXml {
    private String id;
    private boolean active;
    private String name;
    private List<SchemaEntryXml> entries;
    private String hint;
    private String version;

    public static SchemaXml from(final Element schemaEl, final SchemataXml schemata) {
        final SchemaXml schema = new SchemaXml();
        schema.setVersion(schemata.getVersion());
        if (schemaEl != null) {
            schema.setId(schemaEl.attributeValue("schema_id"));
            schema.setActive("ja".equals(schemaEl.attributeValue("aktiv")));
            schema.setName(schemaEl.elementTextTrim("schemaname"));

            final String hint = Optional.ofNullable(schemaEl.element("schemahinweis")).map(el -> el.elementText("p")).orElse("");
            schema.setHint(hint);

            final Iterator<Element> schemaEntryItr = schemaEl.elementIterator("schemaeintrag");
            while (schemaEntryItr.hasNext()) {
                final Element schemaEntryEl = schemaEntryItr.next();
                if (schemaEntryEl != null) {
                    final Iterator<Element> schemaTextItr = schemaEntryEl.elementIterator("schematext");
                    while (schemaTextItr.hasNext()) {
                        final Element schemaTextEl = schemaTextItr.next();
                        schema.getEntries().add(SchemaEntryXml.from(schemaTextEl, schema));
                    }
                }
            }
        }
        return schema;
    }

    public SchemaXml() {
        this.id = null;
        this.active = false;
        this.name = null;
        this.entries = new ArrayList<>();
        this.hint = null;
        this.version = null;
    }

    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(final boolean active) {
        this.active = active;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public List<SchemaEntryXml> getEntries() {
        return this.entries;
    }

    public void setEntries(final List<SchemaEntryXml> entries) {
        this.entries = entries;
    }

    public String getHint() {
        return this.hint;
    }

    public void setHint(final String hint) {
        this.hint = hint;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

}
