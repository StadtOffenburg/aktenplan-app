package digital.offenburg.aktenplan.data.xml.schema;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Element;

public class SchemataXml {
    private String version;
    private List<SchemaXml> schemas;

    public static SchemataXml from(final Element schemataEl) {
        final SchemataXml schemata = new SchemataXml();
        if (schemataEl != null) {
            final String version = schemataEl.attributeValue("version");
            schemata.setVersion(version);
            final Iterator<Element> schemaItr = schemataEl.elementIterator("schema");
            while (schemaItr.hasNext()) {
                final Element schemaEl = schemaItr.next();
                schemata.getSchemas().add(SchemaXml.from(schemaEl, schemata));
            }
        }
        return schemata;
    }

    public SchemataXml() {
        this.version = null;
        this.schemas = new ArrayList<>();
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public List<SchemaXml> getSchemas() {
        return this.schemas;
    }

    public void setSchemas(final List<SchemaXml> schemas) {
        this.schemas = schemas;
    }

}
