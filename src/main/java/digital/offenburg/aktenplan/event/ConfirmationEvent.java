package digital.offenburg.aktenplan.event;

import com.vaadin.flow.component.ComponentEvent;

import digital.offenburg.aktenplan.component.ConfirmDialog;

public class ConfirmationEvent extends ComponentEvent<ConfirmDialog> {

    public ConfirmationEvent(final ConfirmDialog source) {
        super(source, false);
    }

}
