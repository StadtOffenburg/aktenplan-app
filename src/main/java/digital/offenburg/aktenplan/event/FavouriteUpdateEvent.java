package digital.offenburg.aktenplan.event;

import com.vaadin.flow.component.ComponentEvent;

import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.views.search.details.FilePlanDetailsView;

public class FavouriteUpdateEvent extends ComponentEvent<FilePlanDetailsView> {

    private final FileReferenceDto fileReference;

    public FavouriteUpdateEvent(final FilePlanDetailsView source, final FileReferenceDto fileReference) {
        super(source, false);
        this.fileReference = fileReference;
    }

    public FileReferenceDto getFileReference() {
        return this.fileReference;
    }

}
