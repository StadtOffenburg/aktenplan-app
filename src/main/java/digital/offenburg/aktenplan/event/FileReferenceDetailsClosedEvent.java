package digital.offenburg.aktenplan.event;

import com.vaadin.flow.component.ComponentEvent;

import digital.offenburg.aktenplan.views.search.details.FilePlanDetailsView;

public class FileReferenceDetailsClosedEvent extends ComponentEvent<FilePlanDetailsView> {

    public FileReferenceDetailsClosedEvent(final FilePlanDetailsView source) {
        super(source, false);
    }

}
