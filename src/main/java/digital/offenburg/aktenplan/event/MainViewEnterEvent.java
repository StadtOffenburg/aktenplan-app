package digital.offenburg.aktenplan.event;

import com.vaadin.flow.component.ComponentEvent;

import digital.offenburg.aktenplan.views.search.MainView;

public class MainViewEnterEvent extends ComponentEvent<MainView> {

    public MainViewEnterEvent(final MainView source) {
        super(source, false);
    }

}
