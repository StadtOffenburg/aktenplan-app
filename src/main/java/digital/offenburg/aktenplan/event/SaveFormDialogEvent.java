package digital.offenburg.aktenplan.event;

import java.io.Serializable;

import com.vaadin.flow.component.ComponentEvent;

import digital.offenburg.aktenplan.component.FormDialog;

public class SaveFormDialogEvent<T extends Serializable> extends ComponentEvent<FormDialog<T>> {

    private final T item;

    public SaveFormDialogEvent(final FormDialog<T> source) {
        super(source, false);
        this.item = null;
    }

    public SaveFormDialogEvent(final FormDialog<T> source, final T item) {
        super(source, false);
        this.item = item;
    }

    public T getItem() {
        return this.item;
    }

}
