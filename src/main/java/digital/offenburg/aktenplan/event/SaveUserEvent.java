package digital.offenburg.aktenplan.event;

import com.vaadin.flow.component.ComponentEvent;

import digital.offenburg.aktenplan.data.dto.UserDto;
import digital.offenburg.aktenplan.views.admin.UserDialog;

public class SaveUserEvent extends ComponentEvent<UserDialog> {

    private final UserDto item;

    public SaveUserEvent(final UserDialog source, final UserDto item) {
        super(source, false);
        this.item = item;
    }

    public UserDto getItem() {
        return this.item;
    }

}
