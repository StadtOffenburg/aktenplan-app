package digital.offenburg.aktenplan.event;

import com.vaadin.flow.component.ComponentEvent;

import digital.offenburg.aktenplan.data.view.SearchOptions;
import digital.offenburg.aktenplan.views.search.SearchPanel;

public class SearchEvent extends ComponentEvent<SearchPanel> {

    private final SearchOptions searchOptions;

    public SearchEvent(final SearchPanel source, final SearchOptions searchOptions) {
        super(source, false);
        this.searchOptions = searchOptions;
    }

    public SearchOptions getSearchOptions() {
        return this.searchOptions;
    }

}
