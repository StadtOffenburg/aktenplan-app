package digital.offenburg.aktenplan.event;

import com.vaadin.flow.component.ComponentEvent;

import digital.offenburg.aktenplan.data.view.SortBy;
import digital.offenburg.aktenplan.views.search.SearchPanel;

public class SortEvent extends ComponentEvent<SearchPanel> {

    private final SortBy sortBy;

    public SortEvent(final SearchPanel source, final SortBy sortBy) {
        super(source, false);
        this.sortBy = sortBy;
    }

    public SortBy getSortBy() {
        return this.sortBy;
    }

}
