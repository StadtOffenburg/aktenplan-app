package digital.offenburg.aktenplan.exceptions;

public class OsTicketException extends Exception {

    public OsTicketException(final String message) {
        super(message);
    }

}
