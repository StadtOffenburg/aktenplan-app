package digital.offenburg.aktenplan.views;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

import com.vaadin.flow.shared.Registration;

public class Cleaner implements Serializable {

    private final List<Registration> registrations;

    public Cleaner() {
        this.registrations = new ArrayList<>();
    }

    public void addRegistration(final Registration registration) {
        this.registrations.add(registration);
    }

    public void cleanup() {
        this.registrations.stream().filter(Objects::nonNull).forEach(Registration::remove);
    }

    public void addRegistration(final Supplier<Registration> supplier) {
        this.registrations.add(supplier.get());
    }

}
