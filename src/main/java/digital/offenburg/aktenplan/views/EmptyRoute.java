package digital.offenburg.aktenplan.views;

import javax.annotation.security.PermitAll;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;

import digital.offenburg.aktenplan.views.search.MainView;

@Route("")
@PermitAll
public class EmptyRoute extends Div implements BeforeEnterObserver {

    @Override
    public void beforeEnter(final BeforeEnterEvent event) {
        event.forwardTo(MainView.class);
    }

}
