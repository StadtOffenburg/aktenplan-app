package digital.offenburg.aktenplan.views;

import java.util.Optional;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.theme.lumo.LumoUtility;

import digital.offenburg.aktenplan.component.OffenburgBranding;
import digital.offenburg.aktenplan.config.ConfigConstants;
import digital.offenburg.aktenplan.data.dto.UserDto;
import digital.offenburg.aktenplan.data.service.AccessControlService;
import digital.offenburg.aktenplan.views.admin.AdminView;
import digital.offenburg.aktenplan.views.favourites.FavouritesView;
import digital.offenburg.aktenplan.views.search.MainView;

public class MainLayout extends VerticalLayout implements RouterLayout {
    private final transient AccessControlService accessControlService;
    
    public MainLayout(final AccessControlService accessControlService) {
        this.accessControlService = accessControlService;
        this.setSizeFull();
        this.setPadding(false);
        this.setSpacing(false);
        this.add(this.header());
    }
    
    private Component header() {
        final HorizontalLayout layout = new HorizontalLayout();
        layout.setWidthFull();
        layout.setAlignItems(Alignment.CENTER);
        layout.setJustifyContentMode(JustifyContentMode.BETWEEN);
        layout.add(new OffenburgBranding(), this.menu(), this.user());
        return layout;
    }
    
    private Component menu() {
        final HorizontalLayout layout = new HorizontalLayout();
        layout.addClassName(LumoUtility.Padding.MEDIUM);
        layout.setJustifyContentMode(JustifyContentMode.CENTER);
        layout.setSpacing(false);
        layout.getThemeList().add("spacing-xl");
        layout.getStyle().set("flex", "1 1 0");
        
        final RouterLink searchLink = this.createRouterLink(VaadinIcon.SEARCH, this.getTranslation("menu.search"), MainView.class);
        layout.add(searchLink);
        
        this.accessControlService.currentUser().ifPresent(user -> {
            final RouterLink favouritesLink = this.createRouterLink(VaadinIcon.STAR_O, this.getTranslation("menu.favourites"), FavouritesView.class);
            layout.add(favouritesLink);
        });
        
        if (this.accessControlService.isAdmin()) {
            final RouterLink adminLink = this.createRouterLink(VaadinIcon.COG_O, this.getTranslation("menu.admin"), AdminView.class);
            layout.add(adminLink);
        }
        
        return layout;
    }
    
    private RouterLink createRouterLink(final VaadinIcon icon, final String text, final Class<? extends Component> navigationTarget) {
        final RouterLink link = new RouterLink();
        link.setRoute(navigationTarget);
        final VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);
        layout.setSpacing(false);
        layout.setAlignItems(Alignment.CENTER);
        layout.add(icon.create(), new Span(text));
        link.add(layout);
        return link;
    }
    
    private Component user() {
        final FlexLayout layout = new FlexLayout();
        layout.addClassName(LumoUtility.Padding.MEDIUM);
        layout.getStyle().set("flex", "1 1 0");
        layout.setJustifyContentMode(JustifyContentMode.END);
        final Optional<UserDto> loggedInUser = this.accessControlService.currentUser();
        loggedInUser.ifPresentOrElse(user -> {
            final Avatar avatar = new Avatar();
            avatar.addClassNames(LumoUtility.Background.CONTRAST_70, LumoUtility.TextColor.PRIMARY_CONTRAST);
            avatar.setName(user.name());
            avatar.setHeight("50px");
            avatar.setWidth("50px");
            layout.add(avatar);
            
            this.logoutContextMenu(user, avatar);
        }, () -> {
            final Button button = new Button(this.getTranslation("login.button.label"));
            button.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_LARGE);
            button.addClickListener(event -> {
                UI.getCurrent().getPage().setLocation(ConfigConstants.LOGIN_URL);
            });
            layout.add(button);
        });
        return layout;
    }
    
    private void logoutContextMenu(final UserDto user, final Component target) {
        final Span nameSpan = new Span(user.name());
        
        final String roles = user.getRole().getDescription();
        final Span roleSpan = new Span(roles);
        roleSpan.addClassName("ui-logout-popup--roles");
        
        final VerticalLayout layout = new VerticalLayout();
        layout.add(nameSpan, roleSpan);
        layout.addClassNames(LumoUtility.Padding.Bottom.NONE);
        
        final ContextMenu menu = new ContextMenu();
        menu.setTarget(target);
        menu.add(layout);
        menu.add(new Hr());
        menu.setOpenOnClick(true);
        
        final Span logoutText = new Span("Logout");
        final HorizontalLayout logoutItem = new HorizontalLayout();
        logoutItem.add(VaadinIcon.SIGN_OUT.create(), logoutText);
        logoutItem.addClassNames(LumoUtility.Padding.Left.MEDIUM, LumoUtility.TextColor.ERROR, LumoUtility.FontWeight.SEMIBOLD);
        menu.addItem(logoutItem, event -> UI.getCurrent().getPage().setLocation(ConfigConstants.LOGOUT_URL));
    }
    
}
