package digital.offenburg.aktenplan.views.admin;

import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.security.RolesAllowed;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.ParentLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RoutePrefix;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.theme.lumo.LumoUtility;

import digital.offenburg.aktenplan.data.constant.Role;
import digital.offenburg.aktenplan.views.MainLayout;

@RoutePrefix("admin")
@Route(value = "admin", absolute = true, layout = MainLayout.class)
@ParentLayout(value = MainLayout.class)
@RolesAllowed({ Role.Named.ADMIN })
public class AdminView extends VerticalLayout implements RouterLayout, BeforeEnterObserver {

    private final FlexLayout tabViewPort;
    private Tabs tabs;
    private Map<Class<? extends Component>, Tab> navigationMap;

    private enum AdminTabs {
        XML_IMPORT(XmlImportView.class, "admin.tabMenu.xml"),
        USER_MANAEMENT(UserManagementView.class, "admin.tabMenu.user"),
        UNIT_MANAEGMENT(UnitManagementView.class, "admin.tabMenu.units");

        final Class<? extends Component> target;
        final String labelKey;

        AdminTabs(final Class<? extends Component> target, final String labelKey) {
            this.target = target;
            this.labelKey = labelKey;
        }
    }

    public AdminView() {
        this.setSizeFull();
        this.setPadding(false);
        this.setSpacing(false);
        this.tabViewPort = new FlexLayout();
        this.tabViewPort.setSizeFull();
        this.add(this.tabOptions());
        this.add(this.tabViewPort);
    }

    private Component tabOptions() {
        final HorizontalLayout layout = new HorizontalLayout();
        layout.addClassNames("ui-horizontal-separator", LumoUtility.Background.CONTRAST_5);
        layout.setWidthFull();
        layout.setJustifyContentMode(JustifyContentMode.CENTER);

        this.navigationMap = Arrays.stream(AdminTabs.values())
                                   .map(item -> {
                                       final RouterLink link = new RouterLink(this.getTranslation(item.labelKey), item.target);
                                       final Tab tab = new Tab(link);
                                       tab.setId(item.name());

                                       return new SimpleEntry<>(item.target, tab);
                                   })
                                   .collect(Collectors.toMap(Entry::getKey, Entry::getValue, (u, v) -> u, LinkedHashMap::new));

        this.tabs = new Tabs(false, this.navigationMap.values().toArray(Tab[]::new));
        layout.add(this.tabs);
        return layout;
    }

    @Override
    public void showRouterLayoutContent(final HasElement content) {
        if (content != null) {
            this.tabViewPort.getElement().appendChild(Objects.requireNonNull(content.getElement()));
        }
    }

    @Override
    public void beforeEnter(final BeforeEnterEvent event) {
        if (event.getNavigationTarget() == this.getClass()) {
            this.tabs.getChildren().map(component -> (Tab) component).filter(Tab::isSelected).findFirst().ifPresentOrElse(tab -> {
                final AdminTabs selected = AdminTabs.valueOf(tab.getId().orElse(""));
                event.forwardTo(selected.target);
            }, () -> event.forwardTo(AdminTabs.XML_IMPORT.target));
        } else {
            final Tab targetTab = this.navigationMap.get(event.getNavigationTarget());
            if (targetTab != null) {
                this.tabs.setSelectedTab(targetTab);
            }
        }
    }

}
