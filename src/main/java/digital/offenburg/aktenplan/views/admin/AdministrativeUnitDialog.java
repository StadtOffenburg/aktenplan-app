package digital.offenburg.aktenplan.views.admin;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;

import digital.offenburg.aktenplan.component.FormDialog;
import digital.offenburg.aktenplan.data.dto.AdministrativeUnitDto;
import digital.offenburg.aktenplan.event.SaveFormDialogEvent;

public final class AdministrativeUnitDialog extends FormDialog<AdministrativeUnitDto> {

    public static class Builder {
        private TextField name;
        private AdministrativeUnitDialog dialog;

        public Builder forEdit(final AdministrativeUnitDto unitDto) {
            this.dialog = new AdministrativeUnitDialog(unitDto);
            this.dialog.prepare(this.formDialogI18n("admin.unit.dialog.editHeader"), this.formComponent());
            return this;
        }

        public Builder forNew() {
            this.dialog = new AdministrativeUnitDialog(new AdministrativeUnitDto());
            this.dialog.prepare(this.formDialogI18n("admin.unit.dialog.newHeader"), this.formComponent());
            return this;
        }

        public Builder withSaveEventListener(final ComponentEventListener<SaveFormDialogEvent<AdministrativeUnitDto>> listener) {
            this.dialog.addSaveItemEventListener(listener);
            return this;
        }

        public AdministrativeUnitDialog build() {
            return this.dialog;
        }

        private FormDialogI18n formDialogI18n(final String headerKey) {
            final FormDialogI18n formDialogI18n = new FormDialogI18n();
            formDialogI18n.setHeaderTitle(this.dialog.getTranslation(headerKey));
            formDialogI18n.setSaveButtonLabel(this.dialog.getTranslation("admin.unit.dialog.saveButtonLabel"));
            formDialogI18n.setCancelButtonLabel(this.dialog.getTranslation("admin.unit.dialog.cancelButtonLabel"));
            return formDialogI18n;
        }

        private Component formComponent() {
            this.name = new TextField(this.dialog.getTranslation("admin.unit.dialog.nameLabel"));
            final VerticalLayout dialogLayout = new VerticalLayout(this.name);
            dialogLayout.setPadding(false);
            dialogLayout.setSpacing(false);
            dialogLayout.setSizeFull();
            dialogLayout.setAlignItems(FlexComponent.Alignment.STRETCH);

            this.dialog.getBinder().bindInstanceFields(this);
            return dialogLayout;
        }
    }

    private AdministrativeUnitDialog(final AdministrativeUnitDto unitDto) {
        super(AdministrativeUnitDto.class, unitDto);
    }

}
