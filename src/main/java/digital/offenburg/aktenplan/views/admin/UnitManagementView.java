package digital.offenburg.aktenplan.views.admin;

import java.util.List;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.lang3.ObjectUtils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.lumo.LumoUtility;
import com.vaadin.flow.theme.lumo.LumoUtility.FontSize;

import digital.offenburg.aktenplan.component.ConfirmDialog;
import digital.offenburg.aktenplan.component.ConfirmDialog.ConfirmDialogI18n;
import digital.offenburg.aktenplan.component.LinkButton;
import digital.offenburg.aktenplan.data.constant.Role;
import digital.offenburg.aktenplan.data.dto.AdministrativeUnitDto;
import digital.offenburg.aktenplan.data.service.AdminService;
import digital.offenburg.aktenplan.event.SaveFormDialogEvent;

@PageTitle("Unit Management | AktenPlan")
@Route(value = "units", layout = AdminView.class)
@RolesAllowed({ Role.Named.ADMIN })
public class UnitManagementView extends VerticalLayout {

    private final transient AdminService adminService;
    private final Grid<AdministrativeUnitDto> unitGrid;
    private final VerticalLayout gridSection;

    private TextField searchText;
    private Button editBtn;
    private Button deleteBtn;
    private Set<AdministrativeUnitDto> selectedUnits;

    public UnitManagementView(final AdminService adminService) {
        this.adminService = adminService;

        this.setPadding(false);
        this.setSpacing(false);
        this.setSizeFull();

        this.unitGrid = this.createGrid();
        this.gridSection = new VerticalLayout();
        this.gridSection.setSizeFull();

        this.add(this.searchBar(), this.gridSection);
        this.refreshGrid();
    }

    private Grid<AdministrativeUnitDto> createGrid() {
        final Grid<AdministrativeUnitDto> grid = new Grid<>();
        grid.setSelectionMode(Grid.SelectionMode.MULTI);
        grid.setSizeFull();

        grid.addColumn(AdministrativeUnitDto::getName)
            .setComparator(AdministrativeUnitDto::getName)
            .setHeader(this.getTranslation("admin.unit.grid.header.name"))
            .setAutoWidth(true);

        grid.addItemClickListener(event -> {
            final var model = grid.getSelectionModel();
            if (model.isSelected(event.getItem())) {
                model.deselect(event.getItem());
            } else {
                model.select(event.getItem());
            }
        });

        grid.asMultiSelect().addValueChangeListener(event -> {
            this.selectedUnits = event.getValue();
            this.editBtn.setEnabled(ObjectUtils.isNotEmpty(this.selectedUnits) && this.selectedUnits.size() == 1);
            this.deleteBtn.setEnabled(ObjectUtils.isNotEmpty(this.selectedUnits));

            if (ObjectUtils.isNotEmpty(this.selectedUnits)) {
                this.deleteBtn.addClassNames(LumoUtility.TextColor.ERROR);
            } else {
                this.deleteBtn.removeClassName(LumoUtility.TextColor.ERROR);
            }
        });
        return grid;
    }

    private Component searchBar() {
        this.searchText = new TextField();
        this.searchText.setPlaceholder(this.getTranslation("admin.unit.searchPlaceholder"));
        this.searchText.setPrefixComponent(VaadinIcon.SEARCH.create());
        this.searchText.setClearButtonVisible(true);
        this.searchText.setValueChangeMode(ValueChangeMode.LAZY);
        this.searchText.setValueChangeTimeout(1000);
        this.searchText.addValueChangeListener(event -> this.refreshGrid());
        this.searchText.addKeyPressListener(Key.ENTER, event -> this.refreshGrid());
        this.searchText.getStyle().set("flex", "1 1 0");

        this.editBtn = new LinkButton(this.getTranslation("admin.unit.button.editLabel"), VaadinIcon.EDIT.create());
        this.editBtn.setEnabled(false);
        this.editBtn.addClickListener(event -> {
            if (ObjectUtils.isNotEmpty(this.selectedUnits) && this.selectedUnits.size() == 1) {
                new AdministrativeUnitDialog.Builder().forEdit(this.selectedUnits.iterator().next())
                                                      .withSaveEventListener(this::saveAdministrativeUnit)
                                                      .build()
                                                      .open();
            }
        });

        this.deleteBtn = new LinkButton(this.getTranslation("admin.unit.button.deleteLabel"), VaadinIcon.TRASH.create());
        this.deleteBtn.setEnabled(false);
        this.deleteBtn.addClickListener(event -> {

            final ConfirmDialogI18n confirmDialogI18n = new ConfirmDialogI18n();
            confirmDialogI18n.setTitle(this.getTranslation("admin.unit.delete.dialog.title"));
            confirmDialogI18n.setMessage(this.getTranslation("admin.unit.delete.dialog.message"));
            confirmDialogI18n.setCancelButtonLabel(this.getTranslation("admin.unit.delete.dialog.cancelButtonLabel"));
            confirmDialogI18n.setConfirmButtonLabel(this.getTranslation("admin.unit.delete.dialog.confirmButtonLabel"));

            final ConfirmDialog deleteConfirmDialog = new ConfirmDialog(confirmDialogI18n);
            deleteConfirmDialog.addConfirmListener(delEvent -> {
                this.adminService.deleteAdministrativeUnits(this.selectedUnits);
                deleteConfirmDialog.close();
                this.refreshGrid();
            });
            deleteConfirmDialog.open();
        });

        final Button newBtn = new Button(this.getTranslation("admin.unit.button.newLabel"));
        newBtn.addClassNames("ui-primary-small-button");
        newBtn.addClickListener(event -> {
            new AdministrativeUnitDialog.Builder().forNew()
                                                  .withSaveEventListener(this::saveAdministrativeUnit)
                                                  .build()
                                                  .open();
        });

        final HorizontalLayout buttonsContainer = new HorizontalLayout(this.editBtn, this.deleteBtn, newBtn);
        buttonsContainer.getStyle().set("flex", "4 1 0");
        buttonsContainer.addClassNames(LumoUtility.JustifyContent.END, LumoUtility.AlignItems.CENTER);

        final HorizontalLayout layout = new HorizontalLayout();
        layout.setWidthFull();
        layout.addClassNames(LumoUtility.AlignItems.CENTER, LumoUtility.Background.CONTRAST_5, LumoUtility.Padding.MEDIUM);
        layout.add(this.searchText, buttonsContainer);
        return layout;
    }

    private void saveAdministrativeUnit(final SaveFormDialogEvent<AdministrativeUnitDto> event) {
        this.adminService.saveAdministrativeUnit(event.getItem());
        this.refreshGrid();
        event.getSource().close();
    }

    private void refreshGrid() {
        this.gridSection.removeAll();
        final List<AdministrativeUnitDto> allUnits = this.adminService.fetchAllUnits(this.searchText.getValue());
        if (ObjectUtils.isNotEmpty(allUnits)) {
            this.unitGrid.setItems(allUnits);
            this.gridSection.add(this.unitGrid);
        } else {
            final Span noDataFound = new Span(this.getTranslation("admin.unit.noDataFound"));
            noDataFound.addClassNames(LumoUtility.Padding.LARGE, FontSize.MEDIUM);
            this.gridSection.add(noDataFound);
        }
    }

}
