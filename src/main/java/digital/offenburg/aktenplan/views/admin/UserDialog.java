package digital.offenburg.aktenplan.views.admin;

import org.apache.commons.lang3.ObjectUtils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder.Binding;

import digital.offenburg.aktenplan.component.FormDialog;
import digital.offenburg.aktenplan.data.constant.Role;
import digital.offenburg.aktenplan.data.dto.UserDto;
import digital.offenburg.aktenplan.event.SaveFormDialogEvent;

public final class UserDialog extends FormDialog<UserDto> {

    public static class Builder {
        private TextField firstName;
        private TextField lastName;
        private Select<Role> role;
        private TextField email;
        private PasswordField inputPassword;
        private PasswordField confirmPassword;

        private UserDialog dialog;

        public Builder forEdit(final UserDto userDto) {
            this.dialog = new UserDialog(userDto);
            this.dialog.prepare(this.formDialogI18n("admin.user.dialog.editHeader"), this.formComponent(false));
            return this;
        }

        public Builder forNew() {
            this.dialog = new UserDialog(new UserDto());
            this.dialog.prepare(this.formDialogI18n("admin.user.dialog.newHeader"), this.formComponent(true));
            return this;
        }

        public Builder withSaveEventListener(final ComponentEventListener<SaveFormDialogEvent<UserDto>> listener) {
            this.dialog.addSaveItemEventListener(listener);
            return this;
        }

        public UserDialog build() {
            return this.dialog;
        }

        private FormDialogI18n formDialogI18n(final String headerKey) {
            final FormDialogI18n formDialogI18n = new FormDialogI18n();
            formDialogI18n.setHeaderTitle(this.dialog.getTranslation(headerKey));
            formDialogI18n.setSaveButtonLabel(this.dialog.getTranslation("admin.user.dialog.saveButtonLabel"));
            formDialogI18n.setCancelButtonLabel(this.dialog.getTranslation("admin.user.dialog.cancelButtonLabel"));
            return formDialogI18n;
        }

        private Component formComponent(final boolean passwordsMandatory) {
            this.firstName = new TextField(this.dialog.getTranslation("admin.user.dialog.firstNameLabel"));
            this.lastName = new TextField(this.dialog.getTranslation("admin.user.dialog.lastNameLabel"));
            this.role = new Select<>(this.dialog.getTranslation("admin.user.dialog.rolesLabel"), event -> {
            }, Role.values());
            this.email = new TextField(this.dialog.getTranslation("admin.user.dialog.emailLabel"));
            this.inputPassword = new PasswordField(this.dialog.getTranslation("admin.user.dialog.inputPasswordLabel"));
            this.confirmPassword = new PasswordField(this.dialog.getTranslation("admin.user.dialog.confirmPasswordLabel"));

            final H4 loginDetailsHeader = new H4(this.dialog.getTranslation("admin.user.dialog.loginDetailsHeader"));

            final VerticalLayout dialogLayout = new VerticalLayout(this.firstName, this.lastName, this.role, loginDetailsHeader, this.email,
                                                                   this.inputPassword, this.confirmPassword);

            dialogLayout.setPadding(false);
            dialogLayout.setSpacing(false);
            dialogLayout.setSizeFull();
            dialogLayout.setAlignItems(FlexComponent.Alignment.STRETCH);

            this.dialog.getBinder().bindInstanceFields(this);

            if (passwordsMandatory) {
                this.enablePasswordValidations();
            } else {
                this.disablePasswordValidations();
            }

            this.inputPassword.addValueChangeListener(event -> {
                if (ObjectUtils.isNotEmpty(event.getValue())) {
                    this.enablePasswordValidations();
                } else if (ObjectUtils.isEmpty(this.confirmPassword.getValue())) {
                    this.disablePasswordValidations();
                    this.dialog.getBinder().validate();
                }
            });

            this.confirmPassword.addValueChangeListener(event -> {
                if (ObjectUtils.isNotEmpty(event.getValue())) {
                    this.enablePasswordValidations();
                } else if (ObjectUtils.isEmpty(this.inputPassword.getValue())) {
                    this.disablePasswordValidations();
                    this.dialog.getBinder().validate();
                }
            });

            return dialogLayout;
        }

        private void enablePasswordValidations() {
            this.dialog.getBinder().getBinding("inputPassword").ifPresent(binding -> binding.setValidatorsDisabled(false));
            this.dialog.getBinder().getBinding("confirmPassword").ifPresent(binding -> binding.setValidatorsDisabled(false));

            final Binding<UserDto, String> confirmPasswordBinding = this.dialog.getBinder()
                                                                               .forField(this.confirmPassword)
                                                                               .withValidator(confirmPass -> confirmPass.equals(this.inputPassword.getValue()),
                                                                                              this.dialog.getTranslation("admin.user.dialog.passwordMismatch"))
                                                                               .bind("confirmPassword");

            this.inputPassword.addValueChangeListener(event -> confirmPasswordBinding.validate());
            this.inputPassword.setRequiredIndicatorVisible(true);
            this.confirmPassword.setRequiredIndicatorVisible(true);
        }

        private void disablePasswordValidations() {
            this.inputPassword.setRequiredIndicatorVisible(false);
            this.confirmPassword.setRequiredIndicatorVisible(false);
            this.dialog.getBinder().getBinding("inputPassword").ifPresent(binding -> binding.setValidatorsDisabled(true));
            this.dialog.getBinder().getBinding("confirmPassword").ifPresent(binding -> binding.setValidatorsDisabled(true));
        }
    }

    private UserDialog(final UserDto userDto) {
        super(UserDto.class, userDto);
    }

}
