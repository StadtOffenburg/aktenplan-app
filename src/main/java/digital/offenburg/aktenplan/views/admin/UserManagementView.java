package digital.offenburg.aktenplan.views.admin;

import java.util.List;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.lang3.ObjectUtils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.lumo.LumoUtility;
import com.vaadin.flow.theme.lumo.LumoUtility.FontSize;

import digital.offenburg.aktenplan.component.ConfirmDialog;
import digital.offenburg.aktenplan.component.ConfirmDialog.ConfirmDialogI18n;
import digital.offenburg.aktenplan.component.LinkButton;
import digital.offenburg.aktenplan.data.constant.Role;
import digital.offenburg.aktenplan.data.dto.UserDto;
import digital.offenburg.aktenplan.data.service.user.UserService;
import digital.offenburg.aktenplan.event.SaveFormDialogEvent;

@PageTitle("User Management | AktenPlan")
@Route(value = "users", layout = AdminView.class)
@RolesAllowed({ Role.Named.ADMIN })
public class UserManagementView extends VerticalLayout {

    private final transient UserService userService;
    private final Grid<UserDto> userGrid;
    private final VerticalLayout gridSection;

    private TextField searchText;
    private Button editBtn;
    private Button deleteBtn;
    private Set<UserDto> selectedUsers;

    public UserManagementView(final UserService userService) {
        this.userService = userService;

        this.setPadding(false);
        this.setSpacing(false);
        this.setSizeFull();

        this.userGrid = this.createGrid();
        this.gridSection = new VerticalLayout();
        this.gridSection.setSizeFull();

        this.add(this.searchBar(), this.gridSection);
        this.refreshGrid();
    }

    private Grid<UserDto> createGrid() {
        final Grid<UserDto> grid = new Grid<>();
        grid.setSelectionMode(Grid.SelectionMode.MULTI);
        grid.setSizeFull();

        grid.addColumn(UserDto::getFirstName)
            .setComparator(UserDto::getFirstName)
            .setHeader(this.getTranslation("admin.user.grid.header.firstName"))
            .setAutoWidth(true);

        grid.addColumn(UserDto::getLastName)
            .setComparator(UserDto::getLastName)
            .setHeader(this.getTranslation("admin.user.grid.header.lastName"))
            .setAutoWidth(true);

        grid.addColumn(UserDto::getEmail)
            .setComparator(UserDto::getEmail)
            .setHeader(this.getTranslation("admin.user.grid.header.email"))
            .setAutoWidth(true);

        grid.addColumn(UserDto::getRole)
            .setHeader(this.getTranslation("admin.user.grid.header.roles"))
            .setAutoWidth(true);

        grid.addItemClickListener(event -> {
            final var model = grid.getSelectionModel();
            if (model.isSelected(event.getItem())) {
                model.deselect(event.getItem());
            } else {
                model.select(event.getItem());
            }
        });

        grid.asMultiSelect().addValueChangeListener(event -> {
            this.selectedUsers = event.getValue();
            this.editBtn.setEnabled(ObjectUtils.isNotEmpty(this.selectedUsers) && this.selectedUsers.size() == 1);
            this.deleteBtn.setEnabled(ObjectUtils.isNotEmpty(this.selectedUsers));

            if (ObjectUtils.isNotEmpty(this.selectedUsers)) {
                this.deleteBtn.addClassNames(LumoUtility.TextColor.ERROR);
            } else {
                this.deleteBtn.removeClassName(LumoUtility.TextColor.ERROR);
            }

        });
        return grid;
    }

    private Component searchBar() {
        this.searchText = new TextField();
        this.searchText.setPlaceholder(this.getTranslation("admin.user.searchPlaceholder"));
        this.searchText.setPrefixComponent(VaadinIcon.SEARCH.create());
        this.searchText.setClearButtonVisible(true);
        this.searchText.setValueChangeMode(ValueChangeMode.LAZY);
        this.searchText.setValueChangeTimeout(1000);
        this.searchText.addValueChangeListener(event -> this.refreshGrid());
        this.searchText.addKeyPressListener(Key.ENTER, event -> this.refreshGrid());
        this.searchText.getStyle().set("flex", "1 1 0");

        this.editBtn = new LinkButton(this.getTranslation("admin.user.button.editLabel"), VaadinIcon.EDIT.create());
        this.editBtn.setEnabled(false);
        this.editBtn.addClickListener(event -> {
            if (ObjectUtils.isNotEmpty(this.selectedUsers) && this.selectedUsers.size() == 1) {
                new UserDialog.Builder()
                                        .forEdit(this.selectedUsers.iterator().next())
                                        .withSaveEventListener(this::saveUser)
                                        .build()
                                        .open();
            }
        });

        this.deleteBtn = new LinkButton(this.getTranslation("admin.user.button.deleteLabel"), VaadinIcon.TRASH.create());
        this.deleteBtn.setEnabled(false);
        this.deleteBtn.addClickListener(event -> {
            final ConfirmDialogI18n confirmDialogI18n = new ConfirmDialogI18n();
            confirmDialogI18n.setTitle(this.getTranslation("admin.user.delete.dialog.title"));
            confirmDialogI18n.setMessage(this.getTranslation("admin.user.delete.dialog.message"));
            confirmDialogI18n.setCancelButtonLabel(this.getTranslation("admin.user.delete.dialog.cancelButtonLabel"));
            confirmDialogI18n.setConfirmButtonLabel(this.getTranslation("admin.user.delete.dialog.confirmButtonLabel"));

            final ConfirmDialog deleteConfirmDialog = new ConfirmDialog(confirmDialogI18n);
            deleteConfirmDialog.addConfirmListener(delEvent -> {
                this.userService.deleteUsers(this.selectedUsers);
                deleteConfirmDialog.close();
                this.refreshGrid();
            });
            deleteConfirmDialog.open();
        });

        final Button newBtn = new Button(this.getTranslation("admin.user.button.newLabel"));
        newBtn.addClassNames("ui-primary-small-button");
        newBtn.addClickListener(event -> {
            new UserDialog.Builder()
                                    .forNew()
                                    .withSaveEventListener(this::saveUser)
                                    .build()
                                    .open();
        });

        final HorizontalLayout buttonsContainer = new HorizontalLayout(this.editBtn, this.deleteBtn, newBtn);
        buttonsContainer.getStyle().set("flex", "4 1 0");
        buttonsContainer.addClassNames(LumoUtility.JustifyContent.END, LumoUtility.AlignItems.CENTER);

        final HorizontalLayout layout = new HorizontalLayout();
        layout.setWidthFull();
        layout.addClassNames(LumoUtility.AlignItems.CENTER, LumoUtility.Background.CONTRAST_5, LumoUtility.Padding.MEDIUM);
        layout.add(this.searchText, buttonsContainer);
        return layout;
    }

    private void saveUser(final SaveFormDialogEvent<UserDto> event) {
        this.userService.saveUser(event.getItem());
        this.refreshGrid();
        event.getSource().close();
    }

    private void refreshGrid() {
        this.gridSection.removeAll();
        final List<UserDto> allUnits = this.userService.fetchAllUsers(this.searchText.getValue());
        if (ObjectUtils.isNotEmpty(allUnits)) {
            this.userGrid.setItems(allUnits);
            this.gridSection.add(this.userGrid);
        } else {
            final Span noDataFound = new Span(this.getTranslation("admin.user.noDataFound"));
            noDataFound.addClassNames(LumoUtility.Padding.LARGE, FontSize.MEDIUM);
            this.gridSection.add(noDataFound);
        }
    }
}