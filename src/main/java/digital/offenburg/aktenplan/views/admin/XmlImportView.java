package digital.offenburg.aktenplan.views.admin;

import java.io.IOException;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dom4j.DocumentException;
import org.springframework.http.MediaType;
import org.xml.sax.SAXException;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.FileBuffer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.lumo.LumoUtility;

import digital.offenburg.aktenplan.component.Alert;
import digital.offenburg.aktenplan.data.constant.Role;
import digital.offenburg.aktenplan.data.service.xml.XmlService;

@PageTitle("XML Import | AktenPlan")
@Route(value = "xmlImport", layout = AdminView.class)
@RolesAllowed({ Role.Named.ADMIN })
public class XmlImportView extends HorizontalLayout {
    private static final Logger LOGGER = LogManager.getLogger(XmlImportView.class);
    private static final int MAX_FILE_SIZE_IN_MB = 20;

    private final transient XmlService xmlService;

    private final Component confirmationComponent;
    private Upload singleFileUpload;
    private FileBuffer buffer;

    public XmlImportView(final XmlService xmlService) {
        this.xmlService = xmlService;
        this.confirmationComponent = this.confirmUpdate();
        this.setSizeFull();

        final FlexLayout leftVoid = new FlexLayout();
        leftVoid.getStyle().set("flex", "1 1 0");

        final VerticalLayout middleContent = this.content();
        middleContent.getStyle().set("flex", "1 1 0");

        final FlexLayout rightVoid = new FlexLayout();
        rightVoid.getStyle().set("flex", "1 1 0");

        this.add(leftVoid, middleContent, rightVoid);
    }

    private VerticalLayout content() {
        final VerticalLayout layout = new VerticalLayout();
        layout.setSpacing(false);
        layout.addClassNames(LumoUtility.Gap.SMALL);

        final H4 heading = new H4(this.getTranslation("admin.xml.uploadHeading"));
        heading.addClassNames(LumoUtility.Margin.Bottom.NONE);

        final Paragraph message = new Paragraph();
        message.setText(this.getTranslation("admin.xml.beforeUploadMessage"));
        message.addClassNames(LumoUtility.Margin.NONE);

        final Button singleFileUploadBtn = new Button(this.getTranslation("admin.xml.uploadButton"));
        singleFileUploadBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        this.buffer = new FileBuffer();
        this.singleFileUpload = new Upload(this.buffer);
        this.singleFileUpload.setWidthFull();
        this.singleFileUpload.setUploadButton(singleFileUploadBtn);
        this.singleFileUpload.setAcceptedFileTypes(MediaType.TEXT_XML_VALUE);
        this.singleFileUpload.setMaxFileSize(MAX_FILE_SIZE_IN_MB * (int) FileUtils.ONE_MB);

        this.singleFileUpload.addSucceededListener(event -> this.confirmationComponent.setVisible(true));

        this.singleFileUpload.addFailedListener(event -> Alert.error(event.getReason().getMessage()));
        this.singleFileUpload.addFileRejectedListener(event -> Alert.error(event.getErrorMessage()));

        this.singleFileUpload.getElement().addEventListener("file-remove", event -> this.clearUploadState());

        layout.add(heading, message, this.singleFileUpload, this.confirmationComponent);
        return layout;
    }

    private Component confirmUpdate() {
        final VerticalLayout layout = new VerticalLayout();
        layout.setVisible(false);
        layout.setSpacing(false);
        layout.setPadding(false);

        layout.addClassNames(LumoUtility.Gap.SMALL);

        final H4 heading = new H4(this.getTranslation("admin.xml.upload.confirm"));
        heading.addClassNames(LumoUtility.Margin.Bottom.NONE);

        final Paragraph message = new Paragraph();
        message.setText(this.getTranslation("admin.xml.upload.afterMessage"));
        message.addClassNames(LumoUtility.Margin.NONE);

        final FlexLayout warningBlock = new FlexLayout();
        warningBlock.setSizeFull();
        warningBlock.add(VaadinIcon.INFO_CIRCLE.create());
        warningBlock.addClassNames(LumoUtility.Padding.MEDIUM, LumoUtility.Border.ALL, LumoUtility.Background.ERROR_10, LumoUtility.Gap.MEDIUM,
                                   LumoUtility.Margin.Vertical.MEDIUM, LumoUtility.BoxSizing.BORDER);

        final Span warningMessage = new Span(this.getTranslation("admin.xml.upload.warningMessage"));
        warningBlock.add(warningMessage);

        final HorizontalLayout buttons = new HorizontalLayout();
        final Button cancelUpdate = new Button(this.getTranslation("admin.xml.upload.button.cancel"), event -> {
            this.clearUploadState();
        });

        final Button proceedWithUpdate = new Button(this.getTranslation("admin.xml.upload.button.proceed"), event -> {
            this.saveUpload();
        });

        buttons.add(cancelUpdate, proceedWithUpdate);

        layout.add(heading, message, warningBlock, buttons);
        return layout;
    }

    private void saveUpload() {
        try {
            this.xmlService.upload(this.buffer.getFileData());
            this.clearUploadState();
            Alert.success(this.getTranslation("admin.xml.upload.notification.success"));
        } catch (final IOException | DocumentException | SAXException ex) {
            LOGGER.fatal(ex.getMessage(), ex);
            Alert.error(this.getTranslation("error.message.fatal"));
        }
    }

    private void clearUploadState() {
        this.singleFileUpload.clearFileList();
        this.confirmationComponent.setVisible(false);
    }

}
