package digital.offenburg.aktenplan.views.favourites;

import java.util.List;
import java.util.Set;

import javax.annotation.security.PermitAll;

import org.apache.commons.lang3.ObjectUtils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.theme.lumo.LumoUtility;
import com.vaadin.flow.theme.lumo.LumoUtility.FontSize;

import digital.offenburg.aktenplan.component.ConfirmDialog;
import digital.offenburg.aktenplan.component.ConfirmDialog.ConfirmDialogI18n;
import digital.offenburg.aktenplan.component.LinkButton;
import digital.offenburg.aktenplan.data.dto.UserFavouriteDto;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.service.AktenPlanService;
import digital.offenburg.aktenplan.views.MainLayout;
import digital.offenburg.aktenplan.views.search.MainView;

@PageTitle("Favourites | AktenPlan")
@Route(value = "favourites", layout = MainLayout.class)
@PermitAll
public class FavouritesView extends VerticalLayout {

    private final transient AktenPlanService aktenPlanService;
    private final Grid<UserFavouriteDto> favouritesGrid;
    private final VerticalLayout gridSection;

    private TextField searchText;
    private Button deleteBtn;
    private Set<UserFavouriteDto> selectedFavourites;

    public FavouritesView(final AktenPlanService aktenPlanService) {
        this.aktenPlanService = aktenPlanService;

        this.setPadding(false);
        this.setSpacing(false);
        this.setSizeFull();

        this.favouritesGrid = this.createGrid();
        this.gridSection = new VerticalLayout();
        this.gridSection.setSizeFull();

        this.add(this.searchBar(), this.gridSection);
        this.refreshGrid();
    }

    private Grid<UserFavouriteDto> createGrid() {
        final Grid<UserFavouriteDto> grid = new Grid<>();
        grid.setSelectionMode(Grid.SelectionMode.MULTI);
        grid.setSizeFull();

        grid.addColumn(new ComponentRenderer<>(item -> {
            final FileReferenceDto ref = item.getFileReference();
            final Span planNumber = new Span(ref.getId());
            if (ref.isActive()) {
                planNumber.addClassNames(LumoUtility.FontWeight.BOLD);
            }
            final Span textContent = new Span(ref.getTitle());

            final Span span = new Span(planNumber, new Text("-"), textContent);
            span.addClassName("ui-file-reference-label-in-grid");

            if (!ref.isActive()) {
                final Icon infoIcon = VaadinIcon.INFO_CIRCLE.create();
                infoIcon.addClassNames("ui-item-inactive");
                infoIcon.getElement().setAttribute("title", this.getTranslation("search.view.inactive.tooltip"));
                span.add(infoIcon);
                span.addClassNames("ui-file-reference-inactive");
            }
            return span;
        }))
            .setComparator(item -> item.getFileReference().getId())
            .setHeader(this.getTranslation("favourite.grid.header.itemLabel"))
            .setAutoWidth(true);

        grid.addColumn(new ComponentRenderer<>(userFavouriteDto -> {
            final RouterLink routerLink = new RouterLink("", MainView.class, userFavouriteDto.getFileReference().getId());
            routerLink.add(VaadinIcon.ANGLE_RIGHT.create());
            routerLink.getElement()
                      .setAttribute("aria-label",
                                    this.getTranslation("favourite.aria.linkNavigation") + userFavouriteDto.getFileReference().getId());
            routerLink.addClassNames(LumoUtility.TextColor.BODY);
            return routerLink;
        })).setFlexGrow(0);

        grid.addItemClickListener(event -> {
            final var model = grid.getSelectionModel();
            if (model.isSelected(event.getItem())) {
                model.deselect(event.getItem());
            } else {
                model.select(event.getItem());
            }
        });

        grid.asMultiSelect().addValueChangeListener(event -> {
            this.selectedFavourites = event.getValue();
            this.deleteBtn.setEnabled(ObjectUtils.isNotEmpty(this.selectedFavourites));

            if (ObjectUtils.isNotEmpty(this.selectedFavourites)) {
                this.deleteBtn.addClassNames(LumoUtility.TextColor.ERROR);
            } else {
                this.deleteBtn.removeClassName(LumoUtility.TextColor.ERROR);
            }
        });
        return grid;
    }

    private Component searchBar() {
        this.searchText = new TextField();
        this.searchText.setPlaceholder(this.getTranslation("favourite.searchPlaceholder"));
        this.searchText.setPrefixComponent(VaadinIcon.SEARCH.create());
        this.searchText.setClearButtonVisible(true);
        this.searchText.setValueChangeMode(ValueChangeMode.LAZY);
        this.searchText.setValueChangeTimeout(1000);
        this.searchText.addValueChangeListener(event -> this.refreshGrid());
        this.searchText.addKeyPressListener(Key.ENTER, event -> this.refreshGrid());
        this.searchText.getStyle().set("flex", "1 1 0");

        this.deleteBtn = new LinkButton(this.getTranslation("favourite.button.deleteLabel"), VaadinIcon.STAR_O.create());
        this.deleteBtn.setEnabled(false);
        this.deleteBtn.addClickListener(event -> {

            final ConfirmDialogI18n confirmDialogI18n = new ConfirmDialogI18n();
            confirmDialogI18n.setTitle(this.getTranslation("favourite.delete.dialog.title"));
            confirmDialogI18n.setMessage(this.getTranslation("favourite.delete.dialog.message"));
            confirmDialogI18n.setCancelButtonLabel(this.getTranslation("favourite.delete.dialog.cancelButtonLabel"));
            confirmDialogI18n.setConfirmButtonLabel(this.getTranslation("favourite.delete.dialog.confirmButtonLabel"));

            final ConfirmDialog deleteConfirmDialog = new ConfirmDialog(confirmDialogI18n);
            deleteConfirmDialog.addConfirmListener(delEvent -> {
                this.aktenPlanService.deleteUserFavourites(this.selectedFavourites);
                deleteConfirmDialog.close();
                this.refreshGrid();
            });
            deleteConfirmDialog.open();
        });

        final HorizontalLayout buttonsContainer = new HorizontalLayout(this.deleteBtn);
        buttonsContainer.getStyle().set("flex", "4 1 0");
        buttonsContainer.addClassNames(LumoUtility.JustifyContent.END, LumoUtility.AlignItems.CENTER);

        final HorizontalLayout layout = new HorizontalLayout();
        layout.setWidthFull();
        layout.addClassNames(LumoUtility.AlignItems.CENTER, LumoUtility.Background.CONTRAST_5, LumoUtility.Padding.MEDIUM);
        layout.add(this.searchText, buttonsContainer);
        return layout;
    }

    private void refreshGrid() {
        this.gridSection.removeAll();
        final List<UserFavouriteDto> allUserFavourites = this.aktenPlanService.fetchUserFavourites(this.searchText.getValue());
        if (ObjectUtils.isNotEmpty(allUserFavourites)) {
            this.favouritesGrid.setItems(allUserFavourites);
            this.gridSection.add(this.favouritesGrid);
        } else {
            final Span noDataFound = new Span(this.getTranslation("favourite.noDataFound"));
            noDataFound.addClassNames(LumoUtility.Padding.LARGE, FontSize.MEDIUM);
            this.gridSection.add(noDataFound);
        }
    }

}
