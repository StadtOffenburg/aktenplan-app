package digital.offenburg.aktenplan.views.login;

import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import digital.offenburg.aktenplan.component.OffenburgBranding;

@PageTitle("Login")
@Route(value = "login")
public class LoginView extends LoginOverlay {

    public LoginView() {
        this.setAction("login");
        this.setTitle(new OffenburgBranding());
        this.setForgotPasswordButtonVisible(false);
        this.setOpened(true);
    }

}
