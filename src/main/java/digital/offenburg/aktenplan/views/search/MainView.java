package digital.offenburg.aktenplan.views.search;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.annotation.security.PermitAll;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.GridSortOrderBuilder;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout.Orientation;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.provider.hierarchy.TreeDataProvider;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasDynamicTitle;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.theme.lumo.LumoUtility;
import com.vaadin.flow.theme.lumo.LumoUtility.AlignItems;
import com.vaadin.flow.theme.lumo.LumoUtility.FontSize;
import com.vaadin.flow.theme.lumo.LumoUtility.Gap;
import com.vaadin.flow.theme.lumo.LumoUtility.MinWidth;
import com.vaadin.flow.theme.lumo.LumoUtility.Padding;
import com.vaadin.flow.theme.lumo.LumoUtility.TextColor;

import digital.offenburg.aktenplan.component.InfoDialog;
import digital.offenburg.aktenplan.component.LinkButton;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.dto.schema.SchemaDto;
import digital.offenburg.aktenplan.data.dto.schema.SchemaEntryDto;
import digital.offenburg.aktenplan.data.service.AktenPlanService;
import digital.offenburg.aktenplan.data.view.SortBy;
import digital.offenburg.aktenplan.event.FileReferenceDetailsClosedEvent;
import digital.offenburg.aktenplan.event.MainViewEnterEvent;
import digital.offenburg.aktenplan.event.SortEvent;
import digital.offenburg.aktenplan.views.search.details.FilePlanDetailsView;

@Route(value = "main", layout = MainViewLayout.class)
@PermitAll
public class MainView extends HorizontalLayout implements HasUrlParameter<String>, HasDynamicTitle, BeforeEnterObserver {
    private static final String CLASS_TO_HIDE_SPLITTER = "ui-aktenplan-split-view";
    private static final double SPLITTER_POSITION_WHEN_SHOWING_SCHEMA = 50d;
    private static final int MAX_TREEGRID_RECURSIVE_DEPTH = 6;

    private final transient AktenPlanService aktenPlanService;

    private String pageTitle;

    private VerticalLayout detailsLayout;
    private SplitLayout splitSectionLayout;
    private VerticalLayout schemaSection;

    private TreeGrid<FileReferenceDto> fileGrid;
    private List<GridSortOrder<FileReferenceDto>> ascendingSort;
    private List<GridSortOrder<FileReferenceDto>> descendingSort;
    private Set<FileReferenceDto> filePlanData;
    private FileReferenceDto selectedFileReference;

    private String parameter;
    private SortBy sortBy;
    private Span noDataFound;

    private Checkbox showInactiveItemsCheckBox;

    public MainView(final AktenPlanService aktenPlanService) {
        this.aktenPlanService = aktenPlanService;
        this.parameter = null;
        this.filePlanData = Collections.emptySet();
        this.selectedFileReference = null;
        this.setSizeFull();
        this.setSpacing(false);
        this.setPadding(false);
        this.setupView();
    }

    private void setupView() {
        this.removeAll();

        this.splitSectionLayout = this.createSplitView();
        this.splitSectionLayout.setVisible(false);

        this.detailsLayout = this.createDetailsView();

        this.noDataFound = new Span(this.getTranslation("search.view.noDataFound"));
        this.noDataFound.addClassNames(LumoUtility.Padding.LARGE, FontSize.MEDIUM);
        this.noDataFound.setVisible(false);

        this.add(this.splitSectionLayout);
        this.add(this.detailsLayout);
        this.add(this.noDataFound);
    }

    private SplitLayout createSplitView() {
        final SplitLayout layout = new SplitLayout();
        layout.addClassNames(CLASS_TO_HIDE_SPLITTER);
        layout.getStyle().set("flex", "4 1 0");
        layout.getStyle().set("border-right", "1px solid var(--lumo-contrast-20pct)");
        layout.addToPrimary(this.gridSection());

        this.schemaSection = new VerticalLayout();
        this.schemaSection.setPadding(false);
        this.schemaSection.addClassNames(LumoUtility.Padding.Vertical.MEDIUM, LumoUtility.Padding.Left.MEDIUM);

        layout.addToSecondary(this.schemaSection);
        layout.setOrientation(Orientation.VERTICAL);
        layout.setSplitterPosition(100d);
        return layout;
    }

    private VerticalLayout createDetailsView() {
        final VerticalLayout detailsLayout = new VerticalLayout();
        detailsLayout.setSizeFull();
        detailsLayout.getStyle().set("flex", "2 1 0");
        detailsLayout.setVisible(false);
        detailsLayout.setPadding(false);
        detailsLayout.setSpacing(false);
        return detailsLayout;
    }

    private VerticalLayout gridSection() {
        this.fileGrid = new TreeGrid<>();
        this.fileGrid.addThemeVariants(GridVariant.LUMO_NO_ROW_BORDERS, GridVariant.LUMO_NO_BORDER);
        final Column<FileReferenceDto> gridCol = this.fileGrid.addComponentHierarchyColumn(item -> {
            final Span planNumber = new Span(item.getId());
            if (item.isActive()) {
                planNumber.addClassNames(LumoUtility.FontWeight.BOLD);
            }

            final Span textContent = new Span(item.getTitle());

            final Span span = new Span(planNumber, new Text("-"), textContent);
            span.addClassName("ui-file-reference-label-in-grid");

            if (item.isUserFavourite()) {
                final Icon addedIcon = VaadinIcon.STAR.create();
                addedIcon.addClassNames(FontSize.XXSMALL, item.isActive() ? "ui-fav-star" : "ui-fav-star-inactive");
                span.add(addedIcon);
            }

            if (!item.isActive()) {
                final Icon infoIcon = VaadinIcon.INFO_CIRCLE.create();
                infoIcon.addClassNames("ui-item-inactive");
                infoIcon.getElement().setAttribute("title", this.getTranslation("search.view.inactive.tooltip"));
                span.add(infoIcon);
                span.addClassNames("ui-file-reference-inactive");
            }
            return span;
        });
        gridCol.setComparator(FileReferenceDto::getId);

        this.ascendingSort = new GridSortOrderBuilder<FileReferenceDto>().thenAsc(gridCol).build();
        this.descendingSort = new GridSortOrderBuilder<FileReferenceDto>().thenDesc(gridCol).build();

        this.sortFileGrid(this.sortBy);

        this.fileGrid.addExpandListener(event -> {
            if (event.isFromClient()) {
                event.getItems().forEach(item -> {
                    this.fileGrid.getSelectionModel().select(item);
                });
            }
        });

        this.fileGrid.addCollapseListener(event -> {
            if (event.isFromClient()) {
                event.getItems().forEach(item -> {
                    this.fileGrid.getSelectionModel().select(item);
                });
            }
        });

        this.fileGrid.addSelectionListener(event -> {
            event.getFirstSelectedItem().ifPresentOrElse(this::showSelection, this::hideSelection);
        });

        final VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);
        layout.addClassNames(LumoUtility.Padding.Vertical.MEDIUM, LumoUtility.Padding.Left.MEDIUM);
        layout.add(this.fileGridHeader(), this.fileGrid);
        return layout;
    }

    private Component fileGridHeader() {
        final Span headerText = new Span(this.getTranslation("search.view.mainViewHeader"));
        headerText.addClassName("ui-section-heading");

        this.showInactiveItemsCheckBox = new Checkbox(this.getTranslation("search.view.checkbox.includeInactive"),
                                                      false,
                                                      event -> this.filterFileGrid(event.getValue()));

        this.showInactiveItemsCheckBox.addValueChangeListener(event -> {
            final boolean isInactive = this.selectedFileReference != null && !this.selectedFileReference.isActive();
            if (isInactive && !event.getValue()) {
                this.clearFileGridSelection();
            }
        });

        final LinkButton inactiveCheckboxHelp = new LinkButton(VaadinIcon.QUESTION_CIRCLE.create());
        inactiveCheckboxHelp.addClassNames(Padding.NONE, TextColor.SECONDARY, MinWidth.NONE);
        inactiveCheckboxHelp.addClickListener(event -> {
            final InfoDialog info = new InfoDialog(this.getTranslation("search.view.includeInactive.infoDialog.heading"), this.getTranslation("search.view.includeInactive.infoDialog.body"));
            info.open();
        });

        final HorizontalLayout inactiveItems = new HorizontalLayout();
        inactiveItems.add(this.showInactiveItemsCheckBox, inactiveCheckboxHelp);
        inactiveItems.addClassNames(Gap.XSMALL, AlignItems.BASELINE);

        final LinkButton collapseAll = new LinkButton(this.getTranslation("search.view.collapseAll"), VaadinIcon.ANGLE_UP.create());
        collapseAll.addClickListener(event -> {
            this.fileGrid.collapseRecursively(this.filePlanData, MAX_TREEGRID_RECURSIVE_DEPTH);
            this.clearFileGridSelection();
        });

        final HorizontalLayout layout = new HorizontalLayout();
        layout.addClassNames(Gap.XLARGE, AlignItems.BASELINE);
        layout.add(headerText, inactiveItems, collapseAll);
        return layout;
    }

    private void filterFileGrid(final boolean includeInactive) {
        final TreeDataProvider<FileReferenceDto> dataProvider = (TreeDataProvider<FileReferenceDto>) this.fileGrid.getDataProvider();
        if (includeInactive) {
            dataProvider.setFilter(null);
        } else {
            dataProvider.setFilterByValue(FileReferenceDto::isActive, Boolean.TRUE);
        }
    }

    private void showSelection(final FileReferenceDto fileReference) {
        this.selectedFileReference = fileReference;
        final FileReferenceDto detailedReference = this.aktenPlanService.fetchDetails(fileReference);
        this.setupDetailsSection(detailedReference);

        if (ObjectUtils.isNotEmpty(fileReference.getSchemaRefId())) {
            this.aktenPlanService.fetchSchemaById(fileReference.getSchemaRefId())
                                 .ifPresentOrElse(this::showSchemaSection, this::hideSchemaSection);
        } else {
            this.hideSchemaSection();
        }
    }

    private void setupDetailsSection(final FileReferenceDto fileReference) {
        this.detailsLayout.removeAll();
        this.detailsLayout.setVisible(true);

        final FilePlanDetailsView filePlanDetailsView = new FilePlanDetailsView(this.aktenPlanService, fileReference);
        filePlanDetailsView.addFavouriteUpdateEventListener(event -> {
            this.fileGrid.getDataProvider().refreshItem(event.getFileReference());
        });
        this.detailsLayout.add(filePlanDetailsView);
    }

    private void hideSelection() {
        this.selectedFileReference = null;
        this.detailsLayout.removeAll();
        this.detailsLayout.setVisible(false);
        this.hideSchemaSection();
    }

    private void clearFileGridSelection() {
        this.hideSelection();
        this.fileGrid.getSelectionModel().deselectAll();
    }

    private void showSchemaSection(final SchemaDto schema) {
        this.splitSectionLayout.removeClassName(CLASS_TO_HIDE_SPLITTER);
        this.splitSectionLayout.setSplitterPosition(SPLITTER_POSITION_WHEN_SHOWING_SCHEMA);
        this.schemaSection.removeAll();

        final TreeGrid<SchemaEntryDto> schemaGrid = new TreeGrid<>();
        schemaGrid.addThemeVariants(GridVariant.LUMO_NO_ROW_BORDERS, GridVariant.LUMO_NO_BORDER);
        schemaGrid.setItems(schema.getEntries(), SchemaEntryDto::getSubEntry);
        final Column<SchemaEntryDto> gridCol = schemaGrid.addComponentHierarchyColumn(entry -> {
            final Span planNumber = new Span(entry.getId().getEntryNumber());
            planNumber.addClassNames(LumoUtility.FontWeight.BOLD);

            final Span textContent = new Span(String.format(LocaleContextHolder.getLocale(), " - %s", entry.getText()));
            return new Span(planNumber, textContent);
        });
        gridCol.setComparator(item -> item.getId().getEntryNumber());
        schemaGrid.sort(new GridSortOrderBuilder<SchemaEntryDto>().thenAsc(gridCol).build());

        final String schemaHeadingText = String.format(LocaleContextHolder.getLocale(), "%s - %s", this.getTranslation("search.view.schema.heading"), schema.getName());
        final Span schemaHeading = new Span(schemaHeadingText);
        schemaHeading.addClassName("ui-section-heading");
        this.schemaSection.add(schemaHeading, schemaGrid);
    }

    private void hideSchemaSection() {
        this.splitSectionLayout.addClassName(CLASS_TO_HIDE_SPLITTER);
        this.splitSectionLayout.setSplitterPosition(100d);
        this.schemaSection.removeAll();
    }

    private void sortFileGrid(final SortBy sortBy) {
        switch (Optional.ofNullable(sortBy).orElse(SortBy.ASCENDING)) {
            case ASCENDING -> {
                this.fileGrid.sort(this.ascendingSort);
            }
            case DESCENDING -> {
                this.fileGrid.sort(this.descendingSort);
            }
        }
    }

    @Override
    protected void onAttach(final AttachEvent attachEvent) {
        super.onAttach(attachEvent);
        ComponentUtil.addListener(attachEvent.getUI(), FileReferenceDetailsClosedEvent.class, event -> this.clearFileGridSelection());
        ComponentUtil.addListener(attachEvent.getUI(), SortEvent.class, event -> this.sortFileGrid(event.getSortBy()));
    }

    @Override
    public void setParameter(final BeforeEvent event, @OptionalParameter final String parameter) {
        this.parameter = parameter;
    }

    @Override
    public String getPageTitle() {
        return Optional.ofNullable(this.pageTitle).orElseGet(() -> this.getTranslation("page.title.main"));
    }

    @Override
    public void beforeEnter(final BeforeEnterEvent event) {
        this.sortBy = VaadinSession.getCurrent().getAttribute(SortBy.class);
        boolean includeInactiveItems = false;

        if (ObjectUtils.isNotEmpty(this.parameter)) {
            this.filePlanData = this.aktenPlanService.forFileNumber(this.parameter);
            this.fileGrid.setItems(this.filePlanData, FileReferenceDto::getSubReference);
            final Optional<FileReferenceDto> found = this.gridAutoSelection(this.filePlanData);
            includeInactiveItems = found.isPresent() && !found.get().isActive();
        } else {
            this.filePlanData = this.aktenPlanService.fetchFilePlanHierarchy();
            this.fileGrid.setItems(this.filePlanData, FileReferenceDto::getSubReference);
            this.pageTitle = null;
        }

        this.filterFileGrid(includeInactiveItems);
        this.showInactiveItemsCheckBox.setValue(includeInactiveItems);

        this.splitSectionLayout.setVisible(ObjectUtils.isNotEmpty(this.filePlanData));
        this.noDataFound.setVisible(ObjectUtils.isEmpty(this.filePlanData));

        ComponentUtil.fireEvent(UI.getCurrent(), new MainViewEnterEvent(this));
    }

    private Optional<FileReferenceDto> gridAutoSelection(final Set<FileReferenceDto> fileReferences) {
        Optional<FileReferenceDto> found = Optional.empty();
        if (ObjectUtils.isNotEmpty(fileReferences)) {
            final Iterator<FileReferenceDto> refItr = fileReferences.iterator();
            while (refItr.hasNext() && found.isEmpty()) {
                final FileReferenceDto fileRef = refItr.next();
                this.fileGrid.expand(fileRef);
                if (fileRef.isSelected()) {
                    this.fileGrid.select(fileRef);
                    this.pageTitle = fileRef.getTitle();
                    found = Optional.of(fileRef);
                } else {
                    found = this.gridAutoSelection(fileRef.getSubReference());
                }
            }
        }
        return found;
    }
}
