package digital.offenburg.aktenplan.views.search;

import javax.annotation.security.PermitAll;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.ParentLayout;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.server.VaadinSession;

import digital.offenburg.aktenplan.data.service.AktenPlanService;
import digital.offenburg.aktenplan.data.view.SearchOptions;
import digital.offenburg.aktenplan.views.MainLayout;

@ParentLayout(value = MainLayout.class)
@PermitAll
public class MainViewLayout extends VerticalLayout implements RouterLayout {
    private final SearchPanel searchPanel;

    public MainViewLayout(final AktenPlanService aktenPlanService) {
        this.setSizeFull();
        this.setPadding(false);
        this.setSpacing(false);

        this.searchPanel = new SearchPanel(aktenPlanService);
        this.add(this.searchPanel);
    }

    @Override
    protected void onAttach(final AttachEvent attachEvent) {
        super.onAttach(attachEvent);

        this.searchPanel.addSearchEventListener(event -> {
            final SearchOptions searchOptions = event.getSearchOptions();
            VaadinSession.getCurrent().setAttribute(SearchOptions.class, searchOptions);
            if (searchOptions.hasData()) {
                UI.getCurrent().navigate(SearchView.class);
            } else {
                UI.getCurrent().navigate(MainView.class);
            }
        });
    }

}
