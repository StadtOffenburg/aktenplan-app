package digital.offenburg.aktenplan.views.search;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.theme.lumo.LumoUtility;
import com.vaadin.flow.theme.lumo.LumoUtility.FlexWrap;

import digital.offenburg.aktenplan.component.Alert;
import digital.offenburg.aktenplan.component.LinkButton;
import digital.offenburg.aktenplan.data.dto.AdministrativeUnitDto;
import digital.offenburg.aktenplan.data.service.AktenPlanService;
import digital.offenburg.aktenplan.data.view.BooleanSelection;
import digital.offenburg.aktenplan.data.view.SearchOptions;
import digital.offenburg.aktenplan.data.view.SortBy;
import digital.offenburg.aktenplan.event.MainViewEnterEvent;
import digital.offenburg.aktenplan.event.SearchEvent;
import digital.offenburg.aktenplan.event.SortEvent;

public class SearchPanel extends VerticalLayout implements BeforeEnterObserver {
    private static final Logger LOGGER = LogManager.getLogger(SearchPanel.class);

    private final transient AktenPlanService aktenPlanService;

    private final Binder<SearchOptions> binder;
    private final RouterLink backToSearchResultsLink;

    private SearchOptions searchOptions;

    private TextField searchText;
    private TextField fileNumber;
    private Select<AdministrativeUnitDto> unitSelect;
    private Select<BooleanSelection> commentsSelect;
    private Select<BooleanSelection> excludeFavouritesSelect;

    private Select<SortBy> sortBySelect;
    private Select<BooleanSelection> includeInactive;
    private Button resetBtn;

    public SearchPanel(final AktenPlanService aktenPlanService) {
        this.aktenPlanService = aktenPlanService;
        this.setWidthFull();
        this.setPadding(false);
        this.addClassNames(LumoUtility.Background.CONTRAST_10, LumoUtility.Padding.LARGE);

        this.searchOptions = new SearchOptions();

        final Component moreOptionsPanel = this.moreSearchOptions();
        this.add(this.searchBar(moreOptionsPanel));
        this.add(moreOptionsPanel);

        this.backToSearchResultsLink = this.createBackToSearchResultsLink();
        this.add(this.backToSearchResultsLink);

        this.binder = new BeanValidationBinder<>(SearchOptions.class);
        this.binder.bindInstanceFields(this);
        this.binder.readBean(this.searchOptions);
        this.binder.addValueChangeListener(event -> this.notifyUpdateSearchResults());
    }

    private RouterLink createBackToSearchResultsLink() {
        final RouterLink searchResultLink = new RouterLink("", SearchView.class);
        final Span linkText = new Span(this.getTranslation("search.view.backToSearchLink"));
        final HorizontalLayout content = new HorizontalLayout(VaadinIcon.ARROW_LEFT.create(), linkText);
        content.setPadding(false);
        content.setAlignItems(Alignment.END);
        searchResultLink.add(content);
        searchResultLink.addClassNames("ui-section-heading", LumoUtility.Display.INLINE_FLEX);
        searchResultLink.setVisible(false);
        return searchResultLink;
    }

    private Component searchBar(final Component moreOptionsPanel) {
        final HorizontalLayout searchBar = new HorizontalLayout();
        searchBar.setWidthFull();

        this.searchText = new TextField();
        this.searchText.getElement().setAttribute("aria-label", "search");
        this.searchText.getStyle().set("flex", "6 1 0");

        this.searchText.setPlaceholder(this.getTranslation("search.name.placeholder"));
        this.searchText.setClearButtonVisible(true);
        this.searchText.setPrefixComponent(VaadinIcon.SEARCH.create());
        this.searchText.setValueChangeMode(ValueChangeMode.LAZY);
        this.searchText.setValueChangeTimeout(1000);
        this.searchText.addKeyPressListener(Key.ENTER, event -> this.notifyUpdateSearchResults());

        final FlexLayout moreSection = new FlexLayout();
        moreSection.getStyle().set("flex", "1 1 0");
        moreSection.setJustifyContentMode(JustifyContentMode.BETWEEN);

        final Button toggleMoreOptions = new Button(this.getTranslation("search.button.more"), VaadinIcon.ANGLE_DOWN.create());
        toggleMoreOptions.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        toggleMoreOptions.addClassNames(LumoUtility.FontWeight.MEDIUM, LumoUtility.FontSize.LARGE);

        toggleMoreOptions.addClickListener(event -> {
            if (moreOptionsPanel.isVisible()) {
                this.closeMoreOptions(moreOptionsPanel, toggleMoreOptions);
            } else {
                this.openMoreOptions(moreOptionsPanel, toggleMoreOptions);
            }
        });

        this.resetBtn = new LinkButton(this.getTranslation("search.button.reset"), VaadinIcon.FILTER.create());
        this.resetBtn.addThemeVariants(ButtonVariant.LUMO_ICON);
        this.resetBtn.addClickListener(event -> {
            this.closeMoreOptions(moreOptionsPanel, toggleMoreOptions);
            this.binder.readBean(new SearchOptions());
            this.notifyUpdateSearchResults();
            this.sortBySelect.setValue(SortBy.ASCENDING);
        });

        moreSection.add(this.resetBtn, toggleMoreOptions);
        searchBar.add(this.searchText, moreSection);

        return searchBar;
    }

    private void openMoreOptions(final Component moreOptionsPanel, final Button moreOptionsBtn) {
        moreOptionsPanel.setVisible(true);
        moreOptionsBtn.setIcon(VaadinIcon.ANGLE_UP.create());
    }

    private void closeMoreOptions(final Component moreOptionsPanel, final Button moreOptionsBtn) {
        moreOptionsPanel.setVisible(false);
        moreOptionsBtn.setIcon(VaadinIcon.ANGLE_DOWN.create());
    }

    private Component moreSearchOptions() {
        final HorizontalLayout layout = new HorizontalLayout();
        layout.setVisible(false);
        layout.setSizeFull();

        final HorizontalLayout fieldContainer = new HorizontalLayout();
        fieldContainer.getStyle().set("flex", "6 1 0");
        fieldContainer.addClassNames(FlexWrap.WRAP);

        this.fileNumber = new TextField();
        this.fileNumber.setClearButtonVisible(true);
        this.fileNumber.setValueChangeMode(ValueChangeMode.LAZY);
        this.fileNumber.setValueChangeTimeout(1000);
        this.fileNumber.setLabel(this.getTranslation("search.field.filenumber"));
        this.fileNumber.getStyle().set("flex", "1 1 0");
        this.fileNumber.addKeyPressListener(Key.ENTER, event -> this.notifyUpdateSearchResults());

        this.unitSelect = new Select<>();
        this.unitSelect.setLabel(this.getTranslation("search.field.administrativeUnit"));
        this.unitSelect.getStyle().set("flex", "1 1 0");
        this.unitSelect.setItems(this.aktenPlanService.fetchAllUnits());
        this.unitSelect.setItemLabelGenerator(item -> item != null ? item.getName() : "");
        this.unitSelect.setEmptySelectionAllowed(true);
        this.unitSelect.setEmptySelectionCaption(this.getTranslation(BooleanSelection.ALL.getKey()));

        this.commentsSelect = new Select<>();
        this.commentsSelect.setLabel(this.getTranslation("search.field.commentsAvailable"));
        this.commentsSelect.setItems(BooleanSelection.values());
        this.commentsSelect.setItemLabelGenerator(item -> this.getTranslation(item.getKey()));
        this.commentsSelect.getStyle().set("flex", "1 1 0");

        this.excludeFavouritesSelect = new Select<>();
        this.excludeFavouritesSelect.setLabel(this.getTranslation("search.field.excludeFavourites"));
        this.excludeFavouritesSelect.setItems(List.of(BooleanSelection.YES, BooleanSelection.NO));
        this.excludeFavouritesSelect.setItemLabelGenerator(item -> this.getTranslation(item.getKey()));
        this.excludeFavouritesSelect.getStyle().set("flex", "1 1 0");
        this.excludeFavouritesSelect.setEnabled(this.aktenPlanService.currentUser().isPresent());

        this.sortBySelect = new Select<>();
        this.sortBySelect.setLabel(this.getTranslation("search.field.sortBy"));
        this.sortBySelect.setItems(SortBy.values());
        this.sortBySelect.setItemLabelGenerator(item -> this.getTranslation(item.getKey()));
        this.sortBySelect.getStyle().set("flex", "1 1 0");
        this.sortBySelect.addValueChangeListener(event -> {
            VaadinSession.getCurrent().setAttribute(SortBy.class, event.getValue());
            ComponentUtil.fireEvent(UI.getCurrent(), new SortEvent(this, event.getValue()));
        });
        this.sortBySelect.setValue(SortBy.ASCENDING);

        this.includeInactive = new Select<>();
        this.includeInactive.setLabel(this.getTranslation("search.field.includeInactive"));
        this.includeInactive.setItems(List.of(BooleanSelection.YES, BooleanSelection.NO));
        this.includeInactive.setItemLabelGenerator(item -> this.getTranslation(item.getKey()));
        this.includeInactive.getStyle().set("flex", "1 1 0");

        fieldContainer.add(this.fileNumber, this.unitSelect, this.commentsSelect, this.excludeFavouritesSelect, this.sortBySelect, this.includeInactive);

        final FlexLayout resetSection = new FlexLayout();
        resetSection.setAlignItems(Alignment.END);
        resetSection.setJustifyContentMode(JustifyContentMode.END);
        resetSection.getStyle().set("flex", "1 1 0");

        layout.add(fieldContainer, resetSection);
        return layout;
    }

    private void notifyUpdateSearchResults() {
        try {
            this.binder.writeBean(this.searchOptions);
            this.fireEvent(new SearchEvent(this, this.searchOptions));

            if (this.searchOptions.hasData()) {
                this.resetBtn.addClassName(LumoUtility.TextColor.ERROR);
            } else {
                this.resetBtn.removeClassName(LumoUtility.TextColor.ERROR);
            }
        } catch (final ValidationException ex) {
            LOGGER.fatal(ex.getMessage(), ex);
            Alert.error(this.getTranslation("error.message.fatal"));
        }
    }

    public Registration addSearchEventListener(final ComponentEventListener<SearchEvent> listener) {
        return this.addListener(SearchEvent.class, listener);
    }

    @Override
    public void beforeEnter(final BeforeEnterEvent beforeEnterEvent) {
        this.backToSearchResultsLink.setVisible(false);

        final SearchOptions prevSearchOptions = VaadinSession.getCurrent().getAttribute(SearchOptions.class);
        if (prevSearchOptions != null) {
            this.searchOptions = prevSearchOptions;
            this.binder.readBean(prevSearchOptions);
        }

        final SortBy sortBy = VaadinSession.getCurrent().getAttribute(SortBy.class);
        if (sortBy != null) {
            this.sortBySelect.setValue(sortBy);
        }

        ComponentUtil.addListener(beforeEnterEvent.getUI(), MainViewEnterEvent.class, event -> {
            if (event.getSource() instanceof MainView) {
                this.backToSearchResultsLink.setVisible(this.searchOptions.hasData());
            }
        });
    }

}
