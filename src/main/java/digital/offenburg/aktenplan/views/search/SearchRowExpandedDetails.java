package digital.offenburg.aktenplan.views.search;

import java.util.List;

import org.apache.commons.lang3.ObjectUtils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.theme.lumo.LumoUtility.FontWeight;
import com.vaadin.flow.theme.lumo.LumoUtility.Padding;
import com.vaadin.flow.theme.lumo.LumoUtility.TextColor;

import digital.offenburg.aktenplan.data.dto.GuidanceDto;
import digital.offenburg.aktenplan.data.dto.UserNoteDto;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.service.AktenPlanService;
import digital.offenburg.aktenplan.data.view.SearchOptions;

public class SearchRowExpandedDetails extends VerticalLayout {

    private final transient AktenPlanService aktenPlanService;

    private final FileReferenceDto fileReferenceDto;
    private final SearchOptions searchOptions;

    public SearchRowExpandedDetails(final AktenPlanService aktenPlanService, final FileReferenceDto fileReferenceDto, final SearchOptions searchOptions) {
        this.aktenPlanService = aktenPlanService;
        this.fileReferenceDto = fileReferenceDto;
        this.searchOptions = searchOptions;
        this.setup();
    }

    private void setup() {
        this.setPadding(false);
        this.addClassNames(Padding.Bottom.MEDIUM);

        final List<GuidanceDto> guidances = this.aktenPlanService.findGuidances(this.fileReferenceDto, this.searchOptions.getSearchText(), this::searchHighlighter);
        if (ObjectUtils.isNotEmpty(guidances)) {
            this.add(this.guidanceSection(guidances));
        }

        final List<UserNoteDto> notes = this.aktenPlanService.findUserNotes(this.fileReferenceDto, this.searchOptions.getSearchText(), this::searchHighlighter);
        if (ObjectUtils.isNotEmpty(notes)) {
            this.add(this.notesSection(notes));
        }
    }

    private VerticalLayout guidanceSection(final List<GuidanceDto> guidances) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);

        final Span heading = new Span(this.getTranslation("search.grid.details.guidanceHeading"));
        heading.addClassNames(FontWeight.SEMIBOLD, TextColor.HEADER);
        layout.add(heading);

        guidances.forEach(guidance -> {
            layout.add(this.displaySection(guidance.getTitle(), guidance.getText()));
        });
        return layout;
    }

    private VerticalLayout notesSection(final List<UserNoteDto> notes) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);

        final Span heading = new Span(this.getTranslation("search.grid.details.notesHeading"));
        heading.addClassNames(FontWeight.SEMIBOLD, TextColor.HEADER);
        layout.add(heading);

        notes.forEach(note -> {
            layout.add(this.displaySection(note.getTitle(), note.getText()));
        });
        return layout;
    }

    private Component displaySection(final String title, final String text) {
        final Span titleContainer = new Span();
        titleContainer.getElement().setProperty("innerHTML", title);
        titleContainer.addClassNames(FontWeight.SEMIBOLD);

        final Span textContainer = new Span();
        textContainer.getElement().setProperty("innerHTML", text);

        final VerticalLayout layout = new VerticalLayout(titleContainer, textContainer);
        layout.addClassNames("ui-search-grid-details-item");
        return layout;
    }

    private String searchHighlighter(final String matchedText) {
        return "<span class='ui-search-match'>" + matchedText + "</span>";
    }

}
