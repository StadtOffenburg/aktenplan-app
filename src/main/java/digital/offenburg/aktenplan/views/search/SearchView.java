package digital.offenburg.aktenplan.views.search;

import java.util.List;
import java.util.Set;

import javax.annotation.security.PermitAll;

import org.apache.commons.lang3.ObjectUtils;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.GridSortOrderBuilder;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.HasDynamicTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;

import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.service.AktenPlanService;
import digital.offenburg.aktenplan.data.view.SearchOptions;
import digital.offenburg.aktenplan.data.view.SortBy;
import digital.offenburg.aktenplan.event.SortEvent;

@Route(value = "search", layout = MainViewLayout.class)
@PermitAll
public class SearchView extends VerticalLayout implements BeforeEnterObserver, HasDynamicTitle {
    private final static String DETAILS_COL_KEY = "_col_details_btn";

    private final transient AktenPlanService aktenPlanService;

    private Grid<FileReferenceDto> searchGrid;
    private List<GridSortOrder<FileReferenceDto>> ascendingSort;
    private List<GridSortOrder<FileReferenceDto>> descendingSort;

    private SearchOptions searchOptions;
    private SortBy sortBy;

    public SearchView(final AktenPlanService aktenPlanService) {
        this.aktenPlanService = aktenPlanService;
        this.setSizeFull();
    }

    private void init(final Set<FileReferenceDto> fileReferences) {
        if (ObjectUtils.isNotEmpty(fileReferences)) {
            this.searchDisplay(fileReferences);
        } else {
            this.add(new Span(this.getTranslation("search.view.noSearchResult")));
        }
    }

    private void searchDisplay(final Set<FileReferenceDto> fileReferences) {
        this.searchGrid = new Grid<>();
        this.searchGrid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        this.searchGrid.setItems(fileReferences);
        this.searchGrid.setSelectionMode(Grid.SelectionMode.NONE);
        this.searchGrid.setSizeFull();
        this.searchGrid.setDetailsVisibleOnClick(false);
        this.searchGrid.setItemDetailsRenderer(new ComponentRenderer<>(item -> new SearchRowExpandedDetails(this.aktenPlanService, item, this.searchOptions)));

        final Column<FileReferenceDto> fileNumberCol = this.searchGrid.addColumn(LitRenderer.<FileReferenceDto>of("""
                                                                                                                  <span class="${item.classNames}">${item.id}</span>
                                                                                                                  """)
                                                                                            .withProperty("id", FileReferenceDto::getId)
                                                                                            .withProperty("classNames", item -> item.isActive() ? "" : "ui-file-reference-inactive"))
                                                                      .setComparator(FileReferenceDto::getId)
                                                                      .setHeader(this.getTranslation("search.grid.header.fileNumber"))
                                                                      .setAutoWidth(true)
                                                                      .setFlexGrow(0);

        this.searchGrid.addColumn(LitRenderer.<FileReferenceDto>of("""
                                                                   <span class="ui-file-reference-label-in-grid ${item.inactiveClassName}">
                                                                       <span .innerHTML=${item.title}></span>
                                                                       ${item.inactive?
                                                                           html `<vaadin-icon icon="vaadin:info-circle" class="ui-item-inactive" title="${item.tooltip}"></vaadin-icon>`
                                                                           :html ``
                                                                       }
                                                                   </span>
                                                                   """)
                                             .withProperty("title", FileReferenceDto::getTitle)
                                             .withProperty("tooltip", item -> this.getTranslation("search.view.inactive.tooltip"))
                                             .withProperty("inactive", item -> !item.isActive())
                                             .withProperty("inactiveClassName", item -> item.isActive() ? "" : "ui-file-reference-inactive"))
                       .setComparator(FileReferenceDto::getTitle)
                       .setHeader(this.getTranslation("search.grid.header.fileTitle"));

        this.searchGrid.addColumn(LitRenderer.<FileReferenceDto>of("""
                                                                   ${item.detailsRequired?
                                                                       html `<vaadin-button theme="tertiary" @click="${detailsClicked}">
                                                                                <vaadin-icon icon="vaadin:${item.expandIcon}"></vaadin-icon>${item.expandLabel}
                                                                               </vaadin-button>`
                                                                       :html ``
                                                                   }
                                                                   """)
                                             .withProperty("expandLabel", item -> this.getTranslation("search.grid.expandDetails.label"))
                                             .withProperty("expandIcon", item -> this.searchGrid.isDetailsVisible(item) ? "angle-up" : "angle-down")
                                             .withProperty("detailsRequired", FileReferenceDto::isSearchMatchedInDetails)
                                             .withFunction("detailsClicked", item -> {
                                                 final boolean wasVisible = this.searchGrid.isDetailsVisible(item);
                                                 this.searchGrid.setDetailsVisible(item, !wasVisible);
                                             }))
                       .setKey(DETAILS_COL_KEY)
                       .setFlexGrow(0);

        this.ascendingSort = new GridSortOrderBuilder<FileReferenceDto>().thenAsc(fileNumberCol).build();
        this.descendingSort = new GridSortOrderBuilder<FileReferenceDto>().thenDesc(fileNumberCol).build();

        this.sortSearchGrid(this.sortBy);

        this.searchGrid.addItemClickListener(event -> {
            if (!DETAILS_COL_KEY.equalsIgnoreCase(event.getColumn().getKey())) {
                final FileReferenceDto item = event.getItem();
                UI.getCurrent().navigate(MainView.class, item.getId());
            }
        });

        final Span headerText = new Span(this.getTranslation("search.view.searchResultHeader"));
        headerText.addClassName("ui-section-heading");
        this.add(headerText);

        this.add(this.searchGrid);
    }

    private void sortSearchGrid(final SortBy sortBy) {
        switch (sortBy) {
            case ASCENDING -> {
                this.searchGrid.sort(this.ascendingSort);
            }
            case DESCENDING -> {
                this.searchGrid.sort(this.descendingSort);
            }
        }
    }

    @Override
    protected void onAttach(final AttachEvent attachEvent) {
        super.onAttach(attachEvent);

        ComponentUtil.addListener(attachEvent.getUI(), SortEvent.class, event -> this.sortSearchGrid(event.getSortBy()));
    }

    @Override
    public void beforeEnter(final BeforeEnterEvent event) {
        this.searchOptions = VaadinSession.getCurrent().getAttribute(SearchOptions.class);
        this.sortBy = VaadinSession.getCurrent().getAttribute(SortBy.class);
        if (this.searchOptions != null && this.searchOptions.hasData()) {
            final Set<FileReferenceDto> fileReferences = this.aktenPlanService.searchFileReferences(this.searchOptions, this::searchHighlighter);
            this.removeAll();
            this.init(fileReferences);
        } else {
            event.forwardTo(MainView.class);
        }
    }

    private String searchHighlighter(final String matchedText) {
        return "<span class='ui-search-match'>" + matchedText + "</span>";
    }

    @Override
    public String getPageTitle() {
        return this.getTranslation("page.title.search");
    }

}
