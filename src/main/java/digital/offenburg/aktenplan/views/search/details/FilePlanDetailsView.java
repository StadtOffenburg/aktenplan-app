package digital.offenburg.aktenplan.views.search.details;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.theme.lumo.LumoUtility.AlignItems;
import com.vaadin.flow.theme.lumo.LumoUtility.Background;
import com.vaadin.flow.theme.lumo.LumoUtility.Display;
import com.vaadin.flow.theme.lumo.LumoUtility.FontSize;
import com.vaadin.flow.theme.lumo.LumoUtility.FontWeight;
import com.vaadin.flow.theme.lumo.LumoUtility.Gap;
import com.vaadin.flow.theme.lumo.LumoUtility.JustifyContent;
import com.vaadin.flow.theme.lumo.LumoUtility.Margin;
import com.vaadin.flow.theme.lumo.LumoUtility.Padding;

import digital.offenburg.aktenplan.component.LinkButton;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.service.AktenPlanService;
import digital.offenburg.aktenplan.event.FavouriteUpdateEvent;
import digital.offenburg.aktenplan.event.FileReferenceDetailsClosedEvent;
import digital.offenburg.aktenplan.views.search.details.guidance.GuidanceTabView;
import digital.offenburg.aktenplan.views.search.details.units.UnitsTabView;
import digital.offenburg.aktenplan.views.search.details.usernotes.UserNotesTabView;

public class FilePlanDetailsView extends VerticalLayout {
    private final transient AktenPlanService aktenPlanService;

    private final FileReferenceDto fileReference;
    private final VerticalLayout tabContent;

    private static class TabId {
        private static final String METADATA = "tab-metadata";
        private static final String GUIDANCE = "tab-guidance";
        private static final String USER_NOTES = "tab-usernotes";
        private static final String UNITS = "tab-units";
    }

    public FilePlanDetailsView(final AktenPlanService aktenPlanService, final FileReferenceDto fileReference) {
        this.aktenPlanService = aktenPlanService;
        this.fileReference = fileReference;
        this.setSpacing(false);
        this.setSizeFull();
        this.tabContent = new VerticalLayout();
        this.tabContent.setSizeFull();
        this.setPadding(false);
        this.add(this.heading());
        aktenPlanService.currentUser().ifPresent(user -> this.add(this.addFavourite()));

        if (!fileReference.isActive()) {
            this.add(this.showInactiveNotice());
        }

        this.add(this.tabs(), this.tabContent);
    }

    private Component heading() {
        final HorizontalLayout layout = new HorizontalLayout();
        layout.setWidthFull();
        layout.addClassNames(AlignItems.CENTER, JustifyContent.BETWEEN, Padding.Horizontal.LARGE, Margin.Top.SMALL);

        final Span heading = new Span(this.fileReference.getTitle());
        heading.addClassNames(FontWeight.BOLD, FontSize.LARGE);

        final Button close = new Button(VaadinIcon.CLOSE.create());
        close.addThemeVariants(ButtonVariant.LUMO_TERTIARY, ButtonVariant.LUMO_CONTRAST);
        close.addClickListener(event -> {
            final FileReferenceDetailsClosedEvent linkEvent = new FileReferenceDetailsClosedEvent(this);
            ComponentUtil.fireEvent(UI.getCurrent(), linkEvent);
        });

        layout.add(heading, close);
        return layout;
    }

    private Component addFavourite() {
        final Icon notAddedIcon = VaadinIcon.STAR_O.create();
        notAddedIcon.addClassNames(FontSize.XXSMALL);

        final Icon addedIcon = VaadinIcon.STAR.create();
        addedIcon.addClassNames(FontSize.XXSMALL, this.fileReference.isActive() ? "ui-fav-star" : "ui-fav-star-inactive");
        
        final Span favourites = new Span();
        favourites.addClassNames(Display.INLINE_FLEX, Gap.MEDIUM, AlignItems.CENTER);
        favourites.add(this.fileReference.isUserFavourite() ? addedIcon : notAddedIcon);
        favourites.add(new Span(this.getTranslation("details.link.addToFavourites")));

        final Button addToFavourites = new LinkButton(favourites);
        addToFavourites.addClassNames(Padding.Horizontal.LARGE, Margin.Bottom.MEDIUM);

        addToFavourites.addClickListener(event -> {
            final boolean added = this.aktenPlanService.toggleFavourite(this.fileReference);
            this.fileReference.setUserFavourite(added);
            if (added) {
                favourites.addComponentAsFirst(addedIcon);
                favourites.remove(notAddedIcon);
            } else {
                favourites.addComponentAsFirst(notAddedIcon);
                favourites.remove(addedIcon);
            }
            this.fireEvent(new FavouriteUpdateEvent(this, this.fileReference));
        });
        return addToFavourites;
    }
    
    private Component showInactiveNotice() {
        final Span span = new Span(this.getTranslation("details.notice.inactive"));
        final Icon infoIcon = VaadinIcon.INFO_CIRCLE.create();
        infoIcon.addClassNames(FontSize.XXSMALL);

        final HorizontalLayout noticeContainer = new HorizontalLayout(infoIcon, span);
        noticeContainer.setWidthFull();
        noticeContainer.addClassNames("ui-border-shadow", Background.CONTRAST_5, Padding.MEDIUM, AlignItems.CENTER);

        final VerticalLayout layout = new VerticalLayout(noticeContainer);
        layout.addClassNames(Padding.Horizontal.LARGE, Margin.Bottom.MEDIUM);

        return layout;
    }

    private Component tabs() {
        final Tab metadataTab = new Tab(this.getTranslation("tab.label.metadata"));
        metadataTab.setId(TabId.METADATA);

        final Tab guidanceTab = new Tab(this.getTranslation("tab.label.guidance"));
        guidanceTab.setId(TabId.GUIDANCE);

        final Tab notesTab = new Tab(this.getTranslation("tab.label.usernotes"));
        notesTab.setId(TabId.USER_NOTES);

        final Tab unitsTab = new Tab(this.getTranslation("tab.label.units"));
        unitsTab.setId(TabId.UNITS);

        final Tabs tabs = new Tabs(metadataTab, guidanceTab);
        if (this.aktenPlanService.isUser()) {
            tabs.add(notesTab);
        }
        tabs.add(unitsTab);

        tabs.setWidthFull();
        tabs.addThemeVariants(TabsVariant.LUMO_HIDE_SCROLL_BUTTONS);
        tabs.addClassName("ui-horizontal-separator");
        tabs.addSelectedChangeListener(event -> this.setTabContent(event.getSelectedTab()));

        this.setTabContent(metadataTab);
        return tabs;
    }

    private void setTabContent(final Tab tab) {
        final Component tabBodyComponent = switch (tab.getId().orElse("")) {
            case TabId.METADATA -> new MetadataTabView(this.fileReference);
            case TabId.GUIDANCE -> new GuidanceTabView(this.aktenPlanService, this.fileReference);
            case TabId.USER_NOTES -> new UserNotesTabView(this.aktenPlanService, this.fileReference);
            case TabId.UNITS -> new UnitsTabView(this.aktenPlanService, this.fileReference);
            default -> new Span("Invalid tab!");
        };
        this.tabContent.removeAll();
        this.tabContent.add(tabBodyComponent);
    }

    public Registration addFavouriteUpdateEventListener(final ComponentEventListener<FavouriteUpdateEvent> listener) {
        return super.addListener(FavouriteUpdateEvent.class, listener);
    }

}
