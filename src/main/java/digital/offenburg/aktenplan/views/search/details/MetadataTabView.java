package digital.offenburg.aktenplan.views.search.details;

import java.util.Comparator;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.theme.lumo.LumoUtility;

import digital.offenburg.aktenplan.component.LinkButton;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.dto.fileplan.MoreInformationDto;
import digital.offenburg.aktenplan.data.dto.fileplan.ReferenceLinksDto;
import digital.offenburg.aktenplan.views.search.MainView;

@JsModule("./copyToClipboad.js")
public class MetadataTabView extends VerticalLayout {
    private final FileReferenceDto fileReference;

    public MetadataTabView(final FileReferenceDto fileReference) {
        this.fileReference = fileReference;
        this.setSizeFull();
        this.add(this.description(), this.moreInformation(), this.links());
    }

    private Component description() {
        final VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);
        layout.setSpacing(false);

        if (ObjectUtils.isNotEmpty(this.fileReference.getDescriptions())) {
            layout.add(this.sectionHeader("tab.metadata.descriptionHeading"));

            this.fileReference.getDescriptions()
                              .stream()
                              .map(fileRefDesc -> new Paragraph(fileRefDesc.getDescription()))
                              .forEach(layout::add);

        } else {
            layout.setVisible(false);
        }
        return layout;
    }

    private Component moreInformation() {
        final VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);
        layout.addClassNames(LumoUtility.Padding.Top.MEDIUM);

        if (ObjectUtils.isNotEmpty(this.fileReference.getInformations())) {
            layout.add(this.sectionHeader("tab.metadata.moreInfoHeading"));

            final FlexLayout infoContainer = new FlexLayout();
            infoContainer.addClassNames(LumoUtility.FlexDirection.COLUMN, LumoUtility.Gap.SMALL, LumoUtility.Width.FULL);
            infoContainer.getStyle().set("overflow-y", "auto");
            infoContainer.getStyle().set("max-height", "400px");

            this.fileReference.getInformations()
                              .stream()
                              .sorted(Comparator.comparing(MoreInformationDto::getText))
                              .map(fileInfo -> {
                                  final Span infoSpan = new Span(fileInfo.getText());

                                  final Button copyBtn = new LinkButton(VaadinIcon.COPY_O.create());
                                  copyBtn.addThemeVariants(ButtonVariant.LUMO_ICON);
                                  copyBtn.addClassNames(LumoUtility.Margin.NONE);
                                  copyBtn.getElement().setAttribute("aria-label", this.getTranslation("tab.metadata.info.aria.copyToClipboard"));
                                  copyBtn.addClickListener(event -> UI.getCurrent().getPage().executeJs("window.copyToClipboard($0)", fileInfo.getText()));

                                  return this.itemContainer(infoSpan, copyBtn);
                              })
                              .forEach(infoContainer::add);
            layout.add(infoContainer);
        } else {
            layout.setVisible(false);
        }
        return layout;
    }

    private Component links() {
        final VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);
        layout.addClassNames(LumoUtility.Padding.Top.MEDIUM);

        if (ObjectUtils.isNotEmpty(this.fileReference.getLinks())) {
            layout.add(this.sectionHeader("tab.metadata.linksHeading"));

            final FlexLayout linkContainer = new FlexLayout();
            linkContainer.addClassNames(LumoUtility.FlexDirection.COLUMN, LumoUtility.Gap.SMALL, LumoUtility.Width.FULL);
            linkContainer.getStyle().set("overflow-y", "auto");
            linkContainer.getStyle().set("max-height", "400px");

            this.fileReference.getLinks()
                              .stream()
                              .sorted(Comparator.comparing(ReferenceLinksDto::getTarget))
                              .map(link -> {
                                  final Span linkContent = new Span();
                                  final Icon linkIcon = VaadinIcon.LINK.create();
                                  linkIcon.addClassNames("ui-text-8", LumoUtility.Margin.Right.SMALL);
                                  final Span linkTarget = new Span(link.getTarget());
                                  linkTarget.addClassNames(LumoUtility.FontWeight.BOLD);
                                  final Span linkLabel = new Span(String.format(LocaleContextHolder.getLocale(), " - %s", link.getLabel()));
                                  linkContent.add(linkIcon, linkTarget, linkLabel);

                                  final HorizontalLayout linkItem = this.itemContainer(linkContent, VaadinIcon.ANGLE_RIGHT.create());

                                  final RouterLink routerLink = new RouterLink("", MainView.class, link.getTarget());
                                  routerLink.add(linkItem);
                                  routerLink.getElement().setAttribute("aria-label", this.getTranslation("tab.metadata.info.aria.linkNavigation") + link.getTarget());
                                  routerLink.addClassNames(LumoUtility.TextColor.BODY);

                                  return routerLink;
                              })
                              .forEach(linkContainer::add);
            layout.add(linkContainer);
        } else {
            layout.setVisible(false);
        }
        return layout;
    }

    private Span sectionHeader(final String headingTranslationKey) {
        final Span heading = new Span(this.getTranslation(headingTranslationKey));
        heading.addClassNames(LumoUtility.FontWeight.SEMIBOLD, LumoUtility.TextColor.HEADER);
        return heading;
    }

    private HorizontalLayout itemContainer(final Component... components) {
        final HorizontalLayout infoContainer = new HorizontalLayout();
        infoContainer.setWidthFull();
        infoContainer.addClassName("ui-metadata-item");
        infoContainer.add(components);
        return infoContainer;
    }

}
