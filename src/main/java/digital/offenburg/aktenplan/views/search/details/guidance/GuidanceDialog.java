package digital.offenburg.aktenplan.views.search.details.guidance;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;

import digital.offenburg.aktenplan.component.FormDialog;
import digital.offenburg.aktenplan.data.dto.GuidanceDto;
import digital.offenburg.aktenplan.event.SaveFormDialogEvent;

public final class GuidanceDialog extends FormDialog<GuidanceDto> {

    public static class Builder {
        private TextField title;
        private TextArea text;
        private GuidanceDialog dialog;

        public Builder forNew() {
            this.dialog = new GuidanceDialog(new GuidanceDto());
            this.dialog.prepare(this.formDialogI18n("tab.guidance.dialog.newHeading"), this.formComponent());
            return this;
        }

        public Builder forEdit(final GuidanceDto guidanceDto) {
            this.dialog = new GuidanceDialog(guidanceDto);
            this.dialog.prepare(this.formDialogI18n("tab.guidance.dialog.editHeading"), this.formComponent());
            return this;
        }

        public Builder withSaveEventListener(final ComponentEventListener<SaveFormDialogEvent<GuidanceDto>> listener) {
            this.dialog.addSaveItemEventListener(listener);
            return this;
        }

        public GuidanceDialog build() {
            return this.dialog;
        }

        private FormDialogI18n formDialogI18n(final String headerKey) {
            final FormDialogI18n formDialogI18n = new FormDialogI18n();
            formDialogI18n.setHeaderTitle(this.dialog.getTranslation(headerKey));
            formDialogI18n.setSaveButtonLabel(this.dialog.getTranslation("tab.guidance.dialog.saveButtonLabel"));
            formDialogI18n.setCancelButtonLabel(this.dialog.getTranslation("tab.guidance.dialog.cancelButtonLabel"));
            return formDialogI18n;
        }

        private Component formComponent() {
            this.title = new TextField(this.dialog.getTranslation("tab.guidance.dialog.titleLabel"));
            this.text = new TextArea(this.dialog.getTranslation("tab.guidance.dialog.textLabel"));
            this.text.setSizeFull();

            final VerticalLayout dialogLayout = new VerticalLayout(this.title, this.text);
            dialogLayout.setPadding(false);
            dialogLayout.setSpacing(false);
            dialogLayout.setSizeFull();
            dialogLayout.setAlignItems(FlexComponent.Alignment.STRETCH);

            this.dialog.getBinder().bindInstanceFields(this);
            return dialogLayout;
        }

    }

    private GuidanceDialog(final GuidanceDto guidanceDto) {
        super(GuidanceDto.class, guidanceDto);
        this.addClassNames("ui-tall-dialog");
    }
}
