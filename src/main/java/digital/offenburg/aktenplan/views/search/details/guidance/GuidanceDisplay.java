package digital.offenburg.aktenplan.views.search.details.guidance;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.theme.lumo.LumoUtility;

import digital.offenburg.aktenplan.component.LargeTextDisplay;
import digital.offenburg.aktenplan.component.LinkButton;
import digital.offenburg.aktenplan.config.SuppressForbiddenApi;
import digital.offenburg.aktenplan.data.dto.GuidanceDto;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.service.AktenPlanService;
import digital.offenburg.aktenplan.views.search.details.guidance.events.CopyGuidanceEvent;
import digital.offenburg.aktenplan.views.search.details.guidance.events.DeleteGuidanceEvent;
import digital.offenburg.aktenplan.views.search.details.guidance.events.EditGuidanceEvent;

public class GuidanceDisplay extends VerticalLayout {
    private static final int DEFAULT_TEXT_LENGTH_TO_DISPLAY = 200;

    private final transient AktenPlanService aktenPlanService;

    private final GuidanceDto guidance;
    private final FileReferenceDto fileReference;

    public GuidanceDisplay(final AktenPlanService aktenPlanService, final FileReferenceDto fileReference, final GuidanceDto guidance) {
        this.aktenPlanService = aktenPlanService;
        this.fileReference = fileReference;
        this.guidance = guidance;

        this.setSpacing(false);
        this.setPadding(false);
        this.add(this.topRow(), this.title(), this.text());
    }

    @SuppressForbiddenApi
    private Component topRow() {
        final String dateDisplay = this.guidance.getTimestamp().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMANY));
        final String versionText = String.format(LocaleContextHolder.getLocale(), "%s %s %s", "Version", this.guidance.getPlanVersion(), dateDisplay);
        final Span version = new Span(versionText);
        version.addClassNames(LumoUtility.FontSize.SMALL, LumoUtility.TextColor.SECONDARY);

        final HorizontalLayout topRow = new HorizontalLayout();
        topRow.setWidthFull();
        topRow.add(version);

        if (this.aktenPlanService.canProvideGuidance(this.fileReference)) {
            final HorizontalLayout buttons = this.createButtons();
            topRow.add(buttons);
            topRow.setJustifyContentMode(JustifyContentMode.BETWEEN);
        }

        topRow.setAlignItems(Alignment.CENTER);
        return topRow;
    }

    private HorizontalLayout createButtons() {
        final Button edit = new LinkButton(VaadinIcon.EDIT.create());
        edit.addClickListener(event -> this.fireEvent(new EditGuidanceEvent(this, this.guidance)));

        final Button delete = new LinkButton(VaadinIcon.TRASH.create());
        delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
        delete.addClickListener(event -> this.fireEvent(new DeleteGuidanceEvent(this, this.guidance)));

        final Button copy = new LinkButton(VaadinIcon.COPY.create());
        copy.addClickListener(event -> this.fireEvent(new CopyGuidanceEvent(this, this.guidance)));

        final HorizontalLayout buttons = new HorizontalLayout();
        if (this.fileReference.getVersion().equalsIgnoreCase(this.guidance.getPlanVersion())) {
            buttons.add(edit, delete);
        } else {
            buttons.add(copy, delete);
        }
        return buttons;
    }

    private Component title() {
        final H5 title = new H5(this.guidance.getTitle());
        title.addClassNames(LumoUtility.Margin.NONE);
        title.setWidthFull();
        return title;
    }

    private Component text() {
        return new LargeTextDisplay(this.guidance.getText(), DEFAULT_TEXT_LENGTH_TO_DISPLAY, this.getTranslation("text.more.label"),
                                    this.getTranslation("text.less.label"));
    }

    public GuidanceDisplay withEditListener(final ComponentEventListener<EditGuidanceEvent> editListener) {
        this.addListener(EditGuidanceEvent.class, editListener);
        return this;
    }

    public GuidanceDisplay withDeleteListener(final ComponentEventListener<DeleteGuidanceEvent> deleteListener) {
        this.addListener(DeleteGuidanceEvent.class, deleteListener);
        return this;
    }

    public GuidanceDisplay withCopyListener(final ComponentEventListener<CopyGuidanceEvent> copyListener) {
        this.addListener(CopyGuidanceEvent.class, copyListener);
        return this;
    }

}
