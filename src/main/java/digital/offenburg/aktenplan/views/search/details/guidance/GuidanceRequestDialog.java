package digital.offenburg.aktenplan.views.search.details.guidance;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;

import digital.offenburg.aktenplan.component.FormDialog;
import digital.offenburg.aktenplan.data.dto.GuidanceRequestDto;
import digital.offenburg.aktenplan.event.SaveFormDialogEvent;

public final class GuidanceRequestDialog extends FormDialog<GuidanceRequestDto> {

    public static class Builder {
        private TextArea text;
        private GuidanceRequestDialog dialog;

        public Builder forNew() {
            this.dialog = new GuidanceRequestDialog(new GuidanceRequestDto());
            this.dialog.prepare(this.formDialogI18n("tab.guidance.request.dialog.heading"), this.formComponent());
            return this;
        }

        public Builder withSaveEventListener(final ComponentEventListener<SaveFormDialogEvent<GuidanceRequestDto>> listener) {
            this.dialog.addSaveItemEventListener(listener);
            return this;
        }

        public GuidanceRequestDialog build() {
            return this.dialog;
        }

        private FormDialogI18n formDialogI18n(final String headerKey) {
            final FormDialogI18n formDialogI18n = new FormDialogI18n();
            formDialogI18n.setHeaderTitle(this.dialog.getTranslation(headerKey));
            formDialogI18n.setSaveButtonLabel(this.dialog.getTranslation("tab.guidance.request.dialog.saveButtonLabel"));
            formDialogI18n.setCancelButtonLabel(this.dialog.getTranslation("tab.guidance.request.dialog.cancelButtonLabel"));
            return formDialogI18n;
        }

        private Component formComponent() {
            this.text = new TextArea(this.dialog.getTranslation("tab.guidance.request.dialog.textLabel"));
            this.text.setSizeFull();

            final VerticalLayout dialogLayout = new VerticalLayout(this.text);
            dialogLayout.setPadding(false);
            dialogLayout.setSpacing(false);
            dialogLayout.setSizeFull();
            dialogLayout.setAlignItems(FlexComponent.Alignment.STRETCH);

            this.dialog.getBinder().bindInstanceFields(this);
            return dialogLayout;
        }

    }

    private GuidanceRequestDialog(final GuidanceRequestDto guidanceRequestDto) {
        super(GuidanceRequestDto.class, guidanceRequestDto);
        this.addClassNames("ui-tall-dialog");
    }
}
