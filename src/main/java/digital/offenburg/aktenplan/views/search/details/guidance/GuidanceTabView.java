package digital.offenburg.aktenplan.views.search.details.guidance;

import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.theme.lumo.LumoUtility.AlignItems;
import com.vaadin.flow.theme.lumo.LumoUtility.Display;
import com.vaadin.flow.theme.lumo.LumoUtility.Gap;
import com.vaadin.flow.theme.lumo.LumoUtility.Padding;

import digital.offenburg.aktenplan.component.Alert;
import digital.offenburg.aktenplan.component.ConfirmDialog;
import digital.offenburg.aktenplan.component.ConfirmDialog.ConfirmDialogI18n;
import digital.offenburg.aktenplan.component.LinkButton;
import digital.offenburg.aktenplan.data.dto.GuidanceDto;
import digital.offenburg.aktenplan.data.dto.GuidanceRequestDto;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.service.AktenPlanService;
import digital.offenburg.aktenplan.event.SaveFormDialogEvent;
import digital.offenburg.aktenplan.exceptions.OsTicketException;
import digital.offenburg.aktenplan.views.search.details.guidance.events.CopyGuidanceEvent;
import digital.offenburg.aktenplan.views.search.details.guidance.events.DeleteGuidanceEvent;
import digital.offenburg.aktenplan.views.search.details.guidance.events.EditGuidanceEvent;

public class GuidanceTabView extends VerticalLayout {
    private static final Logger LOGGER = LogManager.getLogger(GuidanceTabView.class);
    
    private final transient AktenPlanService aktenPlanService;
    
    private final FileReferenceDto fileReference;
    private final VerticalLayout guidanceContainer;
    
    public GuidanceTabView(final AktenPlanService aktenPlanService, final FileReferenceDto fileReference) {
        this.aktenPlanService = aktenPlanService;
        this.fileReference = fileReference;
        this.guidanceContainer = new VerticalLayout();
        this.guidanceContainer.setPadding(false);
        
        this.add(this.buttonSection(), this.guidanceContainer);
        this.showGuidances();
    }
    
    private Component buttonSection() {
        final HorizontalLayout layout = new HorizontalLayout();
        layout.addClassNames(Gap.XLARGE);
        if (this.aktenPlanService.canProvideGuidance(this.fileReference)) {
            layout.add(this.provideGuidance());
        }
        
        if (this.aktenPlanService.canRequestGuidance(this.fileReference)) {
            layout.add(this.requestGuidance());
        }
        return layout;
    }
    
    private Component provideGuidance() {
        final Span buttonContent = new Span();
        buttonContent.addClassNames(Display.INLINE_FLEX, Gap.SMALL, AlignItems.CENTER);
        buttonContent.add(VaadinIcon.INFO_CIRCLE_O.create());
        buttonContent.add(new Span(this.getTranslation("tab.guidance.provideGuidance")));
        
        final Button addBtn = new LinkButton(buttonContent);
        addBtn.addClassNames(Padding.NONE);
        addBtn.addClickListener(event -> {
            new GuidanceDialog.Builder().forNew()
                                        .withSaveEventListener(this::saveGuidance)
                                        .build()
                                        .open();
        });
        return addBtn;
    }
    
    private Component requestGuidance() {
        final Span buttonContent = new Span();
        buttonContent.addClassNames(Display.INLINE_FLEX, Gap.SMALL, AlignItems.CENTER);
        buttonContent.add(VaadinIcon.INFO_CIRCLE_O.create());
        buttonContent.add(new Span(this.getTranslation("tab.guidance.requestGuidance")));
        
        final Button addBtn = new LinkButton(buttonContent);
        addBtn.addClassNames(Padding.NONE);
        addBtn.addClickListener(event -> {
            new GuidanceRequestDialog.Builder().forNew()
                                               .withSaveEventListener(this::requestGuidance)
                                               .build()
                                               .open();
        });
        return addBtn;
    }
    
    private void saveGuidance(final SaveFormDialogEvent<GuidanceDto> event) {
        this.aktenPlanService.saveGuidance(this.fileReference, event.getItem());
        this.showGuidances();
        event.getSource().close();
    }
    
    private void requestGuidance(final SaveFormDialogEvent<GuidanceRequestDto> event) {
        try {
            final String requestTitle = getTranslation("tab.guidance.request.title", this.fileReference.getId());
            this.aktenPlanService.requestGuidance(this.fileReference, event.getItem(), requestTitle);
            event.getSource().close();
            Alert.success(this.getTranslation("tab.guidance.request.raised"));
        } catch (final OsTicketException ex) {
            LOGGER.fatal(ex.getMessage(), ex);
            Alert.error(this.getTranslation("error.message.fatal"));
        }
    }
    
    private void showGuidances() {
        this.guidanceContainer.removeAll();
        final List<GuidanceDto> guidances = this.aktenPlanService.fetchGuidances(this.fileReference);
        
        final ComponentEventListener<EditGuidanceEvent> editGuidanceListener = event -> {
            new GuidanceDialog.Builder().forEdit(event.getGuidance())
                                        .withSaveEventListener(this::saveGuidance)
                                        .build()
                                        .open();
        };
        
        final ComponentEventListener<DeleteGuidanceEvent> deleteGuidanceListener = event -> {
            final ConfirmDialogI18n confirmDialogI18n = new ConfirmDialogI18n();
            confirmDialogI18n.setTitle(this.getTranslation("tab.guidance.delete.dialog.title"));
            confirmDialogI18n.setMessage(this.getTranslation("tab.guidance.delete.dialog.message"));
            confirmDialogI18n.setCancelButtonLabel(this.getTranslation("tab.guidance.delete.dialog.cancelButtonLabel"));
            confirmDialogI18n.setConfirmButtonLabel(this.getTranslation("tab.guidance.delete.dialog.confirmButtonLabel"));
            final ConfirmDialog deleteConfirmDialog = new ConfirmDialog(confirmDialogI18n);
            deleteConfirmDialog.addConfirmListener(delEvent -> {
                this.aktenPlanService.deleteGuidance(this.fileReference, event.getGuidance());
                deleteConfirmDialog.close();
                this.showGuidances();
            });
            deleteConfirmDialog.open();
        };
        
        final ComponentEventListener<CopyGuidanceEvent> copyGuidanceListener = event -> {
            this.aktenPlanService.copyGuidance(this.fileReference, event.getGuidance());
            this.showGuidances();
        };
        
        final Iterator<GuidanceDisplay> componentItr = guidances.stream()
                                                                .map(item -> new GuidanceDisplay(this.aktenPlanService, this.fileReference, item).withEditListener(editGuidanceListener)
                                                                                                                                                 .withDeleteListener(deleteGuidanceListener)
                                                                                                                                                 .withCopyListener(copyGuidanceListener))
                                                                .iterator();
        
        if (componentItr.hasNext()) {
            this.guidanceContainer.add(componentItr.next());
        }
        
        this.showGuidanceHistory(componentItr);
    }
    
    private void showGuidanceHistory(final Iterator<GuidanceDisplay> componentItr) {
        if (componentItr.hasNext()) {
            this.guidanceContainer.add(new Hr());
            
            final H4 guidanceHistoryHeading = new H4(this.getTranslation("tab.guidance.history.heading"));
            this.guidanceContainer.add(guidanceHistoryHeading);
            
            final VerticalLayout historyGuidanceContainer = new VerticalLayout();
            historyGuidanceContainer.setPadding(false);
            historyGuidanceContainer.setSpacing(false);
            historyGuidanceContainer.addClassName("ui-item-history-container");
            
            componentItr.forEachRemaining(item -> {
                item.addClassName("ui-item-history");
                historyGuidanceContainer.add(item);
            });
            this.guidanceContainer.add(historyGuidanceContainer);
        }
    }
    
}
