package digital.offenburg.aktenplan.views.search.details.guidance.events;

import com.vaadin.flow.component.ComponentEvent;

import digital.offenburg.aktenplan.data.dto.GuidanceDto;
import digital.offenburg.aktenplan.views.search.details.guidance.GuidanceDisplay;

public class EditGuidanceEvent extends ComponentEvent<GuidanceDisplay> {

    private final GuidanceDto guidance;

    public EditGuidanceEvent(final GuidanceDisplay source, final GuidanceDto guidance) {
        super(source, false);
        this.guidance = guidance;
    }

    public GuidanceDto getGuidance() {
        return this.guidance;
    }

}
