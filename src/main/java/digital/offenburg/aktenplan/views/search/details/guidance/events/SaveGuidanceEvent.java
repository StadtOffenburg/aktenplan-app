package digital.offenburg.aktenplan.views.search.details.guidance.events;

import com.vaadin.flow.component.ComponentEvent;

import digital.offenburg.aktenplan.data.dto.GuidanceDto;
import digital.offenburg.aktenplan.views.search.details.guidance.GuidanceDialog;

public class SaveGuidanceEvent extends ComponentEvent<GuidanceDialog> {

    private final GuidanceDto guidance;

    public SaveGuidanceEvent(final GuidanceDialog source, final GuidanceDto guidance) {
        super(source, false);
        this.guidance = guidance;
    }

    public GuidanceDto getGuidance() {
        return this.guidance;
    }

}
