package digital.offenburg.aktenplan.views.search.details.units;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.theme.lumo.LumoUtility.AlignItems;
import com.vaadin.flow.theme.lumo.LumoUtility.Display;
import com.vaadin.flow.theme.lumo.LumoUtility.Gap;
import com.vaadin.flow.theme.lumo.LumoUtility.Padding;

import digital.offenburg.aktenplan.component.LinkButton;
import digital.offenburg.aktenplan.data.dto.AdministrativeUnitDto;
import digital.offenburg.aktenplan.data.dto.FileReferenceUnitDto;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.service.AktenPlanService;

public class FileReferenceUnitsDialog extends Dialog {

    private final transient AktenPlanService aktenPlanService;
    private final FileReferenceDto fileReferenceDto;

    private final List<AdministrativeUnitDto> allUnits;
    private final List<Select<AdministrativeUnitDto>> selectList;

    private Button addAnotherBtn;

    private Checkbox tagAllUnitsCheckbox;
    private final boolean taggedToAllUnits;

    public FileReferenceUnitsDialog(final AktenPlanService aktenPlanService, final FileReferenceDto fileReferenceDto, final boolean taggedToAllUnits) {
        this.aktenPlanService = aktenPlanService;
        this.fileReferenceDto = fileReferenceDto;
        this.taggedToAllUnits = taggedToAllUnits;

        this.selectList = new ArrayList<>();
        this.allUnits = this.aktenPlanService.fetchAllUnits();
        this.setup();
    }

    private void setup() {
        final Button cancelBtn = new Button(this.getTranslation("tab.units.dialog.cancelButtonLabel"), event -> this.close());
        cancelBtn.addClassName("ui-secondary-button");

        final Button saveBtn = new Button(this.getTranslation("tab.units.dialog.saveButtonLabel"), event -> {
            this.save();
        });
        saveBtn.addClickShortcut(Key.ENTER);
        saveBtn.addClassName("ui-primary-button");

        this.setHeaderTitle(this.getTranslation("tab.units.dialog.header"));
        this.getFooter().add(cancelBtn, saveBtn);
        this.add(this.formComponent());
        this.addClassNames("ui-dialog");
    }

    private VerticalLayout formComponent() {
        final VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);

        final VerticalLayout manualTaggingContainer = new VerticalLayout();
        manualTaggingContainer.setPadding(false);

        this.tagAllUnitsCheckbox = new Checkbox(this.getTranslation("tab.units.dialog.tagAllUnitsCheckbox"), event -> {
            if (event.getValue()) {
                manualTaggingContainer.removeAll();
            } else {
                manualTaggingContainer.add(this.manuallyTaggingComponent());
            }
        });
        this.tagAllUnitsCheckbox.setValue(this.taggedToAllUnits);
        if (!this.taggedToAllUnits) {
            manualTaggingContainer.add(this.manuallyTaggingComponent());
        }
        layout.add(this.tagAllUnitsCheckbox, manualTaggingContainer);
        return layout;
    }

    private VerticalLayout manuallyTaggingComponent() {
        final VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);

        final VerticalLayout unitContainer = new VerticalLayout();
        unitContainer.setPadding(false);

        this.addAnotherBtn = this.addAnotherButton(unitContainer);

        layout.add(unitContainer, this.addAnotherBtn);

        final List<FileReferenceUnitDto> taggedUnits = this.aktenPlanService.fetchAllUnitsByFileReference(this.fileReferenceDto);
        this.prepareUnitContainer(unitContainer, taggedUnits);
        return layout;
    }

    private void prepareUnitContainer(final VerticalLayout layout, final List<FileReferenceUnitDto> taggedUnits) {
        if (ObjectUtils.isEmpty(taggedUnits)) {
            layout.add(this.createSelectUnitSection(null, layout));
        } else {
            final Component[] taggedUnitComponents = taggedUnits.stream()
                                                                .map(unit -> this.createSelectUnitSection(unit.getUnit(), layout))
                                                                .toArray(Component[]::new);
            layout.add(taggedUnitComponents);
        }
    }

    private Button addAnotherButton(final VerticalLayout layout) {
        final Span buttonContent = new Span();
        buttonContent.addClassNames(Display.INLINE_FLEX, Gap.MEDIUM, AlignItems.CENTER);
        buttonContent.add(VaadinIcon.PLUS.create());
        buttonContent.add(new Span(this.getTranslation("tab.units.dialog.addAnotherBtnLabel")));

        final Button addBtn = new LinkButton(buttonContent);
        addBtn.addClassNames(Padding.NONE);
        addBtn.addClickListener(event -> {
            layout.add(this.createSelectUnitSection(null, layout));
        });
        return addBtn;
    }

    private Component createSelectUnitSection(final AdministrativeUnitDto value, final VerticalLayout unitContainer) {
        final Select<AdministrativeUnitDto> unitSelect = new Select<>();
        unitSelect.setPlaceholder(this.getTranslation("tab.units.dialog.unitSelectLabel"));
        unitSelect.setItems(this.allUnits);
        unitSelect.setItemLabelGenerator(AdministrativeUnitDto::getName);
        unitSelect.setValue(value);
        unitSelect.setWidthFull();

        this.selectList.add(unitSelect);

        final HorizontalLayout layout = new HorizontalLayout();
        layout.setWidthFull();
        layout.add(unitSelect);

        final Button deleteBtn = new LinkButton(VaadinIcon.TRASH.create());
        deleteBtn.addThemeVariants(ButtonVariant.LUMO_ERROR);
        deleteBtn.addClickListener(event -> {
            unitContainer.remove(layout);
            this.selectList.remove(unitSelect);
            this.checkButtons();
        });

        this.checkButtons();
        layout.add(deleteBtn);
        return layout;
    }

    private void checkButtons() {
        final boolean allUnitsNotAdded = this.allUnits.size() > this.selectList.size();
        this.addAnotherBtn.setEnabled(allUnitsNotAdded);
    }

    private void save() {
        if (this.tagAllUnitsCheckbox.getValue()) {
            this.aktenPlanService.tagFileReferenceToAllUnits(this.fileReferenceDto);
        } else {
            final List<AdministrativeUnitDto> selectedUnits = this.selectList.stream()
                                                                             .map(Select::getValue)
                                                                             .filter(ObjectUtils::isNotEmpty)
                                                                             .distinct()
                                                                             .toList();
            this.aktenPlanService.saveFileReferenceUnits(this.fileReferenceDto, selectedUnits);
        }
        this.close();
    }

}
