package digital.offenburg.aktenplan.views.search.details.units;

import java.util.List;

import org.apache.commons.lang3.ObjectUtils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.theme.lumo.LumoUtility;
import com.vaadin.flow.theme.lumo.LumoUtility.AlignItems;
import com.vaadin.flow.theme.lumo.LumoUtility.Display;
import com.vaadin.flow.theme.lumo.LumoUtility.Gap;
import com.vaadin.flow.theme.lumo.LumoUtility.Padding;

import digital.offenburg.aktenplan.component.LinkButton;
import digital.offenburg.aktenplan.data.dto.FileReferenceUnitDto;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.service.AktenPlanService;

public class UnitsTabView extends VerticalLayout {
    private final transient AktenPlanService aktenPlanService;
    private final FileReferenceDto fileReferenceDto;
    private final VerticalLayout unitDisplayContainer;

    private boolean taggedToAllUnits;

    public UnitsTabView(final AktenPlanService aktenPlanService, final FileReferenceDto fileReferenceDto) {
        this.aktenPlanService = aktenPlanService;
        this.fileReferenceDto = fileReferenceDto;

        this.taggedToAllUnits = this.aktenPlanService.isTaggedToAllUnits(this.fileReferenceDto);

        if (this.aktenPlanService.canAddUnitsToFileReference(fileReferenceDto)) {
            this.add(this.manageUnits());
        }

        this.unitDisplayContainer = new VerticalLayout();
        this.unitDisplayContainer.setPadding(false);
        this.add(this.unitDisplayContainer);
        this.showTaggedUnits();
    }

    private Component manageUnits() {
        final Span buttonContent = new Span();
        buttonContent.addClassNames(Display.INLINE_FLEX, Gap.MEDIUM, AlignItems.CENTER);
        buttonContent.add(VaadinIcon.COG.create());
        buttonContent.add(new Span(this.getTranslation("tab.units.manageunits")));

        final Button addBtn = new LinkButton(buttonContent);
        addBtn.addClassNames(Padding.NONE);
        addBtn.addClickListener(event -> {
            final FileReferenceUnitsDialog dialog = new FileReferenceUnitsDialog(this.aktenPlanService, this.fileReferenceDto, this.taggedToAllUnits);
            dialog.addOpenedChangeListener(dialogEvent -> {
                if (!dialogEvent.isOpened()) {
                    this.taggedToAllUnits = this.aktenPlanService.isTaggedToAllUnits(this.fileReferenceDto);
                    this.showTaggedUnits();
                }
            });
            dialog.open();
        });
        return addBtn;
    }

    private void showTaggedUnits() {
        this.unitDisplayContainer.removeAll();

        if (this.taggedToAllUnits) {
            final Span heading = new Span(this.getTranslation("tab.units.taggedToAllUnits.heading"));
            heading.addClassNames(LumoUtility.FontWeight.SEMIBOLD, LumoUtility.TextColor.HEADER);
            this.unitDisplayContainer.add(heading);
        } else {
            final List<FileReferenceUnitDto> taggedUnits = this.aktenPlanService.fetchAllUnitsByFileReference(this.fileReferenceDto);
            if (ObjectUtils.isNotEmpty(taggedUnits)) {
                final Span heading = new Span(this.getTranslation("tab.units.taggedUnits.heading"));
                heading.addClassNames(LumoUtility.FontWeight.SEMIBOLD, LumoUtility.TextColor.HEADER);

                this.unitDisplayContainer.add(heading);

                taggedUnits.stream()
                           .map(unit -> {
                               final Span infoSpan = new Span(unit.getUnit().getName());
                               infoSpan.addClassName("ui-metadata-item");
                               infoSpan.setWidthFull();
                               return infoSpan;
                           })
                           .forEach(this.unitDisplayContainer::add);
            }
        }
    }
}
