package digital.offenburg.aktenplan.views.search.details.usernotes;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.theme.lumo.LumoUtility;

import digital.offenburg.aktenplan.component.LargeTextDisplay;
import digital.offenburg.aktenplan.component.LinkButton;
import digital.offenburg.aktenplan.config.SuppressForbiddenApi;
import digital.offenburg.aktenplan.data.dto.UserNoteDto;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.service.AktenPlanService;
import digital.offenburg.aktenplan.views.search.details.usernotes.events.CopyUserNoteEvent;
import digital.offenburg.aktenplan.views.search.details.usernotes.events.DeleteUserNoteEvent;
import digital.offenburg.aktenplan.views.search.details.usernotes.events.EditUserNoteEvent;

public class UserNoteDisplay extends VerticalLayout {
    private static final int DEFAULT_TEXT_LENGTH_TO_DISPLAY = 200;

    private transient final AktenPlanService aktenPlanService;
    private final UserNoteDto userNoteDto;
    private final FileReferenceDto fileReference;

    public UserNoteDisplay(final AktenPlanService aktenPlanService, final FileReferenceDto fileReference, final UserNoteDto userNoteDto) {
        this.aktenPlanService = aktenPlanService;
        this.userNoteDto = userNoteDto;
        this.fileReference = fileReference;

        this.setSpacing(false);
        this.setPadding(false);
        this.add(this.topRow(), this.title(), this.text());
    }

    @SuppressForbiddenApi
    private Component topRow() {
        final String dateDisplay = this.userNoteDto.getTimestamp().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMANY));
        final String versionText = String.format(LocaleContextHolder.getLocale(), "%s %s %s", "Version", this.userNoteDto.getPlanVersion(), dateDisplay);
        final Span version = new Span(versionText);
        version.addClassNames(LumoUtility.FontSize.SMALL, LumoUtility.TextColor.SECONDARY);

        final HorizontalLayout topRow = new HorizontalLayout();
        topRow.setWidthFull();
        topRow.add(version);

        if (this.aktenPlanService.canAddUserNotes(this.fileReference)) {
            final HorizontalLayout buttons = this.createButtons();
            topRow.add(buttons);
            topRow.setJustifyContentMode(JustifyContentMode.BETWEEN);
        }

        topRow.setAlignItems(Alignment.CENTER);
        return topRow;
    }

    private HorizontalLayout createButtons() {
        final Button edit = new LinkButton(VaadinIcon.EDIT.create());
        edit.addClickListener(event -> this.fireEvent(new EditUserNoteEvent(this, this.userNoteDto)));

        final Button delete = new LinkButton(VaadinIcon.TRASH.create());
        delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
        delete.addClickListener(event -> this.fireEvent(new DeleteUserNoteEvent(this, this.userNoteDto)));

        final Button copy = new LinkButton(VaadinIcon.COPY.create());
        copy.addClickListener(event -> this.fireEvent(new CopyUserNoteEvent(this, this.userNoteDto)));

        final HorizontalLayout buttons = new HorizontalLayout();
        if (this.fileReference.getVersion().equalsIgnoreCase(this.userNoteDto.getPlanVersion())) {
            buttons.add(edit, delete);
        } else {
            buttons.add(copy, delete);
        }
        return buttons;
    }

    private Component title() {
        final H5 title = new H5(this.userNoteDto.getTitle());
        title.addClassNames(LumoUtility.Margin.NONE);
        title.setWidthFull();
        return title;
    }

    private Component text() {
        return new LargeTextDisplay(this.userNoteDto.getText(), DEFAULT_TEXT_LENGTH_TO_DISPLAY, this.getTranslation("text.more.label"),
                                    this.getTranslation("text.less.label"));
    }

    public UserNoteDisplay withEditListener(final ComponentEventListener<EditUserNoteEvent> editListener) {
        this.addListener(EditUserNoteEvent.class, editListener);
        return this;
    }

    public UserNoteDisplay withDeleteListener(final ComponentEventListener<DeleteUserNoteEvent> deleteListener) {
        this.addListener(DeleteUserNoteEvent.class, deleteListener);
        return this;
    }

    public UserNoteDisplay withCopyListener(final ComponentEventListener<CopyUserNoteEvent> copyListener) {
        this.addListener(CopyUserNoteEvent.class, copyListener);
        return this;
    }

}
