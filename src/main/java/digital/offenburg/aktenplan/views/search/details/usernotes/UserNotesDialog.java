package digital.offenburg.aktenplan.views.search.details.usernotes;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;

import digital.offenburg.aktenplan.component.FormDialog;
import digital.offenburg.aktenplan.data.dto.UserNoteDto;
import digital.offenburg.aktenplan.event.SaveFormDialogEvent;

public final class UserNotesDialog extends FormDialog<UserNoteDto> {

    public static class Builder {
        private TextField title;
        private TextArea text;
        private UserNotesDialog dialog;

        public Builder forNew() {
            this.dialog = new UserNotesDialog(new UserNoteDto());
            this.dialog.prepare(this.formDialogI18n("tab.usernotes.dialog.newHeader"), this.formComponent());
            return this;
        }

        public Builder forEdit(final UserNoteDto userNoteDto) {
            this.dialog = new UserNotesDialog(userNoteDto);
            this.dialog.prepare(this.formDialogI18n("tab.usernotes.dialog.editHeader"), this.formComponent());
            return this;
        }

        public Builder withSaveEventListener(final ComponentEventListener<SaveFormDialogEvent<UserNoteDto>> listener) {
            this.dialog.addSaveItemEventListener(listener);
            return this;
        }

        public UserNotesDialog build() {
            return this.dialog;
        }

        private FormDialogI18n formDialogI18n(final String headerKey) {
            final FormDialogI18n formDialogI18n = new FormDialogI18n();
            formDialogI18n.setHeaderTitle(this.dialog.getTranslation(headerKey));
            formDialogI18n.setSaveButtonLabel(this.dialog.getTranslation("tab.usernotes.dialog.saveButtonLabel"));
            formDialogI18n.setCancelButtonLabel(this.dialog.getTranslation("tab.usernotes.dialog.cancelButtonLabel"));
            return formDialogI18n;
        }

        private Component formComponent() {
            this.title = new TextField(this.dialog.getTranslation("tab.usernotes.dialog.titleLabel"));
            this.text = new TextArea(this.dialog.getTranslation("tab.usernotes.dialog.textLabel"));
            this.text.setSizeFull();

            final VerticalLayout dialogLayout = new VerticalLayout(this.title, this.text);
            dialogLayout.setPadding(false);
            dialogLayout.setSpacing(false);
            dialogLayout.setSizeFull();
            dialogLayout.setAlignItems(FlexComponent.Alignment.STRETCH);

            this.dialog.getBinder().bindInstanceFields(this);
            return dialogLayout;
        }
    }

    private UserNotesDialog(final UserNoteDto userNoteDto) {
        super(UserNoteDto.class, userNoteDto);
        this.addClassNames("ui-tall-dialog");
    }

}
