package digital.offenburg.aktenplan.views.search.details.usernotes;

import java.util.Iterator;
import java.util.List;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.theme.lumo.LumoUtility.AlignItems;
import com.vaadin.flow.theme.lumo.LumoUtility.Background;
import com.vaadin.flow.theme.lumo.LumoUtility.Display;
import com.vaadin.flow.theme.lumo.LumoUtility.FontSize;
import com.vaadin.flow.theme.lumo.LumoUtility.Gap;
import com.vaadin.flow.theme.lumo.LumoUtility.Padding;

import digital.offenburg.aktenplan.component.ConfirmDialog;
import digital.offenburg.aktenplan.component.ConfirmDialog.ConfirmDialogI18n;
import digital.offenburg.aktenplan.component.LinkButton;
import digital.offenburg.aktenplan.data.dto.UserNoteDto;
import digital.offenburg.aktenplan.data.dto.fileplan.FileReferenceDto;
import digital.offenburg.aktenplan.data.service.AktenPlanService;
import digital.offenburg.aktenplan.event.SaveFormDialogEvent;
import digital.offenburg.aktenplan.views.search.details.usernotes.events.CopyUserNoteEvent;
import digital.offenburg.aktenplan.views.search.details.usernotes.events.DeleteUserNoteEvent;
import digital.offenburg.aktenplan.views.search.details.usernotes.events.EditUserNoteEvent;

public class UserNotesTabView extends VerticalLayout {
    private final transient AktenPlanService aktenPlanService;

    private final FileReferenceDto fileReferenceDto;
    private final VerticalLayout notesContainer;

    public UserNotesTabView(final AktenPlanService aktenPlanService, final FileReferenceDto fileReferenceDto) {
        this.aktenPlanService = aktenPlanService;
        this.fileReferenceDto = fileReferenceDto;
        this.notesContainer = new VerticalLayout();
        this.notesContainer.setPadding(false);

        this.add(this.notice());
        if (this.aktenPlanService.canAddUserNotes(fileReferenceDto)) {
            this.add(this.addNotes());
        }
        this.add(this.notesContainer);
        this.refreshNotes();
    }

    private Component notice() {
        final Span span = new Span(this.getTranslation("tab.usernotes.notice"));
        final Icon infoIcon = VaadinIcon.INFO_CIRCLE.create();
        infoIcon.addClassNames(FontSize.XXSMALL);

        final HorizontalLayout layout = new HorizontalLayout(infoIcon, span);
        layout.setWidthFull();
        layout.addClassNames("ui-border-shadow", Background.CONTRAST_5, Padding.MEDIUM, AlignItems.CENTER);
        return layout;
    }

    private Component addNotes() {
        final Span buttonContent = new Span();
        buttonContent.addClassNames(Display.INLINE_FLEX, Gap.MEDIUM, AlignItems.CENTER);
        buttonContent.add(VaadinIcon.PLUS.create());
        buttonContent.add(new Span(this.getTranslation("tab.usernotes.addNote")));

        final Button addBtn = new LinkButton(buttonContent);
        addBtn.addClassNames(Padding.NONE);
        addBtn.addClickListener(event -> {
            new UserNotesDialog.Builder()
                                         .forNew()
                                         .withSaveEventListener(this::saveUserNote)
                                         .build()
                                         .open();
        });
        return addBtn;
    }

    private void saveUserNote(final SaveFormDialogEvent<UserNoteDto> event) {
        this.aktenPlanService.saveUserNote(this.fileReferenceDto, event.getItem());
        this.refreshNotes();
        event.getSource().close();
    }

    private void refreshNotes() {
        this.notesContainer.removeAll();
        final List<UserNoteDto> notes = this.aktenPlanService.fetchUserNotes(this.fileReferenceDto);

        final ComponentEventListener<EditUserNoteEvent> editUserNotesListener = event -> {
            new UserNotesDialog.Builder().forEdit(event.getNote())
                                         .withSaveEventListener(this::saveUserNote)
                                         .build()
                                         .open();
        };

        final ComponentEventListener<DeleteUserNoteEvent> deleteUserNotesListener = event -> {
            final ConfirmDialogI18n confirmDialogI18n = new ConfirmDialogI18n();
            confirmDialogI18n.setTitle(this.getTranslation("tab.usernotes.delete.dialog.title"));
            confirmDialogI18n.setMessage(this.getTranslation("tab.usernotes.delete.dialog.message"));
            confirmDialogI18n.setCancelButtonLabel(this.getTranslation("tab.usernotes.delete.dialog.cancelButtonLabel"));
            confirmDialogI18n.setConfirmButtonLabel(this.getTranslation("tab.usernotes.delete.dialog.confirmButtonLabel"));

            final ConfirmDialog deleteConfirmDialog = new ConfirmDialog(confirmDialogI18n);
            deleteConfirmDialog.addConfirmListener(delEvent -> {
                this.aktenPlanService.deleteUserNote(this.fileReferenceDto, event.getNote());
                deleteConfirmDialog.close();
                this.refreshNotes();
            });
            deleteConfirmDialog.open();
        };

        final ComponentEventListener<CopyUserNoteEvent> copyUserNotesListener = event -> {
            this.aktenPlanService.copyUserNote(event.getNote(), this.fileReferenceDto);
            this.refreshNotes();
        };

        final Iterator<UserNoteDisplay> componentItr = notes.stream()
                                                            .map(item -> new UserNoteDisplay(this.aktenPlanService, this.fileReferenceDto, item).withEditListener(editUserNotesListener)
                                                                                                                                                .withDeleteListener(deleteUserNotesListener)
                                                                                                                                                .withCopyListener(copyUserNotesListener))
                                                            .iterator();

        if (componentItr.hasNext()) {
            this.notesContainer.add(componentItr.next());
        }

        this.showNotesHistory(componentItr);
    }

    private void showNotesHistory(final Iterator<UserNoteDisplay> componentItr) {
        if (componentItr.hasNext()) {
            this.notesContainer.add(new Hr());

            final H4 guidanceHistoryHeading = new H4(this.getTranslation("tab.usernotes.history.heading"));
            this.notesContainer.add(guidanceHistoryHeading);

            final VerticalLayout historyGuidanceContainer = new VerticalLayout();
            historyGuidanceContainer.setPadding(false);
            historyGuidanceContainer.setSpacing(false);
            historyGuidanceContainer.addClassName("ui-item-history-container");

            componentItr.forEachRemaining(item -> {
                item.addClassName("ui-item-history");
                historyGuidanceContainer.add(item);
            });
            this.notesContainer.add(historyGuidanceContainer);
        }
    }

}
