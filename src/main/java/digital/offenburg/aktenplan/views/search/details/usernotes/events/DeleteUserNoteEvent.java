package digital.offenburg.aktenplan.views.search.details.usernotes.events;

import com.vaadin.flow.component.ComponentEvent;

import digital.offenburg.aktenplan.data.dto.UserNoteDto;
import digital.offenburg.aktenplan.views.search.details.usernotes.UserNoteDisplay;

public class DeleteUserNoteEvent extends ComponentEvent<UserNoteDisplay> {

    private final UserNoteDto note;

    public DeleteUserNoteEvent(final UserNoteDisplay source, final UserNoteDto note) {
        super(source, false);
        this.note = note;
    }

    public UserNoteDto getNote() {
        return this.note;
    }

}
