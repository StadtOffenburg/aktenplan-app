create table t_guidance_request (id bigint not null, plan_version varchar(255) not null, text varchar(4000) not null, timestamp datetime(6) not null, title varchar(1000) not null, file_reference_id varchar(255) not null, user_id varchar(255) not null, primary key (id)) engine=InnoDB;
alter table t_guidance_request add constraint fk_guidancerequest_file_reference_id foreign key (file_reference_id) references t_file_reference (id);
alter table t_guidance_request add constraint fk_guidancerequest_user_id foreign key (user_id) references t_user (id);
