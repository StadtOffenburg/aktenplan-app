ALTER TABLE t_file_reference_all_unit DROP FOREIGN KEY fk_filereferenceallunit_user_id;
ALTER TABLE t_administrative_unit DROP FOREIGN KEY fk_administrativeunit_user_id;
ALTER TABLE t_guidance DROP FOREIGN KEY fk_guidance_user_id;
ALTER TABLE t_user_roles DROP FOREIGN KEY fk_userroles_user_id;
ALTER TABLE t_file_reference_unit DROP FOREIGN KEY fk_filereferenceunit_user_id;
ALTER TABLE t_file_upload DROP FOREIGN KEY fk_fileupload_uploaded_by_id;
ALTER TABLE t_user_note DROP FOREIGN KEY fk_usernote_user_id;
ALTER TABLE t_user_favourite DROP FOREIGN KEY fk_userfavourite_user_id;
ALTER TABLE t_guidance_request DROP FOREIGN KEY fk_guidancerequest_user_id;

--ALTER TABLE t_file_reference_all_unit ADD CONSTRAINT fk_filereferenceallunit_user_id  FOREIGN KEY (user_id) REFERENCES t_user(id);
--ALTER TABLE t_administrative_unit ADD CONSTRAINT fk_administrativeunit_user_id  FOREIGN KEY (user_id) REFERENCES t_user(id);
--ALTER TABLE t_guidance ADD CONSTRAINT fk_guidance_user_id  FOREIGN KEY (user_id) REFERENCES t_user(id);
--ALTER TABLE t_user_roles ADD CONSTRAINT fk_userroles_user_id  FOREIGN KEY (user_id) REFERENCES t_user(id);
--ALTER TABLE t_file_reference_unit ADD CONSTRAINT fk_filereferenceunit_user_id  FOREIGN KEY (user_id) REFERENCES t_user(id);
--ALTER TABLE t_file_upload ADD CONSTRAINT fk_fileupload_uploaded_by_id  FOREIGN KEY (user_id) REFERENCES t_user(id);
--ALTER TABLE t_user_note ADD CONSTRAINT fk_usernote_user_id  FOREIGN KEY (user_id) REFERENCES t_user(id);
--ALTER TABLE t_user_favourite ADD CONSTRAINT fk_userfavourite_user_id  FOREIGN KEY (user_id) REFERENCES t_user(id);
--ALTER TABLE t_guidance_request ADD CONSTRAINT fk_guidancerequest_user_id FOREIGN KEY (user_id) REFERENCES t_user (id);
